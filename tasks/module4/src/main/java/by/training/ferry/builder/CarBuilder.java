package by.training.ferry.builder;

import by.training.ferry.model.Car;

import java.util.Map;

public interface CarBuilder {
    Car build(Map<String,String> data);
}
