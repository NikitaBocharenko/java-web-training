package by.training.ferry.builder;

import by.training.ferry.model.Ferry;

import java.util.Map;

public class FerryBuilderImpl implements FerryBuilder {

    @Override
    public Ferry build(Map<String, String> data) {
        String areaStr = data.get("area");
        double area = Double.parseDouble(areaStr);
        String carryingCapacityStr = data.get("carryingCapacity");
        double carryingCapacity = Double.parseDouble(carryingCapacityStr);
        return new Ferry(area, carryingCapacity);
    }
}
