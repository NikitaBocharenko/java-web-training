package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Car implements Runnable {
    private int id;
    private double area;
    private double weight;
    private boolean isFerried;
    private Lock lock;
    private Condition ferriedCondition;

    private static final Logger log = Logger.getLogger(Car.class);

    public Car(int id, double area, double weight) {
        this.id = id;
        this.area = area;
        this.weight = weight;
        this.lock = new ReentrantLock();
        this.ferriedCondition = lock.newCondition();
    }

    public int getId() {
        return id;
    }

    public double getArea() {
        return area;
    }

    public double getWeight() {
        return weight;
    }

    public boolean isFerried() {
        return isFerried;
    }

    void wake(){
        lock.lock();
        try {
            isFerried = true;
            ferriedCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void run() {
        log.info("Car thread #" + id + " started");
        Pier pier = Pier.getInstance();
        try {
            pier.addToQueue(this);
            log.info("Car #" + id + ": at the pier and in queue to the ferry...");
            lock.lock();
            try {
                while (!this.isFerried){
                    ferriedCondition.await();
                }
            } finally {
                lock.unlock();
            }
            log.info("Car #" + id + ": finally I'm on the other side. Let's continue the trip!");
        } catch (InterruptedException e){
            log.error("Error in car thread #" + id + ": InterruptedException, " + e.getMessage());
            return;
        }
        log.info("Car thread #" + id + " finished");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id &&
                Double.compare(car.area, area) == 0 &&
                Double.compare(car.weight, weight) == 0 &&
                isFerried == car.isFerried;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, area, weight, isFerried);
    }
}
