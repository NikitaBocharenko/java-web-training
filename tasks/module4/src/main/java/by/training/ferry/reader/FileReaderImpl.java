package by.training.ferry.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileReaderImpl implements FileReader {
    @Override
    public List<String> readLines(String strPath) throws IOException {
        Path path = Paths.get(strPath);
        return Files.readAllLines(path);
    }
}
