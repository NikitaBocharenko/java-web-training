package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class UnloadingState implements FerryState {
    private Ferry ferry;
    private static final Logger log = Logger.getLogger(UnloadingState.class);
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    public UnloadingState(Ferry ferry) {
        this.ferry = ferry;
    }

    @Override
    public void next(Ferry ferry) {
        throw new IllegalStateException("State after UnloadingState doesn't exist");
    }

    @Override
    public void prev(Ferry ferry) {
        ferry.setFerryState(new SailingState(ferry));
    }

    @Override
    public void action() throws InterruptedException {
        log.info("Ferry is on the other side. Start unloading cars...");
        Iterator<Car> iterator = ferry.getCars().listIterator();
        while (iterator.hasNext()) {
            Car car = iterator.next();
            log.info("Ferry: unloading car #" + car.getId());
            iterator.remove();
            timeUnit.sleep(1);
            log.info("Ferry: car #" + car.getId() + " unloaded");
            log.info("Ferry: free area: " + ferry.getFreeArea() + ", available capacity: " + ferry.getFreeCarryingCapacity());
            car.wake();
        }
        ferry.setLoaded(false);
        log.info("All cars unloaded");
    }
}
