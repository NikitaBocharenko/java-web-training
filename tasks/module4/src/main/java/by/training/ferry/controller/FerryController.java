package by.training.ferry.controller;

import by.training.ferry.builder.CarBuilder;
import by.training.ferry.builder.FerryBuilder;
import by.training.ferry.model.Car;
import by.training.ferry.model.Ferry;
import by.training.ferry.model.ModelType;
import by.training.ferry.parser.LineParser;
import by.training.ferry.reader.FileReader;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FerryController {
    private FileReader fileReader;
    private LineParser lineParser;
    private FerryBuilder ferryBuilder;
    private CarBuilder carBuilder;
    private Ferry ferry;
    private List<Car> cars;

    private static final Logger log = Logger.getLogger(FerryController.class);

    public FerryController(FileReader fileReader, LineParser lineParser, FerryBuilder ferryBuilder, CarBuilder carBuilder) {
        this.fileReader = fileReader;
        this.lineParser = lineParser;
        this.ferryBuilder = ferryBuilder;
        this.carBuilder = carBuilder;
        this.cars = new ArrayList<>();
    }

    public List<Car> executeTask(String path) throws InterruptedException, IOException {
        log.info("Task execution started");
        List<String> lines = fileReader.readLines(path);
        this.buildModel(lines);
        cars.stream().map(Thread::new).forEach(Thread::start);
        Thread ferryThread = new Thread(ferry);
        ferryThread.start();
        ferryThread.join();
        log.info("Task execution finished");
        return cars.stream().filter(Car::isFerried).collect(Collectors.toList());
    }

    private void buildModel(List<String> lines){
        for (String line : lines) {
            Map<String,String> data = lineParser.parseLine(line);
            String typeStr = data.get("type");
            ModelType type = ModelType.fromString(typeStr)
                    .orElseThrow(() -> new IllegalArgumentException("Wrong model type"));
            switch (type){
                case FERRY:
                    ferry = ferryBuilder.build(data);
                    break;
                case CAR:
                    Car car = carBuilder.build(data);
                    cars.add(car);
                    break;
                default: throw new IllegalArgumentException("Wrong model type");
            }
        }
    }
}
