package by.training.ferry.parser;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class LineParserImpl implements LineParser {
    private static final Logger log = Logger.getLogger(LineParserImpl.class);
    @Override
    public Map<String, String> parseLine(String line) {
        Map<String,String> result = new HashMap<>();
        String[] keyValues = line.split("\\|");
        for (String keyValue : keyValues) {
            int delimiterIndex = keyValue.indexOf("=");
            String key = keyValue.substring(0, delimiterIndex);
            String value = keyValue.substring(delimiterIndex + 1);
            log.debug("key=" + key + ", value=" + value);
            result.put(key, value);
        }
        return result;
    }
}
