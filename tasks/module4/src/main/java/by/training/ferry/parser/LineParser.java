package by.training.ferry.parser;

import java.util.Map;

public interface LineParser {
    Map<String,String> parseLine(String line);
}
