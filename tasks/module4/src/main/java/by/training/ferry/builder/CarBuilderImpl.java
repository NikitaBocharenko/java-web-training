package by.training.ferry.builder;

import by.training.ferry.model.Car;

import java.util.Map;

public class CarBuilderImpl implements CarBuilder {

    @Override
    public Car build(Map<String, String> data) {
        String idStr = data.get("id");
        int id = Integer.parseInt(idStr);
        String areaStr = data.get("area");
        double area = Double.parseDouble(areaStr);
        String weightStr = data.get("weight");
        double weight = Double.parseDouble(weightStr);
        return new Car(id, area, weight);
    }
}
