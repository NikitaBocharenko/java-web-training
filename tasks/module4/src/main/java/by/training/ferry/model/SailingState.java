package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class SailingState implements FerryState {
    private Ferry ferry;
    private static final Logger log = Logger.getLogger(SailingState.class);
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    public SailingState(Ferry ferry) {
        this.ferry = ferry;
    }

    @Override
    public void next(Ferry ferry) {
        ferry.setFerryState(new UnloadingState(ferry));
    }

    @Override
    public void prev(Ferry ferry) {
        ferry.setFerryState(new LoadingState(ferry));
    }

    @Override
    public void action() throws InterruptedException {
        log.info("Ferry: sailing started");
        if (ferry.isLoaded()) {
            log.info("Ferry: sailing to the other side...");
        } else {
            log.info("Ferry: sailing to the pier...");
        }
        timeUnit.sleep(5);
        log.info("Ferry: sailing ended");
    }
}
