package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Pier {
    private Queue<Car> carQueue;
    private static Lock lock = new ReentrantLock();
    private static Pier PIER_INSTANCE = null;

    private static final Logger log = Logger.getLogger(Pier.class);

    private Pier() {
        this.carQueue = new LinkedList<>();
    }

    public static Pier getInstance(){
        log.info("Pier instance requested");
        if (PIER_INSTANCE == null) {
            lock.lock();
            try {
                if (PIER_INSTANCE == null) {
                    log.info("Pier instance created");
                    PIER_INSTANCE = new Pier();
                }
            } finally {
                lock.unlock();
            }
        }
        return PIER_INSTANCE;
    }

    public boolean isQueueNotEmpty(){
        lock.lock();
        try {
            return !this.carQueue.isEmpty();
        } finally {
            lock.unlock();
        }

    }

    public int getQueueSize() {
        return carQueue.size();
    }

    public void removeNextFromQueue(){
        lock.lock();
        try {
            carQueue.poll();
        } finally {
            lock.unlock();
        }
    }

    public Car peekNextFromQueue(){
        lock.lock();
        try {
            return carQueue.peek();
        } finally {
            lock.unlock();
        }
    }

    public void addToQueue(Car car){
        lock.lock();
        try {
            this.carQueue.add(car);
        } finally {
            lock.unlock();
        }
    }
}
