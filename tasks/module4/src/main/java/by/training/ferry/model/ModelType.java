package by.training.ferry.model;

import java.util.Optional;
import java.util.stream.Stream;

public enum ModelType {
    FERRY, CAR;

    public static Optional<ModelType> fromString(String str){
        return Stream.of(ModelType.values()).filter(t -> t.name().equalsIgnoreCase(str)).findFirst();
    }
}
