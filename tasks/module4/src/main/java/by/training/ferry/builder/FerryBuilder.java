package by.training.ferry.builder;

import by.training.ferry.model.Ferry;

import java.util.Map;

public interface FerryBuilder {
    Ferry build(Map<String,String> data);
}
