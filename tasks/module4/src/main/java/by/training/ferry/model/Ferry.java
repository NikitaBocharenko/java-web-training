package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Ferry implements Runnable{
    private double area;
    private double carryingCapacity;
    private List<Car> cars;
    private FerryState ferryState;
    private boolean isLoaded;

    private static final Logger log = Logger.getLogger(Ferry.class);

    public Ferry(double area, double carryingCapacity) {
        this.area = area;
        this.carryingCapacity = carryingCapacity;
        this.cars = new ArrayList<>();
        this.ferryState = new LoadingState(this);
    }

    private void nextState() {
        ferryState.next(this);
    }

    private void previousState() {
        ferryState.prev(this);
    }

    private void performAction() throws InterruptedException {
        ferryState.action();
    }

    public void setFerryState(FerryState ferryState) {
        this.ferryState = ferryState;
    }

    public boolean canLoad(Car car){
        return getFreeArea() >= car.getArea() && getFreeCarryingCapacity() >= car.getWeight();
    }

    public double getFreeArea(){
        double occupiedArea = cars.stream().mapToDouble(Car::getArea).sum();
        return area - occupiedArea;
    }

    public double getFreeCarryingCapacity(){
        double occupiedCarryingCapacity = cars.stream().mapToDouble(Car::getWeight).sum();
        return carryingCapacity - occupiedCarryingCapacity;
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }

    public void loadCar(Car car){
        this.cars.add(car);
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public void run() {
        log.info("Ferry thread started");
        Pier pier = Pier.getInstance();
        try {
            while (pier.isQueueNotEmpty()) {
                this.performAction();   //loading
                this.nextState();       //sailing state

                this.performAction();   //sailing to the other side
                this.nextState();       //unloading state

                this.performAction();   //unloading
                this.previousState();   //sailing state

                this.performAction();   //sailing to the pier
                this.previousState();   //loading state
            }
            log.info("Ferry: all cars ferried");
        } catch (InterruptedException e) {
            log.error("Error in ferry thread: InterruptedException, " + e.getMessage());
        }
        log.info("Ferry thread finished");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ferry ferry = (Ferry) o;
        return Double.compare(ferry.area, area) == 0 &&
                Double.compare(ferry.carryingCapacity, carryingCapacity) == 0 &&
                isLoaded == ferry.isLoaded &&
                Objects.equals(cars, ferry.cars) &&
                ferryState.getClass().equals(ferry.ferryState.getClass());
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, carryingCapacity, cars, ferryState, isLoaded);
    }
}
