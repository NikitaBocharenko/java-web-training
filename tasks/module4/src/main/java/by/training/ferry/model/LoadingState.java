package by.training.ferry.model;

import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class LoadingState implements FerryState {
    private Ferry ferry;
    private static final Logger log = Logger.getLogger(LoadingState.class);
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    public LoadingState(Ferry ferry) {
        this.ferry = ferry;
    }

    @Override
    public void next(Ferry ferry) {
        ferry.setFerryState(new SailingState(ferry));
    }

    @Override
    public void prev(Ferry ferry) {
        throw new IllegalStateException("State before LoadingState doesn't exist");
    }

    @Override
    public void action() throws InterruptedException {
        Pier pier = Pier.getInstance();
        log.info("Ferry at the pier. Start loading cars...");
        while (!ferry.isLoaded() && pier.isQueueNotEmpty()) {
            Car car = pier.peekNextFromQueue();
            log.info("Ferry: trying to load car #" + car.getId() + "...");
            if (ferry.canLoad(car)){
                pier.removeNextFromQueue();
                ferry.loadCar(car);
                timeUnit.sleep(1);
                log.info("Ferry: car #" + car.getId() + " loaded");
                log.info("Ferry: free area: " + ferry.getFreeArea() + ", available capacity: " + ferry.getFreeCarryingCapacity());
            } else {
                log.info("Ferry: I'm full!");
            }
        }
        ferry.setLoaded(true);
        log.info("Ferry: loading ended");
    }
}
