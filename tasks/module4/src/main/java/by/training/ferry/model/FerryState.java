package by.training.ferry.model;

public interface FerryState {
    void next(Ferry ferry);
    void prev(Ferry ferry);
    void action() throws InterruptedException;
}
