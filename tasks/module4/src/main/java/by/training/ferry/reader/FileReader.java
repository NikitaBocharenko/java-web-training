package by.training.ferry.reader;

import java.io.IOException;
import java.util.List;

public interface FileReader {
    List<String> readLines(String path) throws IOException;
}
