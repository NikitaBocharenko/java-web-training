package by.training.ferry.builder;

import by.training.ferry.model.Ferry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class FerryBuilderImplTest {
    private Map<String,String> data;
    private Ferry expected;

    @Before
    public void setUp(){
        data = new HashMap<>();
        data.put("area","20");
        data.put("carryingCapacity","30");
        expected = new Ferry(20, 30);
    }

    @Test
    public void shouldBuildFerry(){
        FerryBuilder builder = new FerryBuilderImpl();
        Ferry actual = builder.build(data);
        Assert.assertEquals(expected, actual);
    }
}
