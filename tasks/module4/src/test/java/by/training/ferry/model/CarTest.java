package by.training.ferry.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(JUnit4.class)
public class CarTest {
    private TimeUnit timeUnit = TimeUnit.MILLISECONDS;

    @Test
    public void shouldRunTask() throws InterruptedException {
        List<Car> cars = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Car car = new Car(i, i+1, i+1);
            cars.add(car);
        }
        List<Thread> carThreads = new ArrayList<>();
        cars.forEach((car)->carThreads.add(new Thread(car)));
        carThreads.forEach(Thread::start);
        Pier pier = Pier.getInstance();
        timeUnit.sleep(100);
        Assert.assertEquals(3, pier.getQueueSize());
        Assert.assertTrue(carThreads.stream().allMatch(thread -> thread.getState() == Thread.State.WAITING));
        cars.forEach(Car::wake);
        timeUnit.sleep(100);
        Assert.assertTrue(cars.stream().allMatch(Car::isFerried));
        timeUnit.sleep(100);
        Assert.assertTrue(carThreads.stream().allMatch(thread -> thread.getState() == Thread.State.TERMINATED));
    }
}
