package by.training.ferry.parser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class LineParserImplTest {
    private Map<String,String> expected;

    @Before
    public void setUp(){
        expected = new HashMap<>();
        expected.put("type", "Car");
        expected.put("id", "0");
        expected.put("area", "2.3588009909076044");
        expected.put("weight", "4.717601981815209");
    }
    @Test
    public void shouldParseLine(){
        LineParser parser = new LineParserImpl();
        Map<String, String> actual = parser.parseLine("type=Car|id=0|area=2.3588009909076044|weight=4.717601981815209");
        Assert.assertEquals(expected, actual);
    }
}
