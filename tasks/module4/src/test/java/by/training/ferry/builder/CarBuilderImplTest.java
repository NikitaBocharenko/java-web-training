package by.training.ferry.builder;

import by.training.ferry.model.Car;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class CarBuilderImplTest {
    private Map<String,String> data;
    private Car expected;

    @Before
    public void setUp(){
        data = new HashMap<>();
        data.put("id","0");
        data.put("area","2.3588009909076044");
        data.put("weight","4.717601981815209");
        expected = new Car(0, 2.3588009909076044, 4.717601981815209);
    }
    @Test
    public void shouldBuildCar(){
        CarBuilder builder = new CarBuilderImpl();
        Car actual = builder.build(data);
        Assert.assertEquals(expected, actual);
    }
}
