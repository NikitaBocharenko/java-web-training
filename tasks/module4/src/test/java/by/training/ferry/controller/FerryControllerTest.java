package by.training.ferry.controller;

import by.training.ferry.builder.CarBuilder;
import by.training.ferry.builder.CarBuilderImpl;
import by.training.ferry.builder.FerryBuilder;
import by.training.ferry.builder.FerryBuilderImpl;
import by.training.ferry.model.Car;
import by.training.ferry.parser.LineParser;
import by.training.ferry.parser.LineParserImpl;
import by.training.ferry.reader.FileReader;
import by.training.ferry.reader.FileReaderImpl;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.List;

@RunWith(JUnit4.class)
public class FerryControllerTest {
    private FerryController controller;

    private static final Logger log = Logger.getLogger(FerryControllerTest.class);

    @Before
    public void setUp(){
        FileReader reader = new FileReaderImpl();
        LineParser parser = new LineParserImpl();
        FerryBuilder ferryBuilder = new FerryBuilderImpl();
        CarBuilder carBuilder = new CarBuilderImpl();
        controller = new FerryController(reader, parser, ferryBuilder, carBuilder);
    }

    @Test
    public void shouldRunTask() throws InterruptedException, IOException {
        String path = this.getClass().getClassLoader().getResource("init.txt").getPath().substring(1);
        List<Car> ferriedCars = controller.executeTask(path);
        Assert.assertEquals(20, ferriedCars.size());
    }
}
