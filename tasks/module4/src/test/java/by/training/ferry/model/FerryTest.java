package by.training.ferry.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(JUnit4.class)
public class FerryTest {
    private List<Car> cars;
    private TimeUnit timeUnit = TimeUnit.MILLISECONDS;

    @Before
    public void setUp() throws InterruptedException {
        cars = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Car car = new Car(i, i+1, i+1);
            cars.add(car);
        }
        cars.stream().map(Thread::new).forEach(Thread::start);
        timeUnit.sleep(100);
    }

    @Test
    public void shouldRunTask() throws InterruptedException {
        Ferry ferry = new Ferry(20,30);
        Thread ferryThread = new Thread(ferry);
        ferryThread.start();
        ferryThread.join();
        timeUnit.sleep(100);
        Pier pier = Pier.getInstance();
        Assert.assertFalse(pier.isQueueNotEmpty());
        Assert.assertTrue(cars.stream().allMatch(Car::isFerried));
        Assert.assertEquals(Thread.State.TERMINATED, ferryThread.getState());
    }
}
