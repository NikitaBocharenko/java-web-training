package by.training.ferry.reader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.List;

@RunWith(JUnit4.class)
public class FileReaderImplTest {

    @Test
    public void shouldReadFile() throws IOException {
        FileReader reader = new FileReaderImpl();
        List<String> lines = reader.readLines(this.getClass().getClassLoader().getResource("init.txt").getPath().substring(1));
        Assert.assertEquals(21, lines.size());
    }

    @Test(expected = IOException.class)
    public void shouldThrowException() throws IOException {
        FileReader reader = new FileReaderImpl();
        reader.readLines("zzz");
        Assert.fail("IOException should be thrown");
    }
}
