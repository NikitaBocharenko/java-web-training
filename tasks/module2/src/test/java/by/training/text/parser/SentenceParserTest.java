package by.training.text.parser;

import by.training.text.model.Sentence;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.model.Lexeme;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SentenceParserTest {
    private TextComposite expectedSentence;

    @Before
    public void setUp(){
        expectedSentence = new Sentence();
        expectedSentence.addTextPart(new Lexeme("Hi,"));
        expectedSentence.addTextPart(new Lexeme("welcome"));
        expectedSentence.addTextPart(new Lexeme("to"));
        expectedSentence.addTextPart(new Lexeme("our"));
        expectedSentence.addTextPart(new Lexeme("friendly"));
        expectedSentence.addTextPart(new Lexeme("house!"));
    }

    @Test
    public void shouldParseSentence(){
        TextLeafParser sentenceParser = new SentenceParser(new LexemeParser());
        TextLeaf actualSentence = sentenceParser.parseText("Hi, welcome to our friendly house!");
        Assert.assertEquals(expectedSentence.getText(), actualSentence.getText());
    }
}
