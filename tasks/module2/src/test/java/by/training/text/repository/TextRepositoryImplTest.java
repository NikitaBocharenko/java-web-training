package by.training.text.repository;

import by.training.text.entity.LexemeEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.Optional;

@RunWith(JUnit4.class)
public class TextRepositoryImplTest {
    private TextRepository<LexemeEntity> wordRepository;

    @Before
    public void setUp(){
        wordRepository = new TextRepositoryImpl<>();
        wordRepository.add(new LexemeEntity(0, 0, 0, "Hello"));
        wordRepository.add(new LexemeEntity(1, 0, 1, "world"));
    }

    @Test
    public void shouldAddEntity(){
        wordRepository.add(new LexemeEntity(2, 0, 2, "hmmmmm"));
        List<LexemeEntity> repo = wordRepository.find(lexemeEntity -> true);
        Assert.assertEquals(3, repo.size());
    }

    @Test
    public void shouldSortByWordDescend(){
        List<LexemeEntity> sortedWords = wordRepository.sort((lexemeEntity1, lexemeEntity2) -> lexemeEntity2.getText().compareTo(lexemeEntity1.getText()));
        Assert.assertEquals("world", sortedWords.get(0).getText());
    }

    @Test
    public void shouldFindWord(){
        Optional<LexemeEntity> wordOptional = wordRepository.findFirst(lexemeEntity -> lexemeEntity.getText().equals("Hello"));
        Assert.assertTrue(wordOptional.isPresent());
    }

    @Test
    public void shouldReturnEmptyOptional(){
        Optional<LexemeEntity> wordOptional = wordRepository.findFirst(lexemeEntity -> lexemeEntity.getText().equals("zzzzzz"));
        Assert.assertFalse(wordOptional.isPresent());
    }
}
