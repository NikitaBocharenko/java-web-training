package by.training.text.model;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ParagraphTest {
    private static final Logger log = Logger.getLogger(ParagraphTest.class);
    private static final String EXPECTED_TEXT = "\tThere are three reasons why I prefer jogging to other sports. " +
            "One reason is that jogging is a cheap sport. " +
            "I can practise it anywhere at any time with no need for a ball or any other equipment.";

    @Test
    public void shouldConstructParagraph(){
        TextComposite sentence1 = new Sentence();
        sentence1.addTextPart(new Lexeme("There"));
        sentence1.addTextPart(new Lexeme("are"));
        sentence1.addTextPart(new Lexeme("three"));
        sentence1.addTextPart(new Lexeme("reasons"));
        sentence1.addTextPart(new Lexeme("why"));
        sentence1.addTextPart(new Lexeme("I"));
        sentence1.addTextPart(new Lexeme("prefer"));
        sentence1.addTextPart(new Lexeme("jogging"));
        sentence1.addTextPart(new Lexeme("to"));
        sentence1.addTextPart(new Lexeme("other"));
        sentence1.addTextPart(new Lexeme("sports."));
        TextComposite sentence2 = new Sentence();
        sentence2.addTextPart(new Lexeme("One"));
        sentence2.addTextPart(new Lexeme("reason"));
        sentence2.addTextPart(new Lexeme("is"));
        sentence2.addTextPart(new Lexeme("that"));
        sentence2.addTextPart(new Lexeme("jogging"));
        sentence2.addTextPart(new Lexeme("is"));
        sentence2.addTextPart(new Lexeme("a"));
        sentence2.addTextPart(new Lexeme("cheap"));
        sentence2.addTextPart(new Lexeme("sport."));
        TextComposite sentence3 = new Sentence();
        sentence3.addTextPart(new Lexeme("I"));
        sentence3.addTextPart(new Lexeme("can"));
        sentence3.addTextPart(new Lexeme("practise"));
        sentence3.addTextPart(new Lexeme("it"));
        sentence3.addTextPart(new Lexeme("anywhere"));
        sentence3.addTextPart(new Lexeme("at"));
        sentence3.addTextPart(new Lexeme("any"));
        sentence3.addTextPart(new Lexeme("time"));
        sentence3.addTextPart(new Lexeme("with"));
        sentence3.addTextPart(new Lexeme("no"));
        sentence3.addTextPart(new Lexeme("need"));
        sentence3.addTextPart(new Lexeme("for"));
        sentence3.addTextPart(new Lexeme("a"));
        sentence3.addTextPart(new Lexeme("ball"));
        sentence3.addTextPart(new Lexeme("or"));
        sentence3.addTextPart(new Lexeme("any"));
        sentence3.addTextPart(new Lexeme("other"));
        sentence3.addTextPart(new Lexeme("equipment."));
        TextComposite paragraph = new Paragraph();
        paragraph.addTextPart(sentence1);
        paragraph.addTextPart(sentence2);
        paragraph.addTextPart(sentence3);

        log.debug(paragraph.getText());
        Assert.assertEquals(EXPECTED_TEXT, paragraph.getText());
    }
}
