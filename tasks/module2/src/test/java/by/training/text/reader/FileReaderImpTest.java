package by.training.text.reader;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.util.Optional;

@RunWith(JUnit4.class)
public class FileReaderImpTest {
    private static final Logger log = Logger.getLogger(FileReaderImpTest.class);
    private static final int EXPECTED_LENGTH = 728;

    @Test
    public void shouldReadFile() {
        FileReader reader = new FileReaderImpl();
        Optional<String> fileDataOptional = reader.readData(this.getClass().getClassLoader().getResource("text1.txt").getPath().substring(1));
        if (fileDataOptional.isPresent()){
            log.debug(fileDataOptional.get());
            Assert.assertEquals(EXPECTED_LENGTH, fileDataOptional.get().length());
        }
        else {
            Assert.fail("file wasn't read");
        }
    }
}
