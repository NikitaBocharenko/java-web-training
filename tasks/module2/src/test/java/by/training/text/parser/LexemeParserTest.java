package by.training.text.parser;

import by.training.text.model.TextLeaf;
import by.training.text.model.Lexeme;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LexemeParserTest {
    private TextLeaf expectedWord;

    @Before
    public void setUp(){
        expectedWord = new Lexeme("letters, ");
    }

    @Test
    public void shouldParseWord(){
        TextLeafParser parser = new LexemeParser();
        TextLeaf actualWord = parser.parseText("letters, ");
        Assert.assertEquals(expectedWord.getText(), actualWord.getText());
    }
}
