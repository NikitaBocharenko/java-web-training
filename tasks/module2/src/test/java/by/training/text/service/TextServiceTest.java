package by.training.text.service;

import by.training.text.entity.LexemeEntity;
import by.training.text.entity.ParagraphEntity;
import by.training.text.entity.SentenceEntity;
import by.training.text.entity.TextEntity;
import by.training.text.model.Lexeme;
import by.training.text.model.Paragraph;
import by.training.text.model.Sentence;
import by.training.text.model.Text;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.repository.TextRepository;
import by.training.text.repository.TextRepositoryImpl;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TextServiceTest {
    private TextLeafService textService;
    private Text defaultText;
    private static final Logger log = Logger.getLogger(TextServiceTest.class);

    @Before
    public void setUp(){
        TextRepository<LexemeEntity> wordRepository = new TextRepositoryImpl<>();
        TextRepository<SentenceEntity> sentenceRepository = new TextRepositoryImpl<>();
        TextRepository<ParagraphEntity> paragraphRepository = new TextRepositoryImpl<>();
        TextRepository<TextEntity> textRepository = new TextRepositoryImpl<>();
        textService = new TextService(wordRepository, sentenceRepository, paragraphRepository, textRepository);

        defaultText = new Text();
        TextComposite paragraph1 = new Paragraph();
        TextComposite sentence1 = new Sentence();
        sentence1.addTextPart(new Lexeme("Phasellus"));
        sentence1.addTextPart(new Lexeme("eleifend"));
        sentence1.addTextPart(new Lexeme("facilisis"));
        sentence1.addTextPart(new Lexeme("convallis."));
        paragraph1.addTextPart(sentence1);
        TextComposite sentence2 = new Sentence();
        sentence2.addTextPart(new Lexeme("Aenean"));
        sentence2.addTextPart(new Lexeme("in"));
        sentence2.addTextPart(new Lexeme("purus"));
        sentence2.addTextPart(new Lexeme("ex."));
        paragraph1.addTextPart(sentence2);
        defaultText.addTextPart(paragraph1);
        TextComposite paragraph2 = new Paragraph();
        TextComposite sentence3 = new Sentence();
        sentence3.addTextPart(new Lexeme("Fusce"));
        sentence3.addTextPart(new Lexeme("tortor"));
        sentence3.addTextPart(new Lexeme("ligula,"));
        sentence3.addTextPart(new Lexeme("lobortis"));
        sentence3.addTextPart(new Lexeme("at"));
        sentence3.addTextPart(new Lexeme("auctor"));
        sentence3.addTextPart(new Lexeme("quis,"));
        sentence3.addTextPart(new Lexeme("ultricies"));
        sentence3.addTextPart(new Lexeme("eget"));
        sentence3.addTextPart(new Lexeme("erat."));
        paragraph2.addTextPart(sentence3);
        TextComposite sentence4 = new Sentence();
        sentence4.addTextPart(new Lexeme("Praesent"));
        sentence4.addTextPart(new Lexeme("sit"));
        sentence4.addTextPart(new Lexeme("amet"));
        sentence4.addTextPart(new Lexeme("scelerisque"));
        sentence4.addTextPart(new Lexeme("tellus,"));
        sentence4.addTextPart(new Lexeme("nec"));
        sentence4.addTextPart(new Lexeme("auctor"));
        sentence4.addTextPart(new Lexeme("dui."));
        paragraph2.addTextPart(sentence4);
        defaultText.addTextPart(paragraph2);
        TextComposite paragraph3 = new Paragraph();
        TextComposite sentence5 = new Sentence();
        sentence5.addTextPart(new Lexeme("Ut"));
        sentence5.addTextPart(new Lexeme("cursus"));
        sentence5.addTextPart(new Lexeme("massa"));
        sentence5.addTextPart(new Lexeme("justo,"));
        sentence5.addTextPart(new Lexeme("vel"));
        sentence5.addTextPart(new Lexeme("volutpat"));
        sentence5.addTextPart(new Lexeme("nisl"));
        sentence5.addTextPart(new Lexeme("laoreet"));
        sentence5.addTextPart(new Lexeme("tempor."));
        paragraph3.addTextPart(sentence5);
        defaultText.addTextPart(paragraph3);
    }

    @Test
    public void shouldSaveAndLoadText(){
        long idText = textService.save(defaultText);
        TextLeaf actualText = textService.load(idText);
        Assert.assertEquals(defaultText, actualText);
    }

    @Test
    public void shouldSortLexemesInSentencesByLength(){
        TextLeaf sortedText = textService.sort(defaultText);
        String expectedText = "\teleifend Phasellus facilisis convallis. in ex. purus Aenean\n" +
                "\tat eget Fusce quis, erat. tortor auctor ligula, lobortis ultricies sit nec amet dui. auctor tellus, Praesent scelerisque\n" +
                "\tUt vel nisl massa cursus justo, laoreet tempor. volutpat";
        Assert.assertEquals(expectedText, sortedText.getText());
    }
}
