package by.training.text.parser;

import by.training.text.model.TextComposite;
import by.training.text.model.Paragraph;
import by.training.text.model.Sentence;
import by.training.text.model.Lexeme;
import by.training.text.model.TextLeaf;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ParagraphParserTest {
    private TextComposite expectedParagraph;

    @Before
    public void setUp(){
        expectedParagraph = new Paragraph();
        TextComposite sentence1 = new Sentence();
        sentence1.addTextPart(new Lexeme("My"));
        sentence1.addTextPart(new Lexeme("name"));
        sentence1.addTextPart(new Lexeme("is"));
        sentence1.addTextPart(new Lexeme("Nikita"));
        sentence1.addTextPart(new Lexeme("Bocharenko."));
        expectedParagraph.addTextPart(sentence1);
        TextComposite sentence2 = new Sentence();
        sentence2.addTextPart(new Lexeme("I'm"));
        sentence2.addTextPart(new Lexeme("24"));
        sentence2.addTextPart(new Lexeme("years"));
        sentence2.addTextPart(new Lexeme("old."));
        expectedParagraph.addTextPart(sentence2);
        TextComposite sentence3 = new Sentence();
        sentence3.addTextPart(new Lexeme("My"));
        sentence3.addTextPart(new Lexeme("typical"));
        sentence3.addTextPart(new Lexeme("day"));
        sentence3.addTextPart(new Lexeme("looks"));
        sentence3.addTextPart(new Lexeme("like:"));
        sentence3.addTextPart(new Lexeme("work,"));
        sentence3.addTextPart(new Lexeme("hobby,"));
        sentence3.addTextPart(new Lexeme("rest,"));
        sentence3.addTextPart(new Lexeme("sleeping."));
        expectedParagraph.addTextPart(sentence3);
    }

    @Test
    public void shouldParseParagraph(){
        TextLeafParser paragraphParser = new ParagraphParser(new SentenceParser(new LexemeParser()));
        TextLeaf actualParagraph = paragraphParser.parseText("My name is Nikita Bocharenko. I'm 24 years old. My typical day looks like: work, hobby, rest, sleeping.");
        Assert.assertEquals(expectedParagraph.getText(), actualParagraph.getText());
    }
}
