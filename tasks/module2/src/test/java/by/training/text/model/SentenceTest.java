package by.training.text.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SentenceTest {
    private static final String EXPECTED_TEXT = "Well, better later, than never!";

    @Test
    public void shouldCreateSentence(){
        TextComposite sentence = new Sentence();
        sentence.addTextPart(new Lexeme("Well,"));
        sentence.addTextPart(new Lexeme("better"));
        sentence.addTextPart(new Lexeme("later,"));
        sentence.addTextPart(new Lexeme("than"));
        sentence.addTextPart(new Lexeme("never!"));
        String actualText = sentence.getText();
        Assert.assertEquals(EXPECTED_TEXT, actualText);
    }
}
