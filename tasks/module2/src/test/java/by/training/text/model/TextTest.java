package by.training.text.model;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TextTest {
    private static final Logger log = Logger.getLogger(TextTest.class);
    private static final String EXPECTED_TEXT = "\tHello world! I'm going to study Java hard!\n" +
            "\tBecause I really like programming.";

    @Test
    public void shouldConstructText(){
        TextComposite text = new Text();
        TextComposite sentence1 = new Sentence();
        sentence1.addTextPart(new Lexeme("Hello"));
        sentence1.addTextPart(new Lexeme("world!"));
        TextComposite sentence2 = new Sentence();
        sentence2.addTextPart(new Lexeme("I'm"));
        sentence2.addTextPart(new Lexeme("going"));
        sentence2.addTextPart(new Lexeme("to"));
        sentence2.addTextPart(new Lexeme("study"));
        sentence2.addTextPart(new Lexeme("Java"));
        sentence2.addTextPart(new Lexeme("hard!"));
        TextComposite paragraph1 = new Paragraph();
        paragraph1.addTextPart(sentence1);
        paragraph1.addTextPart(sentence2);
        text.addTextPart(paragraph1);
        TextComposite sentence3 = new Sentence();
        sentence3.addTextPart(new Lexeme("Because"));
        sentence3.addTextPart(new Lexeme("I"));
        sentence3.addTextPart(new Lexeme("really"));
        sentence3.addTextPart(new Lexeme("like"));
        sentence3.addTextPart(new Lexeme("programming."));
        TextComposite paragraph2 = new Paragraph();
        paragraph2.addTextPart(sentence3);
        text.addTextPart(paragraph2);

        log.debug(text.getText());
        Assert.assertEquals(EXPECTED_TEXT, text.getText());
    }
}
