package by.training.text.controller;

import by.training.text.entity.LexemeEntity;
import by.training.text.entity.ParagraphEntity;
import by.training.text.entity.SentenceEntity;
import by.training.text.entity.TextEntity;
import by.training.text.model.TextLeaf;
import by.training.text.parser.LexemeParser;
import by.training.text.parser.ParagraphParser;
import by.training.text.parser.SentenceParser;
import by.training.text.parser.TextLeafParser;
import by.training.text.parser.TextParser;
import by.training.text.reader.FileReader;
import by.training.text.reader.FileReaderImpl;
import by.training.text.repository.TextRepository;
import by.training.text.repository.TextRepositoryImpl;
import by.training.text.service.TextLeafService;
import by.training.text.service.TextService;
import by.training.text.validator.FileValidator;
import by.training.text.validator.FileValidatorImpl;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.util.Optional;

@RunWith(JUnit4.class)
public class TextControllerTest {
    private TextController controller;
    private static final Logger log = Logger.getLogger(TextControllerTest.class);

    @Before
    public void setUp(){
        FileValidator fileValidator = new FileValidatorImpl();
        FileReader fileReader = new FileReaderImpl();
        TextLeafParser parser = new TextParser(new ParagraphParser(new SentenceParser(new LexemeParser())));
        TextRepository<LexemeEntity> lexemeRepository = new TextRepositoryImpl<>();
        TextRepository<SentenceEntity> sentenceRepository = new TextRepositoryImpl<>();
        TextRepository<ParagraphEntity> paragraphRepository = new TextRepositoryImpl<>();
        TextRepository<TextEntity> textRepository = new TextRepositoryImpl<>();
        TextLeafService service = new TextService(lexemeRepository, sentenceRepository, paragraphRepository, textRepository);
        controller = new TextController(fileValidator, fileReader, parser, service);
    }

    @Test
    public void shouldExecuteTask(){
        Optional<TextLeaf> textOptional = controller.execute(this.getClass().getClassLoader().getResource("text1.txt").getPath().substring(1));
        if (textOptional.isPresent()){
            String expectedText = "\tIt has not but the only five also leap into survived remaining centuries, electronic unchanged. essentially typesetting, It in of of was the the and with more with like Lorem Ipsum Aldus Lorem sheets Ipsum. release desktop Letraset recently software versions passages, PageMaker including containing publishing popularised\n" +
                    "\ta a a It is be by of at the its long fact that will page when reader content looking layout. readable distracted established a of is it of as to it The has or- that less look like point using Ipsum more- using here, here' normal making opposed content letters, 'Content readable English. distribution\n" +
                    "\ta a a It is be of at its fact that will page when reader looking layout. established\n" +
                    "\tBye.";
            Assert.assertEquals(expectedText, textOptional.get().getText());
        }
        else{
            Assert.fail();
        }
    }

    @Test
    public void shouldGetEmptyOptional(){
        Optional<TextLeaf> textOptional = controller.execute("ttttttt");
        Assert.assertFalse(textOptional.isPresent());
    }
}
