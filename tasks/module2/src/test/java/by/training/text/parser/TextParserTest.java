package by.training.text.parser;

import by.training.text.model.Lexeme;
import by.training.text.model.Sentence;
import by.training.text.model.Paragraph;
import by.training.text.model.Text;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.reader.FileReader;
import by.training.text.reader.FileReaderImpl;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.util.Optional;

@RunWith(JUnit4.class)
public class TextParserTest {
    private TextComposite expectedText;
    private static final Logger log = Logger.getLogger(TextParserTest.class);

    @Before
    public void setUp(){
        expectedText = new Text();
        TextComposite paragraph1 = new Paragraph();
        TextComposite sentence1 = new Sentence();
        sentence1.addTextPart(new Lexeme("Phasellus"));
        sentence1.addTextPart(new Lexeme("eleifend"));
        sentence1.addTextPart(new Lexeme("facilisis"));
        sentence1.addTextPart(new Lexeme("convallis."));
        paragraph1.addTextPart(sentence1);
        TextComposite sentence2 = new Sentence();
        sentence2.addTextPart(new Lexeme("Aenean"));
        sentence2.addTextPart(new Lexeme("in"));
        sentence2.addTextPart(new Lexeme("purus"));
        sentence2.addTextPart(new Lexeme("ex."));
        paragraph1.addTextPart(sentence2);
        expectedText.addTextPart(paragraph1);
        TextComposite paragraph2 = new Paragraph();
        TextComposite sentence3 = new Sentence();
        sentence3.addTextPart(new Lexeme("Fusce"));
        sentence3.addTextPart(new Lexeme("tortor"));
        sentence3.addTextPart(new Lexeme("ligula,"));
        sentence3.addTextPart(new Lexeme("lobortis"));
        sentence3.addTextPart(new Lexeme("at"));
        sentence3.addTextPart(new Lexeme("auctor"));
        sentence3.addTextPart(new Lexeme("quis,"));
        sentence3.addTextPart(new Lexeme("ultricies"));
        sentence3.addTextPart(new Lexeme("eget"));
        sentence3.addTextPart(new Lexeme("erat."));
        paragraph2.addTextPart(sentence3);
        TextComposite sentence4 = new Sentence();
        sentence4.addTextPart(new Lexeme("Praesent"));
        sentence4.addTextPart(new Lexeme("sit"));
        sentence4.addTextPart(new Lexeme("amet"));
        sentence4.addTextPart(new Lexeme("scelerisque"));
        sentence4.addTextPart(new Lexeme("tellus,"));
        sentence4.addTextPart(new Lexeme("nec"));
        sentence4.addTextPart(new Lexeme("auctor"));
        sentence4.addTextPart(new Lexeme("dui."));
        paragraph2.addTextPart(sentence4);
        expectedText.addTextPart(paragraph2);
        TextComposite paragraph3 = new Paragraph();
        TextComposite sentence5 = new Sentence();
        sentence5.addTextPart(new Lexeme("Ut"));
        sentence5.addTextPart(new Lexeme("cursus"));
        sentence5.addTextPart(new Lexeme("massa"));
        sentence5.addTextPart(new Lexeme("justo,"));
        sentence5.addTextPart(new Lexeme("vel"));
        sentence5.addTextPart(new Lexeme("volutpat"));
        sentence5.addTextPart(new Lexeme("nisl"));
        sentence5.addTextPart(new Lexeme("laoreet"));
        sentence5.addTextPart(new Lexeme("tempor."));
        paragraph3.addTextPart(sentence5);
        expectedText.addTextPart(paragraph3);
    }

    @Test
    public void shouldParseText(){
        TextLeafParser parser = new TextParser(new ParagraphParser(new SentenceParser(new LexemeParser())));
        TextLeaf actualText = parser.parseText(
                "\tPhasellus eleifend facilisis convallis. Aenean in purus ex.\n" +
                "\tFusce tortor ligula, lobortis at auctor quis, ultricies eget erat. Praesent sit amet scelerisque tellus, nec auctor dui.\n" +
                "\tUt cursus massa justo, vel volutpat nisl laoreet tempor."
        );
        Assert.assertEquals(expectedText, actualText);
    }

    @Test
    public void shouldParseTextFromFile(){
        TextLeafParser parser = new TextParser(new ParagraphParser(new SentenceParser(new LexemeParser())));
        FileReader reader = new FileReaderImpl();
        Optional<String> dataOptional = reader.readData(this.getClass().getClassLoader().getResource("text1.txt").getPath().substring(1));
        String expectedText = "\tIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Ipsum is that it has a more- or- less normal distribution of letters, as opposed to using 'Content here, content here' making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.";
        if (dataOptional.isPresent()){
            TextLeaf parsedText = parser.parseText(dataOptional.get());
            Assert.assertEquals(expectedText, parsedText.getText());
        }
        else {
            Assert.fail();
        }
    }
}
