package by.training.text.validator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Set;

@RunWith(JUnit4.class)
public class FileValidatorImplTest {
    @Test
    public void shouldValidateFile(){
        FileValidator validator = new FileValidatorImpl();
        ValidationResult result = validator.validate(this.getClass().getClassLoader().getResource("text1.txt").getPath().substring(1));
        Assert.assertTrue(result.isValid());
    }

    @Test
    public void shouldGetPathIsNullError(){
        FileValidator validator = new FileValidatorImpl();
        ValidationResult result = validator.validate(null);
        Set<String> errors = result.getErrorsByType("path");
        Assert.assertTrue(errors.contains("path is null"));
    }

    @Test
    public void shouldGetFileNotExistError(){
        FileValidator validator = new FileValidatorImpl();
        ValidationResult result = validator.validate("jhg83fgkoi");
        Set<String> errors = result.getErrorsByType("path");
        Assert.assertTrue(errors.contains("file doesn't exist"));
    }

    @Test
    public void shouldGetNotAFileError(){
        FileValidator validator = new FileValidatorImpl();
        ValidationResult result = validator.validate("src\\test\\resources");
        Set<String> errors = result.getErrorsByType("path");
        Assert.assertTrue(errors.contains("not a file"));
    }
}
