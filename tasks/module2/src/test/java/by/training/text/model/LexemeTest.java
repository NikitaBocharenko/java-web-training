package by.training.text.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LexemeTest {
    private static final String EXPECTED_TEXT = "'Especially'";

    @Test
    public void shouldConstructWord(){
        TextLeaf word = new Lexeme("'Especially'");
        String actualText = word.getText();
        Assert.assertEquals(EXPECTED_TEXT, actualText);
    }
}
