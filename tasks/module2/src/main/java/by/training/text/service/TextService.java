package by.training.text.service;

import by.training.text.entity.ParagraphEntity;
import by.training.text.entity.SentenceEntity;
import by.training.text.entity.TextEntity;
import by.training.text.entity.LexemeEntity;
import by.training.text.model.Lexeme;
import by.training.text.model.Paragraph;
import by.training.text.model.Sentence;
import by.training.text.model.Text;
import by.training.text.model.TextLeaf;
import by.training.text.repository.TextRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TextService implements TextLeafService {
    private TextRepository<LexemeEntity> lexemeRepository;
    private TextRepository<SentenceEntity> sentenceRepository;
    private TextRepository<ParagraphEntity> paragraphRepository;
    private TextRepository<TextEntity> textRepository;

    public TextService(TextRepository<LexemeEntity> lexemeRepository, TextRepository<SentenceEntity> sentenceRepository, TextRepository<ParagraphEntity> paragraphRepository, TextRepository<TextEntity> textRepository) {
        this.lexemeRepository = lexemeRepository;
        this.sentenceRepository = sentenceRepository;
        this.paragraphRepository = paragraphRepository;
        this.textRepository = textRepository;
    }

    @Override
    public long save(TextLeaf text) {
        TextEntity textEntity = new TextEntity();
        textRepository.add(textEntity);
        int paragraphCounter = 0;
        for (TextLeaf paragraph : ((Text) text).getTextParts()){
            ParagraphEntity paragraphEntity = new ParagraphEntity(textEntity.getIdText(), paragraphCounter++);
            paragraphRepository.add(paragraphEntity);
            int sentenceCounter = 0;
            for (TextLeaf sentence : ((Paragraph) paragraph).getTextParts()){
                SentenceEntity sentenceEntity = new SentenceEntity(paragraphEntity.getIdParagraph(), sentenceCounter++);
                sentenceRepository.add(sentenceEntity);
                int lexemeCounter = 0;
                for (TextLeaf lexeme : ((Sentence) sentence).getTextParts()){
                    LexemeEntity lexemeEntity = new LexemeEntity(sentenceEntity.getIdSentence(), lexemeCounter++, lexeme.getText());
                    lexemeRepository.add(lexemeEntity);
                }
            }
        }
        return textEntity.getIdText();
    }

    @Override
    public TextLeaf load(long id) {
        Text text = new Text();
        List<ParagraphEntity> paragraphEntities = paragraphRepository.find(paragraphEntity -> paragraphEntity.getIdText() == id);
        paragraphEntities.sort(Comparator.comparingInt(ParagraphEntity::getPositionInText));
        for (ParagraphEntity paragraphEntity : paragraphEntities){
            Paragraph paragraph = new Paragraph();
            List<SentenceEntity> sentenceEntities = sentenceRepository.find(sentenceEntity -> sentenceEntity.getIdParagraph() == paragraphEntity.getIdParagraph());
            sentenceEntities.sort(Comparator.comparingInt(SentenceEntity::getPositionInParagraph));
            for (SentenceEntity sentenceEntity : sentenceEntities){
                Sentence sentence = new Sentence();
                List<LexemeEntity> lexemeEntities = lexemeRepository.find(lexemeEntity -> lexemeEntity.getIdSentence() == sentenceEntity.getIdSentence());
                lexemeEntities.sort(Comparator.comparingInt(LexemeEntity::getPositionInSentence));
                for (LexemeEntity lexemeEntity : lexemeEntities){
                    Lexeme lexeme = new Lexeme(lexemeEntity.getText());
                    sentence.addTextPart(lexeme);
                }
                paragraph.addTextPart(sentence);
            }
            text.addTextPart(paragraph);
        }
        return text;
    }

    @Override
    public TextLeaf sort(TextLeaf text) {
        Text sortedText = new Text();
        for (TextLeaf paragraph : ((Text)text).getTextParts()){
            Paragraph sortedParagraph = new Paragraph();
            for (TextLeaf sentence : ((Paragraph)paragraph).getTextParts()){
                Sentence sortedSentence = new Sentence();
                List<TextLeaf> sortedLexemes = ((Sentence)sentence).getTextParts().stream()
                                                .sorted(Comparator.comparingInt(textLeaf->textLeaf.getText().length()))
                                                .collect(Collectors.toList());
                for(TextLeaf lexeme : sortedLexemes){
                    sortedSentence.addTextPart(lexeme);
                }
                sortedParagraph.addTextPart(sortedSentence);
            }
            sortedText.addTextPart(sortedParagraph);
        }
        return sortedText;
    }
}
