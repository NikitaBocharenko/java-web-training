package by.training.text.parser;

import by.training.text.model.TextLeaf;
import by.training.text.model.Lexeme;

public class LexemeParser implements TextLeafParser {

    @Override
    public TextLeaf parseText(String textString) {
        return new Lexeme(textString);
    }
}
