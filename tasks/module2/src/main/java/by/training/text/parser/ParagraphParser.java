package by.training.text.parser;

import by.training.text.model.Paragraph;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser implements TextLeafParser {
    private static final String SENTENCE_REGEX = "([A-Z])(.\\n?)+?([.!?])";
    private static final Pattern SENTENCE_PATTERN = Pattern.compile(SENTENCE_REGEX, Pattern.MULTILINE);
    private TextLeafParser nextParser;

    private static final Logger log = Logger.getLogger(ParagraphParser.class);

    public ParagraphParser(TextLeafParser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public TextLeaf parseText(String textString) {
        TextComposite paragraph = new Paragraph();
        Matcher sentenceMatcher = SENTENCE_PATTERN.matcher(textString);
        while (sentenceMatcher.find()){
            log.debug("matched sentence: " + sentenceMatcher.group());
            TextLeaf sentence = nextParser.parseText(sentenceMatcher.group());
            paragraph.addTextPart(sentence);
        }
        return paragraph;
    }
}
