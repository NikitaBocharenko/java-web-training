package by.training.text.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Paragraph implements TextComposite {
    private List<TextLeaf> sentences = new LinkedList<>();

    @Override
    public void addTextPart(TextLeaf textPart) {
        this.sentences.add(textPart);
    }

    @Override
    public List<TextLeaf> getTextParts() {
        return new LinkedList<>(this.sentences);
    }

    @Override
    public String getText() {
        StringJoiner sentenceJoiner = new StringJoiner(" ");
        for (TextLeaf sentence : sentences){
            sentenceJoiner.add(sentence.getText());
        }
        return "\t" + sentenceJoiner.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paragraph paragraph = (Paragraph) o;
        return Objects.equals(sentences, paragraph.sentences);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentences);
    }

    @Override
    public String toString() {
        return "Paragraph{" +
                "sentences=" + sentences +
                '}';
    }
}

