package by.training.text.reader;

import java.util.Optional;

public interface FileReader {
    Optional<String> readData(String path);
}
