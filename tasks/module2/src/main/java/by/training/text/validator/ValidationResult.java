package by.training.text.validator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ValidationResult {
    private Map<String, Set<String>> errors;

    public ValidationResult() {
        this.errors = new HashMap<>();
    }

    public Map<String, Set<String>> getErrors() {
        return new HashMap<>(errors);
    }

    public void addError(String type, String message){
        if (errors.containsKey(type)) {
            errors.get(type).add(message);
        } else {
            Set<String> messages = new HashSet<>();
            messages.add(message);
            errors.put(type, messages);
        }
    }

    public Set<String> getErrorsByType(String type){
        return new HashSet<>(errors.get(type));
    }

    public boolean isValid(){
        return this.errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "errors=" + errors +
                '}';
    }
}
