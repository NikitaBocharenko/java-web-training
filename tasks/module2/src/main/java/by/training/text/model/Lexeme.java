package by.training.text.model;

import java.util.Objects;

public class Lexeme implements TextLeaf {
    private String text;

    public Lexeme(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lexeme lexeme = (Lexeme) o;
        return Objects.equals(text, lexeme.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }

    @Override
    public String toString() {
        return "Lexeme{" +
                "text='" + text + '\'' +
                '}';
    }
}
