package by.training.text.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Text implements TextComposite{
    private List<TextLeaf> paragraphs = new LinkedList<>();

    @Override
    public void addTextPart(TextLeaf textPart) {
        this.paragraphs.add(textPart);
    }

    @Override
    public List<TextLeaf> getTextParts() {
        return new LinkedList<>(this.paragraphs);
    }

    @Override
    public String getText() {
        StringJoiner paragraphJoiner = new StringJoiner("\n");
        for (TextLeaf paragraph : paragraphs){
            paragraphJoiner.add(paragraph.getText());
        }
        return paragraphJoiner.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return Objects.equals(paragraphs, text.paragraphs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paragraphs);
    }

    @Override
    public String toString() {
        return "Text{" +
                "paragraphs=" + paragraphs +
                '}';
    }
}
