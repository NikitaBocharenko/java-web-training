package by.training.text.parser;

import by.training.text.model.TextLeaf;

public interface TextLeafParser {
    TextLeaf parseText(String textString);
}
