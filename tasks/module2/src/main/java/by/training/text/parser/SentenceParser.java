package by.training.text.parser;

import by.training.text.model.Sentence;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser implements TextLeafParser {
    private static final String WORD_REGEX = "(\\p{Punct})?(\\w+('\\w)?)(\\p{Punct})?";
    private static final Pattern WORD_PATTERN = Pattern.compile(WORD_REGEX, Pattern.MULTILINE);
    private TextLeafParser nextParser;

    private static final Logger log = Logger.getLogger(SentenceParser.class);

    public SentenceParser(TextLeafParser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public TextLeaf parseText(String textString) {
        TextComposite sentence = new Sentence();
        Matcher wordMatcher = WORD_PATTERN.matcher(textString);
        while (wordMatcher.find()){
            log.debug("matched lexeme: " + wordMatcher.group());
            TextLeaf word = nextParser.parseText(wordMatcher.group());
            sentence.addTextPart(word);
        }
        return sentence;
    }
}
