package by.training.text.model;

public interface TextLeaf {
    String getText();
}
