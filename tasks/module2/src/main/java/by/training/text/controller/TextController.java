package by.training.text.controller;

import by.training.text.model.TextLeaf;
import by.training.text.parser.TextLeafParser;
import by.training.text.reader.FileReader;
import by.training.text.service.TextLeafService;
import by.training.text.validator.FileValidator;
import by.training.text.validator.ValidationResult;
import org.apache.log4j.Logger;
import java.util.Optional;

public class TextController {
    private FileValidator fileValidator;
    private FileReader fileReader;
    private TextLeafParser textParser;
    private TextLeafService textService;

    private static final Logger log = Logger.getLogger(TextController.class);

    public TextController(FileValidator fileValidator, FileReader fileReader, TextLeafParser textParser, TextLeafService textService) {
        this.fileValidator = fileValidator;
        this.fileReader = fileReader;
        this.textParser = textParser;
        this.textService = textService;
    }

    public Optional<TextLeaf> execute(String path){
        ValidationResult validationResult = fileValidator.validate(path);
        if (validationResult.isValid()){
            Optional<String> fileDataOptional = fileReader.readData(path);
            Optional<TextLeaf> textOptional = fileDataOptional.map(fileData -> textParser.parseText(fileData));
            Optional<Long> textIdOptional = textOptional.map(text -> textService.save(text));
            Optional<TextLeaf> loadedTextOptional = textIdOptional.map(textId -> textService.load(textId));
            return loadedTextOptional.map(loadedText -> textService.sort(loadedText));
        }
        log.error("filepath is incorrect: " + validationResult);
        return Optional.empty();
    }
}
