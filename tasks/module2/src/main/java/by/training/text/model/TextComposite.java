package by.training.text.model;

import java.util.List;

public interface TextComposite extends TextLeaf {
    void addTextPart(TextLeaf textPart);
    List<TextLeaf> getTextParts();
}
