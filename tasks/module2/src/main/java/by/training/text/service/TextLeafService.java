package by.training.text.service;

import by.training.text.model.TextLeaf;

public interface TextLeafService {
    long save(TextLeaf text);
    TextLeaf load(long id);
    TextLeaf sort(TextLeaf text);
}
