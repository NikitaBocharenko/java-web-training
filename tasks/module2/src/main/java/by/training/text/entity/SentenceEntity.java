package by.training.text.entity;

import java.util.concurrent.atomic.AtomicLong;

public class SentenceEntity {
    private static final AtomicLong idSequence = new AtomicLong(0);

    private long idSentence;
    private long idParagraph;
    private int positionInParagraph;

    public SentenceEntity(long idParagraph, int positionInParagraph) {
        this.idSentence = idSequence.getAndIncrement();
        this.idParagraph = idParagraph;
        this.positionInParagraph = positionInParagraph;
    }

    public SentenceEntity(long idSentence, long idParagraph, int positionInParagraph) {
        this.idSentence = idSentence;
        this.idParagraph = idParagraph;
        this.positionInParagraph = positionInParagraph;
    }

    public long getIdSentence() {
        return idSentence;
    }

    public long getIdParagraph() {
        return idParagraph;
    }

    public int getPositionInParagraph() {
        return positionInParagraph;
    }
}
