package by.training.text.repository;

public interface TextSortSpecification<T> {
    int compare(T firstEntity, T secondEntity);
}
