package by.training.text.validator;

public interface FileValidator {
    ValidationResult validate(String path);
}
