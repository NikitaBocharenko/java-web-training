package by.training.text.validator;

import java.io.File;

public class FileValidatorImpl implements FileValidator {
    @Override
    public ValidationResult validate(String path) {
        ValidationResult validationResult = new ValidationResult();
        if (path == null){
            validationResult.addError("path", "path is null");
            return validationResult;
        }
        File file = new File(path);
        if (!file.exists()){
            validationResult.addError("path", "file doesn't exist");
            return validationResult;
        }
        if (!file.isFile()){
            validationResult.addError("path", "not a file");
            return validationResult;
        }
        if (!file.canRead()){
            validationResult.addError("path", "file cannot be read");
            return validationResult;
        }
        return new ValidationResult();
    }
}
