package by.training.text.entity;

import java.util.concurrent.atomic.AtomicLong;

public class LexemeEntity {
    private static final AtomicLong idSequence = new AtomicLong(0);

    private long idLexeme;
    private long idSentence;
    private int positionInSentence;
    private String text;


    public LexemeEntity(long idSentence, int positionInSentence, String text) {
        this.idLexeme = idSequence.getAndIncrement();
        this.idSentence = idSentence;
        this.positionInSentence = positionInSentence;
        this.text = text;
    }

    public LexemeEntity(long idLexeme, long idSentence, int positionInSentence, String text) {
        this.idLexeme = idLexeme;
        this.idSentence = idSentence;
        this.positionInSentence = positionInSentence;
        this.text = text;
    }

    public long getIdLexeme() {
        return idLexeme;
    }

    public long getIdSentence() {
        return idSentence;
    }

    public int getPositionInSentence() {
        return positionInSentence;
    }

    public String getText() {
        return text;
    }
}
