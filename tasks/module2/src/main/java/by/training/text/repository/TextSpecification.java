package by.training.text.repository;

public interface TextSpecification<T> {
    boolean specified(T entity);
}
