package by.training.text.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TextRepositoryImpl<T> implements TextRepository<T> {
    private List<T> entities = new ArrayList<>();

    @Override
    public void add(T entity) {
        this.entities.add(entity);
    }

    @Override
    public List<T> find(TextSpecification<T> specification) {
        return this.entities.stream().filter(specification::specified).collect(Collectors.toList());
    }

    @Override
    public Optional<T> findFirst(TextSpecification<T> specification) {
        return this.entities.stream().filter(specification::specified).findFirst();
    }

    @Override
    public List<T> sort(TextSortSpecification<T> sortSpecification) {
        return this.entities.stream().sorted(sortSpecification::compare).collect(Collectors.toList());
    }
}
