package by.training.text.repository;

import java.util.List;
import java.util.Optional;

public interface TextRepository<T> {
    void add(T entity);
    List<T> find(TextSpecification<T> specification);
    Optional<T> findFirst(TextSpecification<T> specification);
    List<T> sort(TextSortSpecification<T> sortSpecification);
}
