package by.training.text.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Sentence implements TextComposite {
    private List<TextLeaf> lexemes = new LinkedList<>();

    @Override
    public void addTextPart(TextLeaf textPart) {
        this.lexemes.add(textPart);
    }

    @Override
    public List<TextLeaf> getTextParts() {
        return new LinkedList<>(this.lexemes);
    }

    @Override
    public String getText() {
        StringJoiner lexemeJoiner = new StringJoiner(" ");
        for (TextLeaf lexeme : lexemes){
            lexemeJoiner.add(lexeme.getText());
        }
        return lexemeJoiner.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return Objects.equals(lexemes, sentence.lexemes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lexemes);
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "lexemes=" + lexemes +
                '}';
    }
}
