package by.training.text.entity;

import java.util.concurrent.atomic.AtomicLong;

public class TextEntity {
    private static final AtomicLong idSequence = new AtomicLong(0);
    private long idText;

    public TextEntity() {
        this.idText = idSequence.getAndIncrement();
    }

    public TextEntity(long idText) {
        this.idText = idText;
    }

    public long getIdText() {
        return idText;
    }
}
