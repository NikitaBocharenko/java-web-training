package by.training.text.parser;

import by.training.text.model.Text;
import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser implements TextLeafParser {
    private static final String PARAGRAPH_REGEX = "^([ ]+|[\\t])(.|\\n)+?([.!?])$";
    private static final Pattern PARAGRAPH_PATTERN = Pattern.compile(PARAGRAPH_REGEX, Pattern.MULTILINE);
    private TextLeafParser nextParser;

    private static final Logger log = Logger.getLogger(TextParser.class);

    public TextParser(TextLeafParser nextParser) {
        this.nextParser = nextParser;
    }

    @Override
    public TextLeaf parseText(String textString) {
        TextComposite text = new Text();
        Matcher paragraphMatcher = PARAGRAPH_PATTERN.matcher(textString);
        while (paragraphMatcher.find()){
            log.debug("matched paragraph: " + paragraphMatcher.group());
            TextLeaf paragraph = nextParser.parseText(paragraphMatcher.group());
            text.addTextPart(paragraph);
        }
        return text;
    }
}
