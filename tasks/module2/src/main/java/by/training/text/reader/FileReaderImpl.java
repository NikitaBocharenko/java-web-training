package by.training.text.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class FileReaderImpl implements FileReader {
    @Override
    public Optional<String> readData(String stringPath){
        List<String> lines;
        try {
            lines = Files.readAllLines(Paths.get(stringPath));
        } catch (IOException e) {
            return Optional.empty();
        }
        String data = String.join("\n", lines);
        return Optional.of(data);
    }
}
