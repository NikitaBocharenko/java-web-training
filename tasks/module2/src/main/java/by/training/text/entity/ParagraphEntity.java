package by.training.text.entity;

import java.util.concurrent.atomic.AtomicLong;

public class ParagraphEntity {
    private static final AtomicLong idSequence = new AtomicLong(0);

    private long idParagraph;
    private long idText;
    private int positionInText;

    public ParagraphEntity(long idText, int positionInText) {
        this.idParagraph = idSequence.getAndIncrement();
        this.idText = idText;
        this.positionInText = positionInText;
    }

    public ParagraphEntity(long idParagraph, long idText, int positionInText) {
        this.idParagraph = idParagraph;
        this.idText = idText;
        this.positionInText = positionInText;
    }

    public long getIdParagraph() {
        return idParagraph;
    }

    public long getIdText() {
        return idText;
    }

    public int getPositionInText() {
        return positionInText;
    }
}
