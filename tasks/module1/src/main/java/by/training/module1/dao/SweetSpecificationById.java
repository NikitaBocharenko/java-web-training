package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public class SweetSpecificationById implements SweetSpecification {
    private long id;

    public SweetSpecificationById(long id) {
        this.id = id;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return this.id == sweet.getId();
    }
}
