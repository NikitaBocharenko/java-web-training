package by.training.module1.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum ChocolateType {
    BITTER, DARK, MILK, WHITE;

    public static Optional<ChocolateType> fromString(String type) {
        return Stream.of(ChocolateType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
