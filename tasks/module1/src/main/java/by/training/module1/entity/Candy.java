package by.training.module1.entity;

import java.util.Objects;

public class Candy extends Sweet {
    private CandyType candyType;
    private String shell;
    private String filling;

    public Candy(SweetType sweetType, long id, String name, double sugar, double weight, CandyType candyType, String shell, String filling) {
        super(sweetType, id, name, sugar, weight);
        this.candyType = candyType;
        this.shell = shell;
        this.filling = filling;
    }

    public CandyType getCandyType() {
        return candyType;
    }

    public void setCandyType(CandyType candyType) {
        this.candyType = candyType;
    }

    public String getShell() {
        return shell;
    }

    public void setShell(String shell) {
        this.shell = shell;
    }

    public String getFilling() {
        return filling;
    }

    public void setFilling(String filling) {
        this.filling = filling;
    }

    @Override
    public String toString() {
        return "Candy{" +
                super.toString() +
                ", candyType=" + candyType +
                ", shell='" + shell + '\'' +
                ", filling='" + filling + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Candy candy = (Candy) o;
        return candyType == candy.candyType &&
                Objects.equals(shell, candy.shell) &&
                Objects.equals(filling, candy.filling);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), candyType, shell, filling);
    }
}
