package by.training.module1.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum CandyType {
    CHOCOLATE, CARAMEL, LOLLIPOP, CHOCOLATE_BAR, MARMALADE, MARSHMALLOW;

    public static Optional<CandyType> fromString(String type) {
        return Stream.of(CandyType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
