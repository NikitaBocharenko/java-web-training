package by.training.module1.controller;

import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;
import by.training.module1.input.*;
import by.training.module1.service.SweetService;
import by.training.module1.service.SweetServiceImpl;
import by.training.module1.validation.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Test {
    private static final Logger log = Logger.getLogger(Test.class);
    public void method() {
        FileValidator fileValidator = new SweetFileValidator();
        FileInputImpl fileReader = new FileInputImpl();
        LineParser dataParser = new LineParserImpl();
        DataValidator dataValidator = new DataValidator();
        SweetBuilderFactory builderFactory = new SweetBuilderFactory();
        SweetValidatorFactory validatorFactory = new SweetValidatorFactory();
        SweetService service = new SweetServiceImpl();
        String path = "path";

        try {
            ValidationResult fileValidation = fileValidator.validateFile(path);
            if (fileValidation.isValid()){
                List<String> fileLines = fileReader.getLines(path);
                for (String line : fileLines){
                    Map<String,String> data = dataParser.parseLine(line);
                    ValidationResult dataValidationResult = dataValidator.validateData(data);
                    if (dataValidationResult.isValid()){
                        SweetType sweetType = SweetType.fromString(data.get("type")).orElseThrow(RuntimeException::new);
                        ValidationResult entityValidationResult = validatorFactory.getBySweetType(sweetType).validate(data);
                        if (entityValidationResult.isValid()){
                            SweetBuilder sweetBuilder = builderFactory.getBySweetType(sweetType);
                            Sweet sweet = sweetBuilder.build(data);
                            service.add(sweet);
                        } else {
                            log.error(entityValidationResult);
                        }
                    } else {
                        log.error(dataValidationResult);
                    }
                }
            } else {
                log.error(fileValidation);
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }
}
