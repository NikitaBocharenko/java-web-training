package by.training.module1.entity;

import java.util.Objects;

public class ChocolatePaste extends Sweet {
    private int volume;
    private String composition;

    public ChocolatePaste(SweetType sweetType, long id, String name, double sugar, double weight, int volume, String composition) {
        super(sweetType, id, name, sugar, weight);
        this.volume = volume;
        this.composition = composition;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    @Override
    public String toString() {
        return "ChocolatePaste{" +
                super.toString() +
                ", volume=" + volume +
                ", composition='" + composition + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ChocolatePaste that = (ChocolatePaste) o;
        return volume == that.volume &&
                Objects.equals(composition, that.composition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), volume, composition);
    }
}
