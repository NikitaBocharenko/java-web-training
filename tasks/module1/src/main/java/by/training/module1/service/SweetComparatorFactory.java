package by.training.module1.service;

import by.training.module1.entity.Sweet;

import java.util.Comparator;

public class SweetComparatorFactory {
    public static Comparator<Sweet> getByAttribute(SweetAttribute attribute){
        switch(attribute){
            case ID: return (o1, o2) -> Long.compare(o1.getId(), o2.getId());
            case NAME: return (o1, o2) -> o1.getName().compareTo(o2.getName());
            case SUGAR: return (o1, o2) -> Double.compare(o1.getSugar(), o2.getSugar());
            case WEIGHT: return (o1, o2) -> Double.compare(o1.getWeight(), o2.getWeight());
            case SWEET_TYPE: return (o1, o2) -> o1.getSweetType().compareTo(o2.getSweetType());
            default: throw new IllegalArgumentException();
        }
    }
}
