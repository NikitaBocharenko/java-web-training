package by.training.module1.input;

import by.training.module1.entity.Sweet;
import java.util.Map;

public interface SweetBuilder {
    Sweet build(Map<String,String> data);
}
