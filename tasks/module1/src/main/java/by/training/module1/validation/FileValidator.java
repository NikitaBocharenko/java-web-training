package by.training.module1.validation;

import java.io.IOException;

public interface FileValidator {
    ValidationResult validateFile(String path) throws IOException;
}
