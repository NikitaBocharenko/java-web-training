package by.training.module1.validation;

import by.training.module1.entity.SweetType;
import java.util.Map;
import java.util.Optional;

public class SweetValidatorFactory {

    public SweetValidator getBySweetType(SweetType type){
        switch(type){
            case CANDY: return new CandyValidator();
            case CHOCOLATE: return new ChocolateValidator();
            case CHOCOLATE_PASTE: return new ChocolatePasteValidator();
            default: throw new IllegalArgumentException();
        }
    }

    //don't have any idea, where I can place type builder :)
    public SweetType getSweetType(Map<String,String> data){
        String typeStr = data.get(SweetValidator.TYPE_KEY);
        Optional<SweetType> type = SweetType.fromString(typeStr);
        return type.get();
    }
}
