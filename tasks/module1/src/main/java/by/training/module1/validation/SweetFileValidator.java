package by.training.module1.validation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SweetFileValidator implements FileValidator{
    private final static String PATH_ERROR_KEY = "path";
    private final static Integer FILE_NOT_FOUND_ERROR_CODE = 101;
    private final static Integer BAD_DATA_IN_FILE_ERROR_CODE = 202;
    private final static String FILE_NOT_FOUND_ERROR_MESSAGE = "File not found";
    private final static String BAD_DATA_IN_FILE_ERROR_MESSAGE = "Bad data in file";

    //regex of correct line of data in file
    private final static String CORRECT_DATA_REGEX = "type:[A-z]+\\|id:[0-9]+\\|name:[\\p{L}|[0-9]|\\s|\\p{Blank}]+\\|sugar:[0-9]+(.[0-9])*\\|weight:[0-9]+(.[0-9])*.+";
    private static final Pattern pattern = Pattern.compile(CORRECT_DATA_REGEX);

    @Override
    public ValidationResult validateFile(String path) throws IOException {
        ValidationResult vr = new ValidationResult();
        if (!Files.exists(Paths.get(path), LinkOption.NOFOLLOW_LINKS)){
            vr.addError(PATH_ERROR_KEY,new ValidationMessage(FILE_NOT_FOUND_ERROR_CODE,FILE_NOT_FOUND_ERROR_MESSAGE));
            return vr;
        }
        List<String> lines = Files.readAllLines(Paths.get(path));
        for (String line : lines){
            Matcher matcher = pattern.matcher(line);
            if (matcher.matches()){
                //if at least one line in file looks correctly, I'll try to parse it
                return new ValidationResult();
            }
        }
        vr.addError(PATH_ERROR_KEY,new ValidationMessage(BAD_DATA_IN_FILE_ERROR_CODE,BAD_DATA_IN_FILE_ERROR_MESSAGE));
        return vr;
    }
}
