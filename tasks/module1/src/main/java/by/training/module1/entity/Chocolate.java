package by.training.module1.entity;

import java.util.Objects;

public class Chocolate extends Sweet {
    private ChocolateType chocolateType;
    private double cocoaContent;

    public Chocolate(SweetType sweetType, long id, String name, double sugar, double weight, ChocolateType chocolateType, double cocoaContent) {
        super(sweetType, id, name, sugar, weight);
        this.chocolateType = chocolateType;
        this.cocoaContent = cocoaContent;
    }

    public ChocolateType getCandyType() {
        return chocolateType;
    }

    public void setChocolateType(ChocolateType chocolateType) {
        this.chocolateType = chocolateType;
    }

    public double getCocoaContent() {
        return cocoaContent;
    }

    public void setCocoaContent(double cocoaContent) {
        this.cocoaContent = cocoaContent;
    }

    @Override
    public String toString() {
        return "Chocolate{" +
                super.toString() +
                ", chocolateType=" + chocolateType +
                ", cocoaContent=" + cocoaContent +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Chocolate chocolate = (Chocolate) o;
        return Double.compare(chocolate.cocoaContent, cocoaContent) == 0 &&
                chocolateType == chocolate.chocolateType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), chocolateType, cocoaContent);
    }
}
