package by.training.module1.entity;

import java.util.Objects;

public abstract class Sweet {
    private long id;
    private String name;
    private double sugar;
    private double weight;
    private SweetType type;

    public Sweet(SweetType type, long id, String name, double sugar, double weight) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.sugar = sugar;
        this.weight = weight;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSugar() {
        return sugar;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public SweetType getSweetType() {
        return type;
    }

    public void setType(SweetType type) {
        this.type = type;
    }
    //comparators

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sweet sweet = (Sweet) o;
        return id == sweet.id &&
                Double.compare(sweet.sugar, sugar) == 0 &&
                Double.compare(sweet.weight, weight) == 0 &&
                Objects.equals(name, sweet.name) &&
                type == sweet.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sugar, weight, type);
    }

    @Override
    public String toString() {
        return "type=" + type +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", sugar=" + sugar +
                ", weight=" + weight;
    }
}
