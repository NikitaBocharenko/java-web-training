package by.training.module1.service;

import by.training.module1.dao.SweetSpecification;
import by.training.module1.entity.Sweet;
import java.util.Comparator;
import java.util.List;

public interface SweetService {
    void add(Sweet sweet);
    void set(Sweet sweet);
    void remove(Sweet sweet);
    double calculateWeight();
    List<Sweet> sort(Comparator<Sweet> comparator);
    List<Sweet> find(SweetSpecification specification);
    List<Sweet> getAll();
}
