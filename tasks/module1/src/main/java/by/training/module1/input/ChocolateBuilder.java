package by.training.module1.input;

import by.training.module1.entity.Chocolate;
import by.training.module1.entity.ChocolateType;
import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;
import org.apache.log4j.Logger;
import java.util.Map;

public class ChocolateBuilder implements SweetBuilder {
    private Logger log = Logger.getLogger(ChocolateBuilder.class);

    @Override
    public Sweet build(Map<String, String> data) {
        SweetType sweetType = SweetType.valueOf(data.get("type").toUpperCase());
        int id = Integer.parseInt(data.get("id"));
        String name = data.get("name");
        double sugar = Double.parseDouble(data.get("sugar"));
        double weight = Double.parseDouble(data.get("weight"));
        ChocolateType chocolateType = ChocolateType.valueOf(data.get("chocolateType").toUpperCase());
        int cocoaContent = Integer.parseInt(data.get("cocoaContent"));
        Sweet chocolate = new Chocolate(sweetType, id, name, sugar, weight, chocolateType, cocoaContent);
        log.info("Entity has been built: " + chocolate);
        return chocolate;
    }
}
