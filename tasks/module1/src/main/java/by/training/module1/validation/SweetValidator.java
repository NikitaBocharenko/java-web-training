package by.training.module1.validation;

import by.training.module1.entity.SweetType;

import java.util.Map;
import java.util.Optional;

public abstract class SweetValidator {
    public static final String TYPE_KEY = "type";
    private static final String ID_KEY = "id";
    private static final String NAME_KEY = "name";
    private static final String SUGAR_KEY = "sugar";
    private static final String WEIGHT_KEY = "weight";

    private static final int TYPE_MISSING_ERROR_CODE = 300;
    private static final int TYPE_BAD_DATA_ERROR_CODE = 301;

    private static final int ID_MISSING_ERROR_CODE = 400;
    private static final int ID_BAD_DATA_ERROR_CODE = 401;
    private static final int ID_LESS_ZERO_ERROR_CODE = 402;

    private static final int NAME_MISSING_ERROR_CODE = 500;

    private static final int SUGAR_MISSING_ERROR_CODE = 600;
    private static final int SUGAR_BAD_DATA_ERROR_CODE = 601;
    private static final int SUGAR_LESS_OR_EQUALS_ZERO_ERROR_CODE = 602;

    private static final int WEIGHT_MISSING_ERROR_CODE = 700;
    private static final int WEIGHT_BAD_DATA_ERROR_CODE = 701;
    private static final int WEIGHT_LESS_OR_EQUALS_ZERO_ERROR_CODE = 702;

    public static ValidationResult validateType(Map<String,String> data){
        ValidationResult result = new ValidationResult();
        String typeStr = data.get(TYPE_KEY);
        if (typeStr == null){
            result.addError(TYPE_KEY, new ValidationMessage(TYPE_MISSING_ERROR_CODE, "Missing parameter: " + TYPE_KEY));
            return result;
        }
        Optional<SweetType> type = SweetType.fromString(typeStr);
        if (!type.isPresent()){
            result.addError(TYPE_KEY, new ValidationMessage(TYPE_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + TYPE_KEY));
            return result;
        }
        return result;
    }

    public ValidationResult validate(Map<String,String> data){
        ValidationResult result = new ValidationResult();

        String idStr = data.get(ID_KEY);
        if (idStr == null){
            result.addError(ID_KEY, new ValidationMessage(ID_MISSING_ERROR_CODE,  "Missing parameter: " + ID_KEY));
        } else {
            int id;
            try {
                id = Integer.parseInt(idStr);
                if (id < 0) {
                    result.addError(ID_KEY, new ValidationMessage(ID_LESS_ZERO_ERROR_CODE, "Parameter should be greater or equals than 0: " + ID_KEY));
                }
            } catch (NumberFormatException ex) {
                result.addError(ID_KEY, new ValidationMessage(ID_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + ID_KEY));
            }
        }

        String name = data.get(NAME_KEY);
        if (name == null){
            result.addError(NAME_KEY, new ValidationMessage(NAME_MISSING_ERROR_CODE,  "Missing parameter: " + NAME_KEY));
        }

        String sugarStr = data.get(SUGAR_KEY);
        if (sugarStr == null){
            result.addError(SUGAR_KEY, new ValidationMessage(SUGAR_MISSING_ERROR_CODE,  "Missing parameter: " + SUGAR_KEY));
        } else {
            double sugar;
            try {
                sugar = Double.parseDouble(sugarStr);
                if (sugar <= 0) {
                    result.addError(SUGAR_KEY, new ValidationMessage(SUGAR_LESS_OR_EQUALS_ZERO_ERROR_CODE, "Parameter should be greater than 0: " + SUGAR_KEY));
                }
            } catch (NumberFormatException ex) {
                result.addError(SUGAR_KEY, new ValidationMessage(SUGAR_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + SUGAR_KEY));
            }
        }

        String weightStr = data.get(WEIGHT_KEY);
        if (weightStr == null){
            result.addError(WEIGHT_KEY, new ValidationMessage(WEIGHT_MISSING_ERROR_CODE,  "Missing parameter: " + WEIGHT_KEY));
        } else {
            double weight;
            try {
                weight = Double.parseDouble(weightStr);
                if (weight <= 0) {
                    result.addError(WEIGHT_KEY, new ValidationMessage(WEIGHT_LESS_OR_EQUALS_ZERO_ERROR_CODE, "Parameter should be greater than 0: " + WEIGHT_KEY));
                }
            } catch (NumberFormatException ex) {
                result.addError(WEIGHT_KEY, new ValidationMessage(WEIGHT_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + WEIGHT_KEY));
            }
        }

        return result;
    }
}
