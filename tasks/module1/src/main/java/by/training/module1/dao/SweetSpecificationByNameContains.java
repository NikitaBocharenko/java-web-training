package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public class SweetSpecificationByNameContains implements SweetSpecification {
    private String nameContains;

    public SweetSpecificationByNameContains(String nameContains) {
        this.nameContains = nameContains;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return sweet.getName().toUpperCase().contains(nameContains.toUpperCase());
    }
}
