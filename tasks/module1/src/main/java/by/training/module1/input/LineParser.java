package by.training.module1.input;

import java.util.Map;

public interface LineParser {
    Map<String,String> parseLine(String line);
}
