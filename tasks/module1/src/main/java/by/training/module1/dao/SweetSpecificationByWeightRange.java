package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public class SweetSpecificationByWeightRange implements SweetSpecification {
    private double lowWeightLimit;
    private double topWeightLimit;

    public SweetSpecificationByWeightRange(double lowWeightLimit, double topWeightLimit) {
        this.lowWeightLimit = lowWeightLimit;
        this.topWeightLimit = topWeightLimit;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return sweet.getWeight() >= lowWeightLimit && sweet.getWeight() <= topWeightLimit;
    }
}
