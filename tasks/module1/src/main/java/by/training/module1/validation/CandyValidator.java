package by.training.module1.validation;

import by.training.module1.entity.CandyType;
import java.util.Map;

public class CandyValidator extends SweetValidator {
    private static final String CANDY_TYPE_KEY = "candyType";
    private static final String SHELL_KEY = "shell";
    private static final String FILLING_KEY = "filling";

    private static final int CANDY_TYPE_MISSING_ERROR_CODE = 1200;
    private static final int CANDY_TYPE_BAD_DATA_ERROR_CODE = 1201;

    private static final int SHELL_MISSING_ERROR_CODE = 1300;

    private static final int FILLING_MISSING_ERROR_CODE = 1400;

    @Override
    public ValidationResult validate(Map<String,String> data){
        ValidationResult vr = super.validate(data);
        String type = data.get(CANDY_TYPE_KEY);
        if (type == null){
            vr.addError(CANDY_TYPE_KEY, new ValidationMessage(CANDY_TYPE_MISSING_ERROR_CODE, "Missing parameter: " + CANDY_TYPE_KEY));
        } else {
            if (!CandyType.fromString(type).isPresent()){
                vr.addError(CANDY_TYPE_KEY, new ValidationMessage(CANDY_TYPE_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + CANDY_TYPE_KEY));
            }
        }

        String shell = data.get(SHELL_KEY);
        if (shell == null){
            vr.addError(SHELL_KEY, new ValidationMessage(SHELL_MISSING_ERROR_CODE,  "Missing parameter: " + SHELL_KEY));
        }

        String name = data.get(FILLING_KEY);
        if (name == null){
            vr.addError(FILLING_KEY, new ValidationMessage(FILLING_MISSING_ERROR_CODE,  "Missing parameter: " + FILLING_KEY));
        }

        return vr;
    }
}
