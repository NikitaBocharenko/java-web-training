package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public interface SweetSpecification {
    boolean specified(Sweet sweet);
}
