package by.training.module1.dao;

import by.training.module1.entity.Sweet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SweetRepositoryImpl implements SweetRepository {
    List<Sweet> sweets;

    public SweetRepositoryImpl() {
        this.sweets = new ArrayList<>();
    }

    @Override
    public void add(Sweet sweet) {
        this.sweets.add(sweet);
    }

    @Override
    public void remove(Sweet sweet) {
        this.sweets.remove(sweet);
    }

    @Override
    public void set(Sweet newSweet) {
        for (Sweet sweet : sweets){
            if (sweet.getId() == newSweet.getId()){
                sweets.set(sweets.indexOf(sweet),newSweet);
                break;
            }
        }
    }

    @Override
    public List<Sweet> find(SweetSpecification specification) {
        List<Sweet> result = new ArrayList<>();
        for (Sweet sweet : sweets){
            if (specification.specified(sweet)){
                result.add(sweet);
            }
        }
        return result;
    }

    @Override
    public List<Sweet> sort(Comparator<Sweet> comparator) {
        List<Sweet> result = new ArrayList<>(sweets);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<Sweet> getAll() {
        return new ArrayList<>(sweets);
    }
}
