package by.training.module1.dao;

import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;

public class SweetSpecificationByType implements SweetSpecification {
    private SweetType type;

    public SweetSpecificationByType(SweetType type) {
        this.type = type;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return sweet.getSweetType().equals(type);
    }
}
