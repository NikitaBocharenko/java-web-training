package by.training.module1.dao;

import by.training.module1.entity.Sweet;

import java.util.Comparator;
import java.util.List;

public interface SweetRepository {
    void add(Sweet sweet);
    void remove(Sweet sweet);
    void set(Sweet sweet);
    List<Sweet> find(SweetSpecification specification);
    List<Sweet> sort(Comparator<Sweet> comparator);
    List<Sweet> getAll();
}
