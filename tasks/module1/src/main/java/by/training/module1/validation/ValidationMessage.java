package by.training.module1.validation;

import java.util.Objects;

public class ValidationMessage {
    private int code;
    private String message;

    public ValidationMessage(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ValidationMessage() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationMessage validationMessage = (ValidationMessage) o;
        return code == validationMessage.code &&
                Objects.equals(message, validationMessage.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, message);
    }

    @Override
    public String toString() {
        return "ValidationMessage{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
