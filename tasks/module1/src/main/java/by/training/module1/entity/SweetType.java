package by.training.module1.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SweetType {
    CANDY,CHOCOLATE,CHOCOLATE_PASTE;

    public static Optional<SweetType> fromString(String type) {
        return Stream.of(SweetType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
