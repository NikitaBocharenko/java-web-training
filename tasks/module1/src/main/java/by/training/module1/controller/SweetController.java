package by.training.module1.controller;

import by.training.module1.dao.SweetSpecification;
import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;
import by.training.module1.input.*;
import by.training.module1.service.SweetService;
import by.training.module1.validation.FileValidator;
import by.training.module1.validation.SweetValidator;
import by.training.module1.validation.SweetValidatorFactory;
import by.training.module1.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SweetController {
    private SweetService service;
    private FileValidator fileValidator;
    private FileInput input;
    private LineParser parser;
    private SweetValidatorFactory validatorFactory;
    private SweetBuilderFactory builderFactory;

    private Logger log = Logger.getLogger(SweetController.class);

    public SweetController(SweetService service, FileValidator fileValidator, FileInput input, LineParser parser, SweetValidatorFactory validatorFactory, SweetBuilderFactory builderFactory) {
        this.service = service;
        this.fileValidator = fileValidator;
        this.input = input;
        this.parser = parser;
        this.validatorFactory = validatorFactory;
        this.builderFactory = builderFactory;
    }

    public void readFile(String path) throws IOException {
        log.info("File " + path + " read started");
        ValidationResult fileVR;
        fileVR = fileValidator.validateFile(path);
        if (fileVR.isValid()){
            List<String> fileLines;
            fileLines = input.getLines(path);
            for (String line : fileLines){
                Map<String,String> data = parser.parseLine(line);
                ValidationResult typeVR = SweetValidator.validateType(data);
                if (typeVR.isValid()){
                    SweetType sweetType = validatorFactory.getSweetType(data);
                    SweetValidator validator = validatorFactory.getBySweetType(sweetType);
                    ValidationResult dataVR = validator.validate(data);
                    if (dataVR.isValid()){
                        SweetBuilder sweetBuilder = builderFactory.getBySweetType(sweetType);
                        Sweet sweet = sweetBuilder.build(data);
                        service.add(sweet);
                    } else {
                        log.warn("Line: " + line + " from file: " + path + " has bad data: " + dataVR);
                    }
                } else {
                    log.warn("Line: " + line + " from file: " + path + " has bad type: " + typeVR);
                }
            }
        } else {
            log.warn("File: " + path + " is invalid: " + fileVR);
        }
        log.info("File " + path + " read finished");
    }

    public List<Sweet> getSweets(){
        List<Sweet> result = this.service.getAll();
        log.info("Method getSweets called: " + result);
        return result;
    }

    public double getWeight(){
        double result = this.service.calculateWeight();
        log.info("Method getWeight called: " + result);
        return result;
    }

    public List<Sweet> getSortedSweets(Comparator<Sweet> comparator){
        List<Sweet> result = this.service.sort(comparator);
        log.info("Method getSortedSweets called: " + result);
        return result;
    }

    public List<Sweet> getSpecifiedSweets(SweetSpecification specification){
        List<Sweet> result = this.service.find(specification);
        log.info("Method getSpecifiedSweets called: " + result);
        return result;
    }
}
