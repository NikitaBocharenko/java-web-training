package by.training.module1.input;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileInputImpl implements FileInput {
    @Override
    public List<String> getLines(String path) throws IOException {
        return Files.readAllLines(Paths.get(path));
    }
}
