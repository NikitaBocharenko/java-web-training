package by.training.module1.service;

public enum SweetAttribute {
    ID, NAME, WEIGHT, SUGAR, SWEET_TYPE
}
