package by.training.module1.validation;

import java.util.Map;

public class ChocolatePasteValidator extends SweetValidator {
    private static final String VOLUME_KEY = "volume";
    private static final String COMPOSITION_KEY = "composition";

    private static final int VOLUME_MISSING_ERROR_CODE = 1000;
    private static final int VOLUME_BAD_DATA_ERROR_CODE = 1001;
    private static final int VOLUME_LESS_OR_EQUALS_ZERO_ERROR_CODE = 1002;

    private static final int COMPOSITION_MISSING_ERROR_CODE = 1100;

    @Override
    public ValidationResult validate(Map<String,String> data) {
        ValidationResult vr = super.validate(data);
        String volumeStr = data.get(VOLUME_KEY);
        if (volumeStr == null){
            vr.addError(VOLUME_KEY, new ValidationMessage(VOLUME_MISSING_ERROR_CODE,  "Missing parameter: " + VOLUME_KEY));
        } else {
            int volume;
            try {
                volume = Integer.parseInt(volumeStr);
                if (volume <= 0) {
                    vr.addError(VOLUME_KEY, new ValidationMessage(VOLUME_LESS_OR_EQUALS_ZERO_ERROR_CODE, "Parameter should be greater than 0: " + VOLUME_KEY));
                }
            } catch (NumberFormatException ex) {
                vr.addError(VOLUME_KEY, new ValidationMessage(VOLUME_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + VOLUME_KEY));
            }
        }

        String composition = data.get(COMPOSITION_KEY);
        if (composition == null){
            vr.addError(COMPOSITION_KEY, new ValidationMessage(COMPOSITION_MISSING_ERROR_CODE,  "Missing parameter: " + COMPOSITION_KEY));
        }

        return vr;
    }
}
