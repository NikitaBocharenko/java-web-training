package by.training.module1.input;

import by.training.module1.entity.Candy;
import by.training.module1.entity.CandyType;
import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;
import org.apache.log4j.Logger;
import java.util.Map;

public class CandyBuilder implements SweetBuilder {
    private Logger log = Logger.getLogger(CandyBuilder.class);

    @Override
    public Sweet build(Map<String, String> data) {
        SweetType sweetType = SweetType.valueOf(data.get("type").toUpperCase());
        int id = Integer.parseInt(data.get("id"));
        String name = data.get("name");
        double sugar = Double.parseDouble(data.get("sugar"));
        double weight = Double.parseDouble(data.get("weight"));
        CandyType candyType = CandyType.valueOf(data.get("candyType").toUpperCase());
        String shell = data.get("shell");
        String filling = data.get("filling");
        Sweet candy = new Candy(sweetType, id, name, sugar, weight, candyType, shell, filling);
        log.info("Entity has been built: " + candy);
        return candy;
    }
}
