package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public class SweetSpecificationBySugarRange implements SweetSpecification{
    private double lowSugarLimit;
    private double topSugarLimit;

    public SweetSpecificationBySugarRange(double lowSugarLimit, double topSugarLimit) {
        this.lowSugarLimit = lowSugarLimit;
        this.topSugarLimit = topSugarLimit;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return sweet.getSugar() >= lowSugarLimit && sweet.getSugar() <= topSugarLimit;
    }
}
