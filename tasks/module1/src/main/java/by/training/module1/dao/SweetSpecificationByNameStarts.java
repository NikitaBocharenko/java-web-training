package by.training.module1.dao;

import by.training.module1.entity.Sweet;

public class SweetSpecificationByNameStarts implements SweetSpecification {
    private String nameStart;

    public SweetSpecificationByNameStarts(String nameStart) {
        this.nameStart = nameStart;
    }

    @Override
    public boolean specified(Sweet sweet) {
        return sweet.getName().toUpperCase().startsWith(nameStart.toUpperCase());
    }
}
