package by.training.module1.input;

import by.training.module1.entity.ChocolatePaste;
import by.training.module1.entity.Sweet;
import by.training.module1.entity.SweetType;
import org.apache.log4j.Logger;
import java.util.Map;

public class ChocolatePasteBuilder implements SweetBuilder {
    private Logger log = Logger.getLogger(ChocolatePasteBuilder.class);

    @Override
    public Sweet build(Map<String, String> data) {
        SweetType sweetType = SweetType.valueOf(data.get("type").toUpperCase());
        int id = Integer.parseInt(data.get("id"));
        String name = data.get("name");
        double sugar = Double.parseDouble(data.get("sugar"));
        double weight = Double.parseDouble(data.get("weight"));
        int volume = Integer.parseInt(data.get("volume"));
        String composition = data.get("composition");
        Sweet chocolatePaste = new ChocolatePaste(sweetType, id, name, sugar, weight, volume, composition);
        log.info("Entity has been built: " + chocolatePaste);
        return chocolatePaste;
    }
}
