package by.training.module1.validation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ValidationResult {
    private Map<String, Set<ValidationMessage>> errors;

    public ValidationResult() {
        errors = new HashMap<>();
    }

    public ValidationResult(Map<String, Set<ValidationMessage>> errors) {
        this.errors = errors;
    }

    public Map<String, Set<ValidationMessage>> getErrors() {
        return new HashMap<>(errors);
    }

    public void addError(String type, ValidationMessage validationMessage){
        if (errors.containsKey(type)) {
            errors.get(type).add(validationMessage);
        } else {
            Set<ValidationMessage> tmp = new HashSet<>();
            tmp.add(validationMessage);
            errors.put(type, tmp);
        }
    }

    public void addError(Map<String, Set<ValidationMessage>> errors){this.errors.putAll(errors);}

    public Set<ValidationMessage> getErrorsByType(String type){
        return new HashSet<>(errors.get(type));
    }

    public boolean isValid(){
        return this.errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "errors=" + errors +
                '}';
    }
}
