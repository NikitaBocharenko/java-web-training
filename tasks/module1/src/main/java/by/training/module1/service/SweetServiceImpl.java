package by.training.module1.service;

import by.training.module1.dao.SweetRepository;
import by.training.module1.dao.SweetSpecification;
import by.training.module1.entity.Sweet;
import java.util.Comparator;
import java.util.List;

public class SweetServiceImpl implements SweetService {
    private SweetRepository repository;

    public SweetServiceImpl(SweetRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(Sweet sweet){this.repository.add(sweet);}

    public void set(Sweet newSweet){ this.repository.set(newSweet); }

    @Override
    public void remove(Sweet sweet){ this.repository.remove(sweet); }

    @Override
    public List<Sweet> find(SweetSpecification specification){
        List<Sweet> result = this.repository.find(specification);
        return result;
    }

    @Override
    public double calculateWeight() {
        List<Sweet> sweets = this.repository.getAll();
        double result = 0;
        for (Sweet sweet : sweets){
            result += sweet.getWeight();
        }
        return result;
    }

    @Override
    public List<Sweet> getAll() {
        return this.repository.getAll();
    }

    @Override
    public List<Sweet> sort(Comparator<Sweet> comparator){
        return this.repository.sort(comparator);
    }

}
