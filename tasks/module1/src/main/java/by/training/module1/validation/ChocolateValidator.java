package by.training.module1.validation;

import by.training.module1.entity.ChocolateType;
import java.util.Map;

public class ChocolateValidator extends SweetValidator {
    private static final String CHOCOLATE_TYPE_KEY = "chocolateType";
    private static final String COCOA_CONTENT_KEY = "cocoaContent";

    private static final int CHOCOLATE_TYPE_MISSING_ERROR_CODE = 800;
    private static final int CHOCOLATE_TYPE_BAD_DATA_ERROR_CODE = 801;

    private static final int COCOA_CONTENT_MISSING_ERROR_CODE = 900;
    private static final int COCOA_CONTENT_BAD_DATA_ERROR_CODE = 901;
    private static final int COCOA_CONTENT_LESS_ZERO_OR_MORE_HUNDRED = 902;

    @Override
    public ValidationResult validate(Map<String,String> data) {
        ValidationResult vr = super.validate(data);
        String type = data.get(CHOCOLATE_TYPE_KEY);
        if (type == null){
            vr.addError(CHOCOLATE_TYPE_KEY, new ValidationMessage(CHOCOLATE_TYPE_MISSING_ERROR_CODE, "Missing parameter: " + CHOCOLATE_TYPE_KEY));
        } else {
            if (!ChocolateType.fromString(type).isPresent()) {
                vr.addError(CHOCOLATE_TYPE_KEY, new ValidationMessage(CHOCOLATE_TYPE_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + CHOCOLATE_TYPE_KEY));
            }
        }

        String cocoaContentStr = data.get(COCOA_CONTENT_KEY);
        if (cocoaContentStr == null){
            vr.addError(COCOA_CONTENT_KEY, new ValidationMessage(COCOA_CONTENT_MISSING_ERROR_CODE,  "Missing parameter: " + COCOA_CONTENT_KEY));
        } else {
            double cocoaContent;
            try {
                cocoaContent = Double.parseDouble(cocoaContentStr);
                if (cocoaContent <= 0 || cocoaContent >= 100) {
                    vr.addError(COCOA_CONTENT_KEY, new ValidationMessage(COCOA_CONTENT_LESS_ZERO_OR_MORE_HUNDRED, "Parameter should be greater than 0 and less than 100: " + COCOA_CONTENT_KEY));
                }
            } catch (NumberFormatException ex) {
                vr.addError(COCOA_CONTENT_KEY, new ValidationMessage(COCOA_CONTENT_BAD_DATA_ERROR_CODE, "Wrong value of parameter: " + COCOA_CONTENT_KEY));
            }
        }

        return vr;
    }
}
