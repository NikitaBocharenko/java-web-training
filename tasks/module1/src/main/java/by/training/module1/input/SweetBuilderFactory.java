package by.training.module1.input;

import by.training.module1.entity.SweetType;

public class SweetBuilderFactory {
    public SweetBuilder getBySweetType(SweetType type){
        switch (type){
            case CANDY: return new CandyBuilder();
            case CHOCOLATE: return new ChocolateBuilder();
            case CHOCOLATE_PASTE: return new ChocolatePasteBuilder();
            default: throw new IllegalArgumentException();
        }
    }
}
