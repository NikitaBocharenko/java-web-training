package by.training.module1.input;

import java.io.IOException;
import java.util.List;

public interface FileInput {
    List<String> getLines(String path) throws IOException;
}
