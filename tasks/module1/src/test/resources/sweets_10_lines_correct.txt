type:candy|id:1|name:Испанская ночь|sugar:8|weight:10|candyType:CHOCOLATE|shell:кондитерская глазурь|filling:патока
type:candy|id:2|name:Буренка Му|sugar:7|weight:10|candyType:CHOCOLATE|shell:молочный шоколад|filling:молочная начинка
type:candy|id:3|name:MICHELLE LA NOIR|sugar:7|weight:9|candyType:CHOCOLATE|shell:кондитерская глазурьfilling:патока
type:candy|id:4|name:Петровский форт|sugar:13|weight:20|candyType:CHOCOLATE|shell:шоколадная глазурь|filling:нуга
type:candy|id:5|:Mars|sugar:34|weight:50|candyType:CHOCOLATE_BAR|shell:молочный шоколад|filling:нуга, карамель
type:candy|id:6|name:Lion|sugar:25.6|weight:42.2|candyType:CHOCOLATE_BAR|shell:белая кондитерская глазурь \"Lion White\"|filling:карамель, хлопья
type:candy|id:7|name:Москвичка|sugar:2.5|weight:5|candyType:CARAMEL|shellкарамель|filling:ликерная начинка
type:candy|id:8|name:Чупа чупс со вкусом апельсина|sugar:11|weight:12|candyType:LOLLIPOP|shell:|filling:фруктовая патока
type:candy|id:9|name:Грушевые дольки|sugar:sssss|weight:210|candyType:MARMALADE|shell:|filling:патока
type:candy|id:10|name:Lefirelle бело-розовый|sugar:81|weight:230|candyType:MARSHMALLOW|shell:|filling:патока, яблочное пюре
type:chocolate|id:11|name:Michelle|sugar:49|weight:90|chocolateType:BITTER|cocoaContent:68
type:chocolate|id:12|name:Alpen Krone|sugar:59|weight:90|chocolateType:MILK|cocoaContent:29
type:chocolate|id:13|name:Loka|sugar:61|weight:100|chocolateType:MILK|cocoaContent:28
type:chocolate_paste|id:14|name:Belisa|sugar:58|weight:250|volume:250|composition:шоколадно-ореховая
type:chocolate_paste|id:15|name:Nutella|sugar:-57.3|weight:180|volume:200|composition:ореховая с добавлением какао