package by.training.module1.service;

import by.training.module1.dao.*;
import by.training.module1.entity.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Comparator;
import java.util.List;

@RunWith(JUnit4.class)
public class SweetServiceImplTest {
    private SweetServiceImpl service;
    private static final double TOTAL_WEIGHT = 1308.2;
    private static final String FIRST_NAME = "Alpen Krone";
    private static final double LOWEST_WEIGHT = 5;
    private static final double SECOND_WEIGHT = 9;
    private static final String NUTELLA_NAME = "Nutella";
    private static final int SWEETS_NAMES_CONTAINS_ELL_NUMBER = 4;
    private static final int SWEETS_NAMES_STARTS_L_NUMBER = 3;
    private static final int SWEETS_TYPE_CANDY_NUMBER = 10;
    private static final int SWEETS_WEIGHT_BETWEEN_TEN_AND_TWENTY_NUMBER = 4;

    @Before
    public void init(){
        SweetRepository repository = new SweetRepositoryImpl();
        service = new SweetServiceImpl(repository);
        service.add(new Candy(SweetType.CANDY, 1,"Испанская ночь", 8, 10, CandyType.CHOCOLATE, "кондитерская глазурь","патока"));
        service.add(new Candy(SweetType.CANDY,2,"Буренка Му", 7, 10, CandyType.CHOCOLATE, "молочный шоколад","молочная начинка"));
        service.add(new Candy(SweetType.CANDY,3,"MICHELLE LA NOIR", 7, 9, CandyType.CHOCOLATE, "кондитерская глазурь","патока"));
        service.add(new Candy(SweetType.CANDY,4,"Петровский форт", 13, 20, CandyType.CHOCOLATE, "шоколадная глазурь","нуга"));
        service.add(new Candy(SweetType.CANDY,5,"Mars", 34, 50, CandyType.CHOCOLATE_BAR, "молочный шоколад","нуга, карамель"));
        service.add(new Candy(SweetType.CANDY,6,"Lion", 25.6, 42.2, CandyType.CHOCOLATE_BAR, "белая кондитерская глазурь \"Lion White\"","карамель, хлопья"));
        service.add(new Candy(SweetType.CANDY,7,"Москвичка", 2.5, 5, CandyType.CARAMEL, "карамель","ликерная начинка"));
        service.add(new Candy(SweetType.CANDY,8,"Чупа чупс со вкусом апельсина", 11, 12, CandyType.LOLLIPOP, null,"фруктовая патока"));
        service.add(new Candy(SweetType.CANDY,9,"Грушевые дольки", 82, 210, CandyType.MARMALADE, null,"патока"));
        service.add(new Candy(SweetType.CANDY,10,"Lefirelle бело-розовый", 81, 230, CandyType.MARSHMALLOW, null,"патока, яблочное пюре"));
        service.add(new Chocolate(SweetType.CHOCOLATE,11,"Michelle", 49, 90, ChocolateType.BITTER, 68));
        service.add(new Chocolate(SweetType.CHOCOLATE,12,"Alpen Krone", 59, 90, ChocolateType.MILK, 29));
        service.add(new Chocolate(SweetType.CHOCOLATE,13,"Loka", 61, 100, ChocolateType.MILK, 28));
        service.add(new ChocolatePaste(SweetType.CHOCOLATE_PASTE,14,"Belisa", 58, 250, 250, "шоколадно-ореховая"));
        service.add(new ChocolatePaste(SweetType.CHOCOLATE_PASTE,15,"Nutella", 57.3, 180, 200, "ореховая с добавлением какао"));
    }

    @Test
    public void shouldCountGiftWeight(){
        //test
        double actual = service.calculateWeight();
        //assert
        Assert.assertEquals(TOTAL_WEIGHT, actual, 0);
    }

    @Test
    public void shouldSortGiftByName(){
        //prepare
        Comparator<Sweet> comparator = SweetComparatorFactory.getByAttribute(SweetAttribute.NAME);
        //test
        List<Sweet> result = service.sort(comparator);
        String actual = result.get(0).getName();
        //assert
        Assert.assertEquals(FIRST_NAME,actual);
    }

    @Test
    public void shouldSortGiftByWeight(){
        //prepare
        Comparator<Sweet> comparator = SweetComparatorFactory.getByAttribute(SweetAttribute.WEIGHT);
        //test
        List<Sweet> result = service.sort(comparator);
        double actual = result.get(0).getWeight();
        //assert
        Assert.assertEquals(LOWEST_WEIGHT,actual,0);
    }

    @Test
    public void shouldSortGiftBySugarAndWeight(){
        //prepare
        Comparator<Sweet> firstComparator = SweetComparatorFactory.getByAttribute(SweetAttribute.SUGAR);
        Comparator<Sweet> secondComparator = SweetComparatorFactory.getByAttribute(SweetAttribute.WEIGHT);
        //test
        List<Sweet> result = service.sort(firstComparator.thenComparing(secondComparator));
        double actual = result.get(1).getWeight();
        //assert
        Assert.assertEquals(SECOND_WEIGHT,actual,0);
    }

    @Test
    public void shouldFindSweetsBySugarRange(){
        //prepare
        SweetSpecification specification = new SweetSpecificationBySugarRange(57.2,57.4);
        //test
        List<Sweet> result = service.find(specification);
        String actual = result.get(0).getName();
        //assert
        Assert.assertEquals(NUTELLA_NAME,actual);
    }

    @Test
    public void shouldFindSweetsById(){
        //prepare
        SweetSpecification specification = new SweetSpecificationById(11);
        //test
        List<Sweet> result = service.find(specification);
        Sweet actualSweet = result.get(0);
        Sweet expectedSweet = service.getAll().get(10);
        //assert
        Assert.assertEquals(expectedSweet,actualSweet);
    }

    @Test
    public void shouldFindSweetsByNameContains(){
        //prepare
        SweetSpecification specification = new SweetSpecificationByNameContains("ell");
        //test
        List<Sweet> result = service.find(specification);
        int actualSize = result.size();
        //assert
        Assert.assertEquals(SWEETS_NAMES_CONTAINS_ELL_NUMBER,actualSize);
    }

    @Test
    public void shouldFindSweetsByNameStarts(){
        //prepare
        SweetSpecification specification = new SweetSpecificationByNameStarts("l");
        //test
        List<Sweet> result = service.find(specification);
        int actualSize = result.size();
        //assert
        Assert.assertEquals(SWEETS_NAMES_STARTS_L_NUMBER,actualSize);
    }

    @Test
    public void shouldFindSweetsByType(){
        //prepare
        SweetSpecification specification = new SweetSpecificationByType(SweetType.CANDY);
        //test
        List<Sweet> result = service.find(specification);
        int actualSize = result.size();
        //assert
        Assert.assertEquals(SWEETS_TYPE_CANDY_NUMBER,actualSize);
    }

    @Test
    public void shouldFindSweetsByWeightRange(){
        //prepare
        SweetSpecification specification = new SweetSpecificationByWeightRange(10,20);
        //test
        List<Sweet> result = service.find(specification);
        int actualSize = result.size();
        //assert
        Assert.assertEquals(SWEETS_WEIGHT_BETWEEN_TEN_AND_TWENTY_NUMBER,actualSize);
    }
}
