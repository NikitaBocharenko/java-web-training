package by.training.module1.input;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class LineParserImplTest {
    private static final String TEST_LINE_1 = "type:candy|id:8|name:Чупа чупс со вкусом апельсина|sugar:11|weight:12|CandyType:LOLLIPOP|shell:|filling:фруктовая патока";
    private static final String TEST_LINE_2 = "wfwef:4353|:|fgaeg358u9j98jt9g8/srgm9w4|:4w5wgw45t|.43-5ii09k5gelg,5-5:3q,00j94egj4985gjgok";

    @Test
    public void shouldParseLine1(){
        Map<String,String> expected = new HashMap<>();
        expected.put("type","candy");
        expected.put("id","8");
        expected.put("name","Чупа чупс со вкусом апельсина");
        expected.put("sugar","11");
        expected.put("weight","12");
        expected.put("CandyType","LOLLIPOP");
        expected.put("shell","");
        expected.put("filling","фруктовая патока");

        LineParserImpl parser = new LineParserImpl();
        Map<String,String> actual = parser.parseLine(TEST_LINE_1);
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void shouldParseLine2(){
        Map<String,String> expected = new HashMap<>();
        expected.put("wfwef","4353");
        expected.put("fgaeg358u9j98jt9g8/srgm9w4","");
        expected.put(".43-5ii09k5gelg,5-5","3q,00j94egj4985gjgok");

        LineParserImpl parser = new LineParserImpl();
        Map<String,String> actual = parser.parseLine(TEST_LINE_2);
        Assert.assertEquals(expected,actual);
    }
}
