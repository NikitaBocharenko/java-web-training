package by.training.module1.controller;

import by.training.module1.dao.SweetRepository;
import by.training.module1.dao.SweetRepositoryImpl;
import by.training.module1.dao.SweetSpecification;
import by.training.module1.dao.SweetSpecificationBySugarRange;
import by.training.module1.entity.Sweet;
import by.training.module1.input.*;
import by.training.module1.service.SweetService;
import by.training.module1.service.SweetServiceImpl;
import by.training.module1.validation.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.io.IOException;
import java.util.List;

@RunWith(JUnit4.class)
public class SweetControllerTest {
    private static final double CORRECT_FILE_TOTAL_WEIGHT = 1308.2;
    private static final String FIRST_NAME = "Alpen Krone";
    private static final String SPECIFIED_BY_SUGAR_VALUE_NAME = "Nutella";
    private static final int TEN_LINES_CORRECT_EXPECTED_SIZE = 10;
    private static final int FIVE_LINES_CORRECT_EXPECTED_SIZE = 5;
    private static final int BAD_DATA_EXPECTED_SIZE = 0;

    private SweetController firstController;
    private SweetController secondController;
    private SweetController thirdController;
    private SweetController fourthController;

    @Before
    public void setUpFirst() throws IOException {
        SweetRepository repository = new SweetRepositoryImpl();
        SweetService service = new SweetServiceImpl(repository);
        FileValidator fileValidator = new SweetFileValidator();
        FileInput input = new FileInputImpl();
        LineParser parser = new LineParserImpl();
        SweetValidatorFactory validatorFactory = new SweetValidatorFactory();
        SweetBuilderFactory builderFactory = new SweetBuilderFactory();
        firstController = new SweetController(service,fileValidator,input,parser,validatorFactory,builderFactory);
        firstController.readFile(this.getClass().getClassLoader().getResource("sweets_correct.txt").getPath().substring(1));
    }

    @Test
    public void shouldCalculateWeight(){
        double actualWeight = firstController.getWeight();
        Assert.assertEquals(CORRECT_FILE_TOTAL_WEIGHT,actualWeight,0);
    }

    @Test
    public void shouldSortByName(){
        List<Sweet> sortedSweets = firstController.getSortedSweets((o1, o2) -> o1.getName().compareTo(o2.getName()));
        String actualFirstName = sortedSweets.get(0).getName();
        Assert.assertEquals(FIRST_NAME,actualFirstName);
    }

    @Test
    public void shouldFindBySugarRange(){
        SweetSpecification spec = new SweetSpecificationBySugarRange(57.2,57.4);
        List<Sweet> sweetsBySugarRange = firstController.getSpecifiedSweets(spec);
        String actualFoundName = sweetsBySugarRange.get(0).getName();
        Assert.assertEquals(SPECIFIED_BY_SUGAR_VALUE_NAME, actualFoundName);
    }

    @Before
    public void setUpSecond() throws IOException {
        SweetRepository repository = new SweetRepositoryImpl();
        SweetService service = new SweetServiceImpl(repository);
        FileValidator fileValidator = new SweetFileValidator();
        FileInput input = new FileInputImpl();
        LineParser parser = new LineParserImpl();
        SweetValidatorFactory validatorFactory = new SweetValidatorFactory();
        SweetBuilderFactory builderFactory = new SweetBuilderFactory();
        secondController = new SweetController(service,fileValidator,input,parser,validatorFactory,builderFactory);
        secondController.readFile(this.getClass().getClassLoader().getResource("sweets_10_lines_correct.txt").getPath().substring(1));
    }

    @Test
    public void shouldReturnTenEntities(){
        int actualListSize = secondController.getSweets().size();
        Assert.assertEquals(TEN_LINES_CORRECT_EXPECTED_SIZE, actualListSize);
    }

    @Before
    public void setUpThird() throws IOException {
        SweetRepository repository = new SweetRepositoryImpl();
        SweetService service = new SweetServiceImpl(repository);
        FileValidator fileValidator = new SweetFileValidator();
        FileInput input = new FileInputImpl();
        LineParser parser = new LineParserImpl();
        SweetValidatorFactory validatorFactory = new SweetValidatorFactory();
        SweetBuilderFactory builderFactory = new SweetBuilderFactory();
        thirdController = new SweetController(service,fileValidator,input,parser,validatorFactory,builderFactory);
        thirdController.readFile(this.getClass().getClassLoader().getResource("sweets_5_lines_correct.txt").getPath().substring(1));
    }

    @Test
    public void shouldReturnFiveEntities(){
        int actualListSize = thirdController.getSweets().size();
        Assert.assertEquals(FIVE_LINES_CORRECT_EXPECTED_SIZE, actualListSize);
    }

    @Before
    public void setUpFourth() throws IOException {
        SweetRepository repository = new SweetRepositoryImpl();
        SweetService service = new SweetServiceImpl(repository);
        FileValidator fileValidator = new SweetFileValidator();
        FileInput input = new FileInputImpl();
        LineParser parser = new LineParserImpl();
        SweetValidatorFactory validatorFactory = new SweetValidatorFactory();
        SweetBuilderFactory builderFactory = new SweetBuilderFactory();
        fourthController = new SweetController(service,fileValidator,input,parser,validatorFactory,builderFactory);
        fourthController.readFile(this.getClass().getClassLoader().getResource("sweets_bad_data.txt").getPath().substring(1));
    }

    @Test
    public void shouldReturnZeroEntities(){
        int actualListSize = fourthController.getSweets().size();
        Assert.assertEquals(BAD_DATA_EXPECTED_SIZE, actualListSize);
    }
}
