package by.training.module1.input;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RunWith(JUnit4.class)
public class FileInputImplTest {
    private final static String TEST_RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private final static String NON_EXISTING_FILE_NAME = "UUUUUUUUUUUUUUUUUU.txt";
    private final static int CORRECT_FILE_LINES_NUMBER = 15;

    @Test
    public void shouldGetFileLines(){
        FileInput input = new FileInputImpl();
        List<String> lines = null;
        try {
            lines = input.getLines(this.getClass().getClassLoader().getResource("sweets_correct.txt").getPath().substring(1));
        } catch (IOException e) {
            Assert.fail();
        }
        Assert.assertEquals(CORRECT_FILE_LINES_NUMBER, lines.size());
    }

    @Test(expected = IOException.class)
    public void shouldThrowIOException() throws IOException{
        FileInputImpl input = new FileInputImpl();
        input.getLines(TEST_RESOURCES_PATH + NON_EXISTING_FILE_NAME);
    }
}
