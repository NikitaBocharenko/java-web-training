package by.training.module1.validation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.io.File;
import java.io.IOException;
import java.util.Set;

@RunWith(JUnit4.class)
public class SweetFileValidatorTest {
    private final static String TEST_RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private final static String NON_EXISTING_FILE_NAME = "UUUUUUUUUUUUUUUUUU.txt";

    @Test
    public void shouldValidateFile(){
        //prepare
        SweetFileValidator validator = new SweetFileValidator();
        //test
        ValidationResult vr = null;
        try {
            vr = validator.validateFile( this.getClass().getClassLoader().getResource("sweets_correct.txt").getPath().substring(1));
        } catch (IOException e) {
            Assert.fail();
        }
        boolean isValid = vr.isValid();
        //assert
        Assert.assertTrue(isValid);
    }

    @Test
    public void shouldGetFileNotExistsError(){
        //prepare
        SweetFileValidator validator = new SweetFileValidator();
        //test
        ValidationResult vr = null;
        try {
            vr = validator.validateFile( TEST_RESOURCES_PATH + NON_EXISTING_FILE_NAME);
        } catch (IOException e) {
            Assert.fail();
        }
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("path");
        //assert
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(101,"File not found")));
    }

    @Test
    public void shouldGetBadDataInFileError(){
        //prepare
        SweetFileValidator validator = new SweetFileValidator();
        //test
        ValidationResult vr = null;
        try {
            vr = validator.validateFile( this.getClass().getClassLoader().getResource("sweets_bad_data.txt").getPath().substring(1));
        } catch (IOException e) {
            Assert.fail();
        }
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("path");
        //assert
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(202,"Bad data in file")));
    }

    @Test(expected = IOException.class)
    public void shouldThrowIOException() throws IOException {
        //prepare
        SweetFileValidator validator = new SweetFileValidator();
        //test && assert
        validator.validateFile( this.getClass().getClassLoader().getResource("sweets_io_exception.txt").getPath().substring(1));
    }
}
