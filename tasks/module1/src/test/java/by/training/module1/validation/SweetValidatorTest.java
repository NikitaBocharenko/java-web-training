package by.training.module1.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RunWith(JUnit4.class)
public class SweetValidatorTest {
    private Map<String, String> candyData;
    private Map<String, String> chocolateData;
    private Map<String, String> chocolatePasteData;

    @Before
    public void setUpCandyData() {
        candyData = new HashMap<>();
        candyData.put("type", "candy");
        candyData.put("id", "1");
        candyData.put("name", "Испанская ночь");
        candyData.put("sugar", "8");
        candyData.put("weight", "10");
        candyData.put("candyType", "CHOCOLATE");
        candyData.put("shell", "кондитерская глазурь");
        candyData.put("filling", "патока");
    }

    @Test
    public void shouldValidateData() {
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Assert.assertTrue(vr.isValid());
    }

    @Test
    public void shouldGetMissingTypeError() {
        candyData.remove("type");
        ValidationResult vr = SweetValidator.validateType(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("type");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(300, "Missing parameter: type")));
    }

    @Test
    public void shouldGetWrongTypeError() {
        candyData.put("type", "q34awf3");
        ValidationResult vr = SweetValidator.validateType(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("type");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(301, "Wrong value of parameter: type")));
    }

    @Test
    public void shouldGetIDMissingError() {
        candyData.remove("id");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("id");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(400, "Missing parameter: id")));
    }

    @Test
    public void shouldGetIDNotNumberError() {
        candyData.put("id", "4gw44g5");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("id");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(401, "Wrong value of parameter: id")));
    }

    @Test
    public void shouldGetIDLessZeroError() {
        candyData.put("id", "-1");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("id");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(402, "Parameter should be greater or equals than 0: id")));
    }

    @Test
    public void shouldGetNameMissingError() {
        candyData.remove("name");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("name");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(500, "Missing parameter: name")));
    }

    @Test
    public void shouldGetSugarMissingError() {
        candyData.remove("sugar");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("sugar");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(600, "Missing parameter: sugar")));
    }

    @Test
    public void shouldGetSugarNotNumberError() {
        candyData.put("sugar", "a34ff");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("sugar");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(601, "Wrong value of parameter: sugar")));
    }

    @Test
    public void shouldGetSugarEqualsOrLessZeroError() {
        candyData.put("sugar", "0");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("sugar");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(602, "Parameter should be greater than 0: sugar")));
    }

    @Test
    public void shouldGetWeightMissingError() {
        candyData.remove("weight");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("weight");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(700, "Missing parameter: weight")));
    }

    @Test
    public void shouldGetWeightNotNumberError() {
        candyData.put("weight", "34aw43f3");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("weight");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(701, "Wrong value of parameter: weight")));
    }

    @Test
    public void shouldGetWeightEqualsOrLessZeroError() {
        candyData.put("weight", "0");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("weight");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(702, "Parameter should be greater than 0: weight")));
    }

    @Test
    public void shouldGetCandyTypeMissingError() {
        candyData.remove("candyType");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("candyType");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1200, "Missing parameter: candyType")));
    }

    @Test
    public void shouldGetWrongCandyTypeError() {
        candyData.put("candyType", "uhv8w9j");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("candyType");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1201, "Wrong value of parameter: candyType")));
    }

    @Test
    public void shouldGetShellMissingError() {
        candyData.remove("shell");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("shell");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1300, "Missing parameter: shell")));
    }

    @Test
    public void shouldGetFillingMissingError() {
        candyData.remove("filling");
        SweetValidator validator = new CandyValidator();
        ValidationResult vr = validator.validate(candyData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("filling");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1400, "Missing parameter: filling")));
    }

    @Before
    public void setUpChocolateData() {
        chocolateData = new HashMap<>();
        chocolateData.put("type", "chocolate");
        chocolateData.put("id", "11");
        chocolateData.put("name", "Michelle");
        chocolateData.put("sugar", "49");
        chocolateData.put("weight", "90");
        chocolateData.put("chocolateType", "BITTER");
        chocolateData.put("cocoaContent", "68");
    }

    @Test
    public void shouldGetChocolateTypeMissingError() {
        chocolateData.remove("chocolateType");
        SweetValidator validator = new ChocolateValidator();
        ValidationResult vr = validator.validate(chocolateData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("chocolateType");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(800, "Missing parameter: chocolateType")));
    }

    @Test
    public void shouldGetWrongChocolateTypeError() {
        chocolateData.put("chocolateType", "uhv8w9j");
        SweetValidator validator = new ChocolateValidator();
        ValidationResult vr = validator.validate(chocolateData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("chocolateType");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(801, "Wrong value of parameter: chocolateType")));
    }

    @Test
    public void shouldGetCocoaContentMissingError() {
        chocolateData.remove("cocoaContent");
        SweetValidator validator = new ChocolateValidator();
        ValidationResult vr = validator.validate(chocolateData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("cocoaContent");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(900, "Missing parameter: cocoaContent")));
    }

    @Test
    public void shouldGetCocoaContentNotNumberError() {
        chocolateData.put("cocoaContent", "uhv8w9j");
        SweetValidator validator = new ChocolateValidator();
        ValidationResult vr = validator.validate(chocolateData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("cocoaContent");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(901, "Wrong value of parameter: cocoaContent")));
    }

    @Test
    public void shouldGetCocoaContentWrongNumberError() {
        chocolateData.put("cocoaContent", "103");
        SweetValidator validator = new ChocolateValidator();
        ValidationResult vr = validator.validate(chocolateData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("cocoaContent");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(902, "Parameter should be greater than 0 and less than 100: cocoaContent")));
    }

    @Before
    public void setUpChocolatePasteData() {
        chocolatePasteData = new HashMap<>();
        chocolatePasteData.put("type", "chocolate_paste");
        chocolatePasteData.put("id", "14");
        chocolatePasteData.put("name", "Belisa");
        chocolatePasteData.put("sugar", "58");
        chocolatePasteData.put("weight", "250");
        chocolatePasteData.put("volume", "250");
        chocolatePasteData.put("composition", "шоколадно-ореховая");
    }

    @Test
    public void shouldGetVolumeMissingError() {
        chocolatePasteData.remove("volume");
        SweetValidator validator = new ChocolatePasteValidator();
        ValidationResult vr = validator.validate(chocolatePasteData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("volume");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1000, "Missing parameter: volume")));
    }

    @Test
    public void shouldGetVolumeNotNumberError() {
        chocolatePasteData.put("volume", "34rf343");
        SweetValidator validator = new ChocolatePasteValidator();
        ValidationResult vr = validator.validate(chocolatePasteData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("volume");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1001, "Wrong value of parameter: volume")));
    }

    @Test
    public void shouldGetVolumeLessOrEqualsZeroError() {
        chocolatePasteData.put("volume", "0");
        SweetValidator validator = new ChocolatePasteValidator();
        ValidationResult vr = validator.validate(chocolatePasteData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("volume");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1002, "Parameter should be greater than 0: volume")));
    }

    @Test
    public void shouldGetCompositionMissingError() {
        chocolatePasteData.remove("composition");
        SweetValidator validator = new ChocolatePasteValidator();
        ValidationResult vr = validator.validate(chocolatePasteData);
        Set<ValidationMessage> validationMessages = vr.getErrorsByType("composition");
        Assert.assertTrue(validationMessages.contains(new ValidationMessage(1100, "Missing parameter: composition")));
    }
}
