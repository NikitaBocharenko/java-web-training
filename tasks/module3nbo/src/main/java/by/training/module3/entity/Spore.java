package by.training.module3.entity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Spore extends Plant {
    private SporeType sporeType;

    public Spore() {
    }

    public Spore(String id, String name, SoilType soilType, String origin, List<String> visualParameters, List<String> growingTips, MultiplyingType multiplyingType, Date plantingDate, SporeType sporeType) {
        super(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate);
        this.sporeType = sporeType;
    }

    public SporeType getSporeType() {
        return sporeType;
    }

    public void setSporeType(SporeType sporeType) {
        this.sporeType = sporeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Spore spore = (Spore) o;
        return sporeType == spore.sporeType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sporeType);
    }

    @Override
    public String toString() {
        return "Spore{" +
                super.toString() +
                ", sporeType=" + sporeType +
                '}';
    }
}
