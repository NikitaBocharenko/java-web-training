package by.training.module3.repo;

import by.training.module3.entity.Plant;

public interface PlantComparator {
    int compare(Plant p1, Plant p2);
}
