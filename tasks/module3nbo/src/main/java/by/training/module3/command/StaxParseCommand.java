package by.training.module3.command;

import by.training.module3.entity.Plant;
import by.training.module3.parser.ParserException;
import by.training.module3.parser.StaxParser;

import java.util.List;

public class StaxParseCommand implements ParseCommand {
    private StaxParser parser;

    public StaxParseCommand(StaxParser parser) {
        this.parser = parser;
    }

    @Override
    public List<Plant> parse(String path) throws ParseCommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new ParseCommandException("Exception was thrown during parsing", e);
        }
    }
}
