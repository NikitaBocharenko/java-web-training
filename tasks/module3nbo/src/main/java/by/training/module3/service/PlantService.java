package by.training.module3.service;

import by.training.module3.entity.Plant;
import by.training.module3.repo.PlantComparator;
import by.training.module3.repo.PlantSpecification;

import java.util.List;

public interface PlantService {
    void save(Plant plant);
    List<Plant> loadAll();
    List<Plant> loadSpecified(PlantSpecification specification);
    List<Plant> loadSorted(PlantComparator comparator);
}
