package by.training.module3.command;

import by.training.module3.entity.Plant;

import java.util.List;

public interface ParseCommand {
    List<Plant> parse(String path) throws ParseCommandException;
}
