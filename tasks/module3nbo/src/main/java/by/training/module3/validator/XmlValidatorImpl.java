package by.training.module3.validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XmlValidatorImpl implements XmlValidator {
    private String xsdPath;
    private static final Logger log = Logger.getLogger(XmlValidatorImpl.class);

    public XmlValidatorImpl(String xsdPath) {
        this.xsdPath = xsdPath;
    }

    @Override
    public ValidationResult validate(String xmlPath){
        ValidationResult validationResult = new ValidationResult();
        XmlValidationErrorHandler errorHandler = new XmlValidationErrorHandler(validationResult);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source xmlFile = new StreamSource(new File(xmlPath));
        try {
            Schema schema = schemaFactory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            validator.setErrorHandler(errorHandler);
            validator.validate(xmlFile);
        } catch (SAXException e) {
            log.fatal(e.getMessage());
        } catch (IOException e){
            validationResult.addError("xml", e.getMessage());
            log.fatal(e.getMessage());
        }
        return errorHandler.getValidationResult();
    }
}
