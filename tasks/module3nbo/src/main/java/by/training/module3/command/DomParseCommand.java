package by.training.module3.command;

import by.training.module3.entity.Plant;
import by.training.module3.parser.DomParser;
import by.training.module3.parser.ParserException;

import java.util.List;

public class DomParseCommand implements ParseCommand {
    private DomParser parser;

    public DomParseCommand(DomParser parser) {
        this.parser = parser;
    }

    @Override
    public List<Plant> parse(String path) throws ParseCommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new ParseCommandException("Exception was thrown during parsing", e);
        }
    }
}
