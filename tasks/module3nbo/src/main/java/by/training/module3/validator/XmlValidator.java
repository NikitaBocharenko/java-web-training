package by.training.module3.validator;

public interface XmlValidator {
    ValidationResult validate(String xmlPath);
}
