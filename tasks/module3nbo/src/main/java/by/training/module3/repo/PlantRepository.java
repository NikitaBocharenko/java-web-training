package by.training.module3.repo;

import by.training.module3.entity.Plant;

import java.util.List;

public interface PlantRepository {
    void add(Plant plant);
    void remove(Plant plant);
    void update(Plant plant);
    List<Plant> find(PlantSpecification specification);
    List<Plant> sort(PlantComparator comparator);
    List<Plant> getAll();
}
