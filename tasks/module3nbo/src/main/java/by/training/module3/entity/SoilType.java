package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SoilType {
    PODZOLIC, DIRT, SOD_PODZOLIC, WATER;

    public static Optional<SoilType> fromString(String type) {
        return Stream.of(SoilType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
