package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SeaweedType {
    GREEN, BROWN, RED, CHAR;

    public static Optional<SeaweedType> fromString(String type) {
        return Stream.of(SeaweedType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
