package by.training.module3.entity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Seed extends Plant {
    private SeedType seedType;

    public Seed() {
    }

    public Seed(String id, String name, SoilType soilType, String origin, List<String> visualParameters, List<String> growingTips, MultiplyingType multiplyingType, Date plantingDate, SeedType seedType) {
        super(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate);
        this.seedType = seedType;
    }

    public SeedType getSeedType() {
        return seedType;
    }

    public void setSeedType(SeedType seedType) {
        this.seedType = seedType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Seed seed = (Seed) o;
        return seedType == seed.seedType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), seedType);
    }

    @Override
    public String toString() {
        return "Seed{" +
                super.toString() +
                ", seedType=" + seedType +
                '}';
    }
}
