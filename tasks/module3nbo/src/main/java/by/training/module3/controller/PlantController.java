package by.training.module3.controller;

import by.training.module3.command.CommandProvider;
import by.training.module3.command.ParseCommand;
import by.training.module3.command.ParseCommandException;
import by.training.module3.command.ParseCommandType;
import by.training.module3.entity.Plant;
import by.training.module3.service.PlantService;
import by.training.module3.validator.FileValidator;
import by.training.module3.validator.ValidationResult;
import by.training.module3.validator.XmlValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class PlantController {
    private FileValidator fileValidator;
    private XmlValidator xmlValidator;
    private CommandProvider commandProvider;
    private PlantService service;

    private static final Logger log = Logger.getLogger(PlantController.class);

    public PlantController(FileValidator fileValidator, XmlValidator xmlValidator, CommandProvider commandProvider, PlantService service) {
        this.fileValidator = fileValidator;
        this.xmlValidator = xmlValidator;
        this.commandProvider = commandProvider;
        this.service = service;
    }

    public List<Plant> parseXmlFile(String path, ParseCommandType commandType){
        List<Plant> result;
        ValidationResult fileValidationResult = fileValidator.validate(path);
        if (!fileValidationResult.isValid()) {
            log.error("File path is incorrect: " + fileValidationResult);
            return new ArrayList<>();
        }
        ValidationResult xmlValidationResult = xmlValidator.validate(path);
        if (!xmlValidationResult.isValid()){
            log.error("Xml file is invalid: " + xmlValidationResult);
            return new ArrayList<>();
        }
        ParseCommand command = commandProvider.getByType(commandType);
        try {
            result = command.parse(path);
        } catch (ParseCommandException e) {
            log.error("Exception was thrown during executing command: " + e.getMessage());
            return new ArrayList<>();
        }
        result.forEach(service::save);
        return service.loadAll();
    }
}
