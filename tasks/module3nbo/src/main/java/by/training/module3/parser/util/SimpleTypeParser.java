package by.training.module3.parser.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleTypeParser {
    private SimpleTypeParser(){}

    public static Date parseStringToDate(String value, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            return dateFormat.parse(value);
        } catch (ParseException e) {
            return null;
        }
    }
}
