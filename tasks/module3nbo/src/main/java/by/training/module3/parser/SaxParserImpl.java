package by.training.module3.parser;

import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.Seed;
import by.training.module3.entity.SeedType;
import by.training.module3.entity.SoilType;
import by.training.module3.entity.Spore;
import by.training.module3.entity.SporeType;
import by.training.module3.parser.util.SimpleTypeParser;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaxParserImpl extends DefaultHandler implements SaxParser {
    private List<Plant> parsedPlants;
    private Plant currentPlant;
    private String currentElementName;
    private List<String> visualParameters;
    private List<String> growingTips;

    private static final Logger log = Logger.getLogger(SaxParserImpl.class);

    @Override
    public List<Plant> parse(String xmlPath) throws ParserException{
        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = parserFactory.newSAXParser();
            parsedPlants = new ArrayList<>();
            parser.parse(xmlPath, this);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            log.error(e.getMessage());
            throw new ParserException("Exception was thrown: ", e);
        }
        return parsedPlants;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes){
        currentElementName = qName;
        switch (qName) {
            case "seaweed":
                currentPlant = new Seaweed();
                setAttributeValues(attributes);
                break;
            case "seed":
                currentPlant = new Seed();
                setAttributeValues(attributes);
                break;
            case "spore":
                currentPlant = new Spore();
                setAttributeValues(attributes);
                break;
            case "visualParameters":
                visualParameters = new ArrayList<>();
                break;
            case "growingTips":
                growingTips = new ArrayList<>();
                break;
            default:break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length){
        if (currentElementName != null){
            String value = new String(ch, start, length);
            switch (currentElementName){
                case "id": currentPlant.setId(value); break;
                case "name": currentPlant.setName(value); break;
                case "origin": currentPlant.setOrigin(value); break;
                case "visualParameter": visualParameters.add(value); break;
                case "growingTip": growingTips.add(value); break;
                case "plantingDate": setPlantingDate(value); break;
                case "seaweedType": setSeaweedType(value); break;
                case "seedType": setSeedType(value); break;
                case "sporeType": setSporeType(value); break;
                default: break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName){
        currentElementName = null;
        switch(qName){
            case "spore":
            case "seaweed":
            case "seed": parsedPlants.add(currentPlant);break;
            case "visualParameters": currentPlant.setVisualParameters(visualParameters);break;
            case "growingTips": currentPlant.setGrowingTips(growingTips);break;
            default: break;
        }
    }

    private void setAttributeValues(Attributes attributes){
        String multiplyingTypeString = attributes.getValue("multiplyingType");
        MultiplyingType multiplyingType = MultiplyingType.fromString(multiplyingTypeString).orElse(null);
        currentPlant.setMultiplyingType(multiplyingType);

        String soilTypeString = attributes.getValue("soilType");
        SoilType soilType = SoilType.fromString(soilTypeString).orElseGet(()->{
            log.error("Xml file is invalid: invalid soil type");
            return null;
        });
        currentPlant.setSoilType(soilType);
    }

    private void setPlantingDate(String value){
        Date plantingDate = SimpleTypeParser.parseStringToDate(value, "yyyy-MM-dd'T'HH:mm:ss");
        if (plantingDate == null) {
            log.error("Xml file is invalid: invalid planting date");
        }
        currentPlant.setPlantingDate(plantingDate);
    }

    private void setSeaweedType(String value){
        if (currentPlant instanceof Seaweed){
            SeaweedType seaweedType = SeaweedType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid seaweed type");
                return null;
            });
            ((Seaweed) currentPlant).setSeaweedType(seaweedType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'seaweedType'");
        }
    }

    private void setSeedType(String value){
        if (currentPlant instanceof Seed){
            SeedType seedType = SeedType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid seed type");
                return null;
            });
            ((Seed) currentPlant).setSeedType(seedType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'seedType'");
        }
    }

    private void setSporeType(String value){
        if (currentPlant instanceof Spore){
            SporeType sporeType = SporeType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid spore type");
                return null;
            });
            ((Spore) currentPlant).setSporeType(sporeType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'sporeType'");
        }
    }
}
