package by.training.module3.service;

import by.training.module3.entity.Plant;
import by.training.module3.repo.PlantComparator;
import by.training.module3.repo.PlantRepository;
import by.training.module3.repo.PlantSpecification;

import java.util.List;

public class PlantServiceImpl implements PlantService {
    private PlantRepository repository;

    public PlantServiceImpl(PlantRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Plant plant) {
        this.repository.add(plant);
    }

    @Override
    public List<Plant> loadAll() {
        return this.repository.getAll();
    }

    @Override
    public List<Plant> loadSpecified(PlantSpecification specification) {
        return this.repository.find(specification);
    }

    @Override
    public List<Plant> loadSorted(PlantComparator comparator) {
        return this.repository.sort(comparator);
    }
}
