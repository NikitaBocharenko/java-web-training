package by.training.module3.entity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Seaweed extends Plant {
    private SeaweedType seaweedType;

    public Seaweed() {
    }

    public Seaweed(String id, String name, SoilType soilType, String origin, List<String> visualParameters, List<String> growingTips, MultiplyingType multiplyingType, Date plantingDate, SeaweedType seaweedType) {
        super(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate);
        this.seaweedType = seaweedType;
    }

    public SeaweedType getSeaweedType() {
        return seaweedType;
    }

    public void setSeaweedType(SeaweedType seaweedType) {
        this.seaweedType = seaweedType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Seaweed seaweed = (Seaweed) o;
        return seaweedType == seaweed.seaweedType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), seaweedType);
    }

    @Override
    public String toString() {
        return "Seaweed{" +
                super.toString() +
                ", seaweedType=" + seaweedType +
                '}';
    }
}
