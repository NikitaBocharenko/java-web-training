package by.training.module3.command;

public interface CommandProvider {
    ParseCommand getByType(ParseCommandType type);
    void addCommand(ParseCommandType type, ParseCommand command);
}
