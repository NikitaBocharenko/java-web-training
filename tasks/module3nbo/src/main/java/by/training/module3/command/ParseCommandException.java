package by.training.module3.command;

public class ParseCommandException extends Exception {
    public ParseCommandException(String message){super(message);}
    public ParseCommandException(String message, Throwable exception){super(message, exception);}
}
