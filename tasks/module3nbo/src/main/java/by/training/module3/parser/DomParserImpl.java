package by.training.module3.parser;

import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.PlantType;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.Seed;
import by.training.module3.entity.SeedType;
import by.training.module3.entity.SoilType;
import by.training.module3.entity.Spore;
import by.training.module3.entity.SporeType;
import by.training.module3.parser.util.SimpleTypeParser;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DomParserImpl implements DomParser {
    private DocumentBuilder documentBuilder;

    private static final Logger log = Logger.getLogger(DomParserImpl.class);

    public DomParserImpl() throws ParserException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new ParserException("Exception was thrown when parser created", e);
        }
    }

    @Override
    public List<Plant> parse(String xmlPath) throws ParserException{
        List<Plant> result = new ArrayList<>();
        try {
            Document document = documentBuilder.parse(xmlPath);
            Element root = document.getDocumentElement();
            NodeList plantsList = root.getChildNodes();
            for (int i = 0; i < plantsList.getLength(); i++){
                Node childNode = plantsList.item(i);
                if (childNode.getNodeType() == Node.ELEMENT_NODE){
                    Element plantElement = (Element) childNode;
                    Plant plant = this.parseElement(plantElement);
                    result.add(plant);
                }
            }
        } catch (IOException | SAXException e) {
            log.error(e.getMessage());
            throw new ParserException("Xml file is invalid", e);
        }
        return result;
    }

    private Plant parseElement(Element plantElement) throws ParserException{
        Plant plant;

        String plantTypeString = plantElement.getNodeName();
        PlantType plantType = PlantType.fromString(plantTypeString)
                .orElseThrow(()->new ParserException("Xml file is invalid: invalid plant type"));

        String multiplyingTypeString = plantElement.getAttribute("multiplyingType");
        MultiplyingType multiplyingType = MultiplyingType.fromString(multiplyingTypeString).orElse(null);

        String soilTypeString = plantElement.getAttribute("soilType");
        SoilType soilType = SoilType.fromString(soilTypeString)
                .orElseThrow(()->new ParserException("Xml file is invalid: invalid soil type"));

        String id = this.getElementTextContent(plantElement, "id");
        String name = this.getElementTextContent(plantElement, "name");
        String origin = this.getElementTextContent(plantElement, "origin");
        List<String> visualParameters = this.getListContent(plantElement, "visualParameters");
        List<String> growingTips = this.getListContent(plantElement, "growingTips");

        String plantingDateString = this.getElementTextContent(plantElement, "plantingDate");
        Date plantingDate = SimpleTypeParser.parseStringToDate(plantingDateString, "yyyy-MM-dd'T'HH:mm:ss");
        if (plantingDate == null) {
            throw new ParserException("Xml file is invalid: invalid planting date");
        }

        switch (plantType){
            case SEED:
                String seedTypeString = this.getElementTextContent(plantElement, "seedType");
                SeedType seedType = SeedType.fromString(seedTypeString).orElseThrow(()->new ParserException("Xml file is invalid: invalid seed type"));
                plant = new Seed(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate, seedType );
                break;
            case SPORE:
                String sporeTypeString = this.getElementTextContent(plantElement, "sporeType");
                SporeType sporeType = SporeType.fromString(sporeTypeString).orElseThrow(()->new ParserException("Xml file is invalid: invalid spore type"));
                plant = new Spore(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate, sporeType);
                break;
            case SEAWEED:
                String seaweedTypeString = this.getElementTextContent(plantElement, "seaweedType");
                SeaweedType seaweedType = SeaweedType.fromString(seaweedTypeString).orElseThrow(()->new ParserException("Xml file is invalid: invalid seaweed type"));
                plant = new Seaweed(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate, seaweedType);
                break;
            default:
                log.error("unregistered plantType found");
                throw new IllegalArgumentException("DomParserImpl: unregistered plantType found");
        }

        return plant;
    }

    private List<String> getListContent(Element plantElement, String plantTagName) {
        List<String> list = new ArrayList<>();
        NodeList listNodes = plantElement.getElementsByTagName(plantTagName);
        Element listElement = (Element) listNodes.item(0);
        NodeList childNodes = listElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++){
            Node childNode = childNodes.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE){
                String textContent = childNode.getTextContent();
                list.add(textContent);
            }
        }
        return list;
    }

    private String getElementTextContent(Element plantElement, String plantTagName) {
        NodeList list = plantElement.getElementsByTagName(plantTagName);
        Node node = list.item(0);
        return node.getTextContent();
    }
}
