package by.training.module3.command;

import by.training.module3.entity.Plant;
import by.training.module3.parser.ParserException;
import by.training.module3.parser.SaxParser;

import java.util.List;

public class SaxParseCommand implements ParseCommand {
    private SaxParser parser;

    public SaxParseCommand(SaxParser parser) {
        this.parser = parser;
    }

    @Override
    public List<Plant> parse(String path) throws ParseCommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new ParseCommandException("Exception was thrown during parsing", e);
        }
    }
}
