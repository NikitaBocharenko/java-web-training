package by.training.module3.parser;

import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.Seed;
import by.training.module3.entity.SeedType;
import by.training.module3.entity.SoilType;
import by.training.module3.entity.Spore;
import by.training.module3.entity.SporeType;
import by.training.module3.parser.util.SimpleTypeParser;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StaxParserImpl implements StaxParser {
    private XMLInputFactory factory;
    private List<Plant> parsedPlants;
    private Plant currentPlant;
    private List<String> visualParameters;
    private List<String> growingTips;
    private String tagContent;

    private static final Logger log = Logger.getLogger(StaxParserImpl.class);

    public StaxParserImpl(){
        factory = XMLInputFactory.newInstance();
        parsedPlants = new ArrayList<>();
    }

    @Override
    public List<Plant> parse(String xmlPath) throws ParserException {
        Source xmlFile = new StreamSource(new File(xmlPath));
        XMLStreamReader reader;
        try {
            reader = factory.createXMLStreamReader(xmlFile);
            while (reader.hasNext()){
                int event = reader.next();
                switch(event){
                    case XMLStreamConstants.START_ELEMENT: this.startElementProcessing(reader);break;
                    case XMLStreamConstants.CHARACTERS: tagContent = reader.getText().trim();break;
                    case XMLStreamConstants.END_ELEMENT: this.finishElementProcessing(reader);break;
                    default:break;
                }
            }
        } catch (XMLStreamException e) {
            throw new ParserException("Exception was thrown: ", e);
        }
        return parsedPlants;
    }

    private void startElementProcessing(XMLStreamReader reader){
        switch (reader.getLocalName()){
            case "seed":
                currentPlant = new Seed();
                this.setAttributeValues(reader);
                break;
            case "seaweed":
                currentPlant = new Seaweed();
                this.setAttributeValues(reader);
                break;
            case "spore":
                currentPlant = new Spore();
                this.setAttributeValues(reader);
                break;
            case "visualParameters":
                visualParameters = new ArrayList<>();
                break;
            case "growingTips":
                growingTips = new ArrayList<>();
                break;
            default:break;
        }
    }

    private void finishElementProcessing(XMLStreamReader reader){
        switch(reader.getLocalName()){
            case "spore":
            case "seaweed":
            case "seed": parsedPlants.add(currentPlant); break;
            case "visualParameters": currentPlant.setVisualParameters(visualParameters); break;
            case "growingTips": currentPlant.setGrowingTips(growingTips); break;
            case "id": currentPlant.setId(tagContent); break;
            case "name": currentPlant.setName(tagContent); break;
            case "origin": currentPlant.setOrigin(tagContent); break;
            case "visualParameter": visualParameters.add(tagContent); break;
            case "growingTip": growingTips.add(tagContent); break;
            case "plantingDate": setPlantingDate(tagContent); break;
            case "seaweedType": setSeaweedType(tagContent); break;
            case "seedType": setSeedType(tagContent); break;
            case "sporeType": setSporeType(tagContent); break;
            default: break;
        }
    }

    private void setAttributeValues(XMLStreamReader reader){
        String multiplyingTypeString = reader.getAttributeValue("","multiplyingType");
        MultiplyingType multiplyingType = MultiplyingType.fromString(multiplyingTypeString).orElse(null);
        currentPlant.setMultiplyingType(multiplyingType);

        String soilTypeString = reader.getAttributeValue("","soilType");
        SoilType soilType = SoilType.fromString(soilTypeString).orElseGet(()->{
            log.error("Xml file is invalid: invalid soil type");
            return null;
        });
        currentPlant.setSoilType(soilType);
    }

    private void setPlantingDate(String value){
        Date plantingDate = SimpleTypeParser.parseStringToDate(value, "yyyy-MM-dd'T'HH:mm:ss");
        if (plantingDate == null) {
            log.error("Xml file is invalid: invalid planting date");
        }
        currentPlant.setPlantingDate(plantingDate);
    }

    private void setSeaweedType(String value){
        if (currentPlant instanceof Seaweed){
            SeaweedType seaweedType = SeaweedType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid seaweed type");
                return null;
            });
            ((Seaweed) currentPlant).setSeaweedType(seaweedType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'seaweedType'");
        }
    }

    private void setSeedType(String value){
        if (currentPlant instanceof Seed){
            SeedType seedType = SeedType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid seed type");
                return null;
            });
            ((Seed) currentPlant).setSeedType(seedType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'seedType'");
        }
    }

    private void setSporeType(String value){
        if (currentPlant instanceof Spore){
            SporeType sporeType = SporeType.fromString(value).orElseGet(()->{
                log.error("Xml file is invalid: invalid spore type");
                return null;
            });
            ((Spore) currentPlant).setSporeType(sporeType);
        } else {
            log.error("Xml file is invalid: unexpected tag 'sporeType'");
        }
    }


}
