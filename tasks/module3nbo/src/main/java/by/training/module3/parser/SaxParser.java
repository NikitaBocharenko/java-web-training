package by.training.module3.parser;

import by.training.module3.entity.Plant;

import java.util.List;

public interface SaxParser {
    List<Plant> parse(String xmlPath) throws ParserException;
}
