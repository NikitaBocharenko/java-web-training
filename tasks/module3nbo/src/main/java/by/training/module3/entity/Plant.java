package by.training.module3.entity;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public abstract class Plant {
    private String id;
    private String name;
    private SoilType soilType;
    private String origin;
    private List<String> visualParameters;
    private List<String> growingTips;
    private MultiplyingType multiplyingType;
    private Date plantingDate;

    public Plant() {
    }

    public Plant(String id, String name, SoilType soilType, String origin, List<String> visualParameters, List<String> growingTips, MultiplyingType multiplyingType, Date plantingDate) {
        this.id = id;
        this.name = name;
        this.soilType = soilType;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplyingType = multiplyingType == null ? MultiplyingType.LEAF : multiplyingType; //optional
        this.plantingDate = (Date)plantingDate.clone();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public SoilType getSoilType() {
        return soilType;
    }

    public String getOrigin() {
        return origin;
    }

    public List<String> getVisualParameters() {
        return visualParameters;
    }

    public List<String> getGrowingTips() {
        return growingTips;
    }

    public MultiplyingType getMultiplyingType() {
        return multiplyingType;
    }

    public Date getPlantingDate() {
        return (Date)plantingDate.clone();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoilType(SoilType soilType) {
        this.soilType = soilType;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVisualParameters(List<String> visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setGrowingTips(List<String> growingTips) {
        this.growingTips = growingTips;
    }

    public void setMultiplyingType(MultiplyingType multiplyingType) {
        this.multiplyingType = multiplyingType == null ? MultiplyingType.LEAF : multiplyingType; //optional
    }

    public void setPlantingDate(Date plantingDate) {
        this.plantingDate = (Date)plantingDate.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plant plant = (Plant) o;
        return Objects.equals(id, plant.id) &&
                Objects.equals(name, plant.name) &&
                soilType == plant.soilType &&
                Objects.equals(origin, plant.origin) &&
                Objects.equals(visualParameters, plant.visualParameters) &&
                Objects.equals(growingTips, plant.growingTips) &&
                multiplyingType == plant.multiplyingType &&
                Objects.equals(plantingDate, plant.plantingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, soilType, origin, visualParameters, growingTips, multiplyingType, plantingDate);
    }

    @Override
    public String toString() {
        return "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", soilType=" + soilType +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplyingType=" + multiplyingType +
                ", plantingDate=" + plantingDate;
    }
}
