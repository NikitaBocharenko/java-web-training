package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SporeType {
    MOSS, PLAUN, JOINTWEED, FERN;

    public static Optional<SporeType> fromString(String type) {
        return Stream.of(SporeType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
