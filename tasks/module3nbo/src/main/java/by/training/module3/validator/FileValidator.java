package by.training.module3.validator;

public interface FileValidator {
    ValidationResult validate(String path);
}
