package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum SeedType {
    GYMNOSPERM, FLOWERING;

    public static Optional<SeedType> fromString(String type) {
        return Stream.of(SeedType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
