package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum PlantType {
    SEAWEED, SEED, SPORE;

    public static Optional<PlantType> fromString(String type) {
        return Stream.of(PlantType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
