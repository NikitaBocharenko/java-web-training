package by.training.module3.command;

import java.util.EnumMap;
import java.util.Map;

public class CommandProviderImpl implements CommandProvider {
    private Map<ParseCommandType, ParseCommand> commands;

    public CommandProviderImpl(){
        commands = new EnumMap<>(ParseCommandType.class);
    }

    @Override
    public ParseCommand getByType(ParseCommandType type) {
        return commands.get(type);
    }

    @Override
    public void addCommand(ParseCommandType type, ParseCommand command) {
        commands.put(type, command);
    }
}
