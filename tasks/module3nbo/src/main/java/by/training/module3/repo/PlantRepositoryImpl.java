package by.training.module3.repo;

import by.training.module3.entity.Plant;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlantRepositoryImpl implements PlantRepository {
    private List<Plant> plants = new ArrayList<>();

    @Override
    public void add(Plant plant) {
        this.plants.add(plant);
    }

    @Override
    public void remove(Plant plant) {
        this.plants.remove(plant);
    }

    @Override
    public List<Plant> find(PlantSpecification specification) {
        return this.plants.stream()
                .filter(specification::specified)
                .collect(Collectors.toList());
    }

    @Override
    public List<Plant> sort(PlantComparator comparator) {
        return this.plants.stream()
                .sorted(comparator::compare)
                .collect(Collectors.toList());
    }

    @Override
    public void update(Plant plant) {
        int index = -1;
        for (Plant oldPlant : this.plants){
            if (oldPlant.getId().equals(plant.getId())){
                index = this.plants.indexOf(oldPlant);
                break;
            }
        }
        if (index >= 0){
            this.plants.set(index, plant);
        }
    }

    @Override
    public List<Plant> getAll() {
        return new ArrayList<>(this.plants);
    }
}
