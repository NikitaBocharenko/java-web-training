package by.training.module3.validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XmlValidationErrorHandler implements ErrorHandler {
    private ValidationResult validationResult;

    public XmlValidationErrorHandler(ValidationResult validationResult) {
        this.validationResult = validationResult;
    }

    public ValidationResult getValidationResult() {
        return validationResult;
    }

    @Override
    public void warning(SAXParseException exception){
        this.validationResult.addError("xml", "warning: " + exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception){
        this.validationResult.addError("xml", "error: " + exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        this.validationResult.addError("xml", "fatalError: " + exception.getMessage());
        throw exception;
    }
}
