package by.training.module3.repo;

import by.training.module3.entity.Plant;

public interface PlantSpecification {
    boolean specified(Plant plant);
}
