package by.training.module3.command;

public enum ParseCommandType {
    DOM, SAX, STAX
}
