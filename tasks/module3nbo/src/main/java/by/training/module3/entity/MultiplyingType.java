package by.training.module3.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum MultiplyingType {
    LEAF, STALK, SEED;

    public static Optional<MultiplyingType> fromString(String type) {
        return Stream.of(MultiplyingType.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
