package by.training.module3.controller;

import by.training.module3.command.CommandProvider;
import by.training.module3.command.CommandProviderImpl;
import by.training.module3.command.DomParseCommand;
import by.training.module3.command.ParseCommand;
import by.training.module3.command.ParseCommandType;
import by.training.module3.command.SaxParseCommand;
import by.training.module3.command.StaxParseCommand;
import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.SoilType;
import by.training.module3.parser.DomParserImpl;
import by.training.module3.parser.ParserException;
import by.training.module3.parser.SaxParserImpl;
import by.training.module3.parser.StaxParserImpl;
import by.training.module3.repo.PlantRepository;
import by.training.module3.repo.PlantRepositoryImpl;
import by.training.module3.service.PlantService;
import by.training.module3.service.PlantServiceImpl;
import by.training.module3.validator.FileValidator;
import by.training.module3.validator.FileValidatorImpl;
import by.training.module3.validator.XmlValidator;
import by.training.module3.validator.XmlValidatorImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(JUnit4.class)
public class PlantControllerTest {
    private PlantController controller;
    private Plant expectedPlant;

    @Before
    public void setUp() throws ParserException {
        expectedPlant = new Seaweed("PLANTA", "Glaucophyta", SoilType.WATER, "Belarus", Arrays.asList("param 1", "param 2"),
                Arrays.asList("tip 1","tip 2"), MultiplyingType.LEAF, new GregorianCalendar(2000, Calendar.JANUARY, 1, 5, 49, 45).getTime(),
                SeaweedType.GREEN);
        PlantRepository repository = new PlantRepositoryImpl();
        PlantService service = new PlantServiceImpl(repository);
        FileValidator fileValidator = new FileValidatorImpl();
        XmlValidator xmlValidator = new XmlValidatorImpl(this.getClass().getClassLoader().getResource("validation_schema.xsd").getPath().substring(1));
        CommandProvider commandProvider = new CommandProviderImpl();
        ParseCommand domParseCommand = new DomParseCommand(new DomParserImpl());
        commandProvider.addCommand(ParseCommandType.DOM, domParseCommand);
        ParseCommand saxParserCommand = new SaxParseCommand(new SaxParserImpl());
        commandProvider.addCommand(ParseCommandType.SAX, saxParserCommand);
        ParseCommand staxParserCommand = new StaxParseCommand(new StaxParserImpl());
        commandProvider.addCommand(ParseCommandType.STAX, staxParserCommand);
        controller = new PlantController(fileValidator, xmlValidator, commandProvider, service);
    }

    @Test
    public void shouldParseXmlFileWithDom(){
        List<Plant> actual = controller.parseXmlFile(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1), ParseCommandType.DOM);
        if (actual.isEmpty()) {
            Assert.fail();
        }
        Assert.assertEquals(16, actual.size());
        Plant actualPlant = actual.get(0);
        Assert.assertEquals(expectedPlant, actualPlant);
    }

    @Test
    public void shouldParseXmlFileWithSax(){
        List<Plant> actual = controller.parseXmlFile(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1), ParseCommandType.SAX);
        if (actual.isEmpty()) {
            Assert.fail();
        }
        Assert.assertEquals(16, actual.size());
        Plant actualPlant = actual.get(0);
        Assert.assertEquals(expectedPlant, actualPlant);
    }

    @Test
    public void shouldParseXmlFileWithStax(){
        List<Plant> actual = controller.parseXmlFile(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1), ParseCommandType.STAX);
        if (actual.isEmpty()) {
            Assert.fail();
        }
        Assert.assertEquals(16, actual.size());
        Plant actualPlant = actual.get(0);
        Assert.assertEquals(expectedPlant, actualPlant);
    }
}
