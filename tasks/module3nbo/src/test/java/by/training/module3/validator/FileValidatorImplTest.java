package by.training.module3.validator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class FileValidatorImplTest {

    @Test
    public void shouldValidateFile(){
        FileValidator validator = new FileValidatorImpl();
        ValidationResult validationResult = validator.validate(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1));
        Assert.assertTrue(validationResult.isValid());
    }
}
