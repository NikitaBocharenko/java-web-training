package by.training.module3.validator;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Set;

@RunWith(JUnit4.class)
public class XmlValidatorImplTest {
    private static final Logger log = Logger.getLogger(XmlValidatorImplTest.class);
    private XmlValidator xmlValidator;

    @Before
    public void setUp(){
        xmlValidator = new XmlValidatorImpl(this.getClass().getClassLoader().getResource("validation_schema.xsd").getPath().substring(1));
    }

    @Test
    public void shouldSuccessfullyValidateXml(){
        ValidationResult actualResult = xmlValidator.validate(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1));
        log.debug(actualResult);
        Assert.assertTrue(actualResult.isValid());
    }

    @Test
    public void shouldFindErrors(){
        ValidationResult actualResult = xmlValidator.validate(this.getClass().getClassLoader().getResource("contains_errors.xml").getPath().substring(1));
        log.debug(actualResult);
        Assert.assertFalse(actualResult.isValid());
        Set<String> errors = actualResult.getErrorsByType("xml");
        Assert.assertTrue(errors.contains("error: cvc-datatype-valid.1.2.1: '2000-0101T05:49:45' is not a valid value for 'dateTime'."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'd'. One of '{\"http://training.by/test\":id}' is expected."));
        Assert.assertTrue(errors.contains("error: cvc-attribute.3: The value 'waer' of attribute 'soilType' on element 'seaweed' is not valid with respect to its type, 'soilType'."));
        Assert.assertTrue(errors.contains("error: cvc-type.3.1.3: The value '2006-01-01T05:49:4' of element 'plantingDate' is not valid."));
        Assert.assertTrue(errors.contains("error: cvc-type.3.1.3: The value '2000-0101T05:49:45' of element 'plantingDate' is not valid."));
        Assert.assertTrue(errors.contains("error: cvc-enumeration-valid: Value 'eed' is not facet-valid with respect to enumeration '[leaf, stalk, seed]'. It must be a value from the enumeration."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'growingTp'. One of '{\"http://training.by/test\":growingTip}' is expected."));
        Assert.assertTrue(errors.contains("error: cvc-enumeration-valid: Value 'waer' is not facet-valid with respect to enumeration '[podzolic, dirt, sod_podzolic, water]'. It must be a value from the enumeration."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'platingDate'. One of '{\"http://training.by/test\":plantingDate}' is expected."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'plantigDate'. One of '{\"http://training.by/test\":plantingDate}' is expected."));
        Assert.assertTrue(errors.contains("error: cvc-attribute.3: The value 'sodpodzolic' of attribute 'soilType' on element 'seed' is not valid with respect to its type, 'soilType'."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'nae'. One of '{\"http://training.by/test\":name}' is expected."));
        Assert.assertTrue(errors.contains("error: cvc-enumeration-valid: Value 'sodpodzolic' is not facet-valid with respect to enumeration '[podzolic, dirt, sod_podzolic, water]'. It must be a value from the enumeration."));
        Assert.assertTrue(errors.contains("error: cvc-attribute.3: The value 'eed' of attribute 'multiplyingType' on element 'spore' is not valid with respect to its type, 'multiplyingType'."));
        Assert.assertTrue(errors.contains("error: cvc-datatype-valid.1.2.1: '2006-01-01T05:49:4' is not a valid value for 'dateTime'."));
        Assert.assertTrue(errors.contains("error: cvc-complex-type.2.4.a: Invalid content was found starting with element 'visualParametr'. One of '{\"http://training.by/test\":visualParameter}' is expected."));
        Assert.assertEquals(16, errors.size());
    }

    @Test
    public void shouldGetFatalError() {
        ValidationResult actualResult = xmlValidator.validate(this.getClass().getClassLoader().getResource("fatal_error.xml").getPath().substring(1));
        log.debug(actualResult);
        Assert.assertFalse(actualResult.isValid());
        Set<String> errors = actualResult.getErrorsByType("xml");
        Assert.assertTrue(errors.contains("fatalError: Content is not allowed in prolog."));
        Assert.assertEquals(1, errors.size());
    }
}
