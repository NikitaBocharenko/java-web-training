package by.training.module3.parser;

import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.SoilType;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(JUnit4.class)
public class DomParserImplTest {
    private Plant expected;

    private static final Logger log = Logger.getLogger(DomParserImplTest.class);

    @Before
    public void setUp(){
        expected = new Seaweed("PLANTA", "Glaucophyta", SoilType.WATER, "Belarus", Arrays.asList("param 1", "param 2"),
                Arrays.asList("tip 1","tip 2"), MultiplyingType.LEAF, new GregorianCalendar(2000, Calendar.JANUARY, 1, 5, 49, 45).getTime(),
                SeaweedType.GREEN);
    }

    @Test
    public void shouldParseFile() throws ParserException {
        DomParser parser = new DomParserImpl();
        List<Plant> parsedData = parser.parse(this.getClass().getClassLoader().getResource("valid.xml").getPath().substring(1));
        parsedData.forEach(log::debug);
        Assert.assertEquals(16, parsedData.size());
        Plant actual = parsedData.get(0);
        Assert.assertEquals(expected, actual);
    }
}
