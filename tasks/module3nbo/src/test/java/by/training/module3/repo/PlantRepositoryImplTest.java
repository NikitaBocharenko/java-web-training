package by.training.module3.repo;

import by.training.module3.entity.MultiplyingType;
import by.training.module3.entity.Plant;
import by.training.module3.entity.Seaweed;
import by.training.module3.entity.SeaweedType;
import by.training.module3.entity.Seed;
import by.training.module3.entity.SeedType;
import by.training.module3.entity.SoilType;
import by.training.module3.entity.Spore;
import by.training.module3.entity.SporeType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(JUnit4.class)
public class PlantRepositoryImplTest {
    private List<Plant> expected;
    private PlantRepository repository;

    @Before
    public void setUp(){
        expected = new ArrayList<>();
        Plant plant1 = new Seaweed("PLANTA", "Glaucophyta", SoilType.WATER, "Belarus", Arrays.asList("param 1", "param 2"),
                Arrays.asList("tip 1","tip 2"), MultiplyingType.LEAF, new GregorianCalendar(2000, Calendar.JANUARY, 1, 5, 49, 45).getTime(),
                SeaweedType.GREEN);
        expected.add(plant1);
        Plant plant2 = new Seed("PLANTB", "Magnoliidae", SoilType.SOD_PODZOLIC, "Netherlands", Arrays.asList("param 1", "param 2"),
                Arrays.asList("tip 1","tip 2"), null, new GregorianCalendar(2001, Calendar.JANUARY, 1, 5, 49, 45).getTime(),
                SeedType.FLOWERING);
        expected.add(plant2);
        Plant plant3 = new Spore("PLANTC", "Anthoceros", SoilType.DIRT, "Norway", Arrays.asList("param 1", "param 2"),
                Arrays.asList("tip 1","tip 2"), MultiplyingType.SEED, new GregorianCalendar(2002, Calendar.JANUARY, 1, 5, 49, 45).getTime(),
                SporeType.MOSS);
        expected.add(plant3);
        repository = new PlantRepositoryImpl();
        expected.forEach(repository::add);
    }

    @Test
    public void shouldReturnAll(){
        List<Plant> actual = repository.getAll();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldFindSpecified(){
        List<Plant> actual = repository.find(plant->plant.getId().equals("PLANTA"));
        Assert.assertEquals(expected.get(0), actual.get(0));
    }

    @Test
    public void shouldSort(){
        List<Plant> actual = repository.sort((p1,p2)->p2.getPlantingDate().compareTo(p1.getPlantingDate()));
        Collections.reverse(expected);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldRemove(){
        Plant forRemove = expected.get(0);
        repository.remove(forRemove);
        List<Plant> actual = repository.getAll();
        expected.remove(forRemove);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldUpdate(){
        Plant forUpdate = expected.get(0);
        int index = expected.indexOf(forUpdate);
        forUpdate.setName("FUUUUUUUUUUUUUU");
        expected.set(index, forUpdate);
        List<Plant> actual = repository.getAll();
        repository.update(forUpdate);
        Assert.assertEquals(expected, actual);
    }
}
