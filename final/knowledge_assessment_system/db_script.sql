-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema kas
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema kas
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `kas` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `kas` ;

-- -----------------------------------------------------
-- Table `kas`.`question_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`question_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`type_name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`user_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`user_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 86
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`test` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `test_name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(500) NULL DEFAULT NULL,
  `creation_time` DATETIME NOT NULL,
  `deadline_time` DATETIME NOT NULL,
  `creator_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `creator_idx` (`creator_id` ASC) VISIBLE,
  CONSTRAINT `creator`
    FOREIGN KEY (`creator_id`)
    REFERENCES `kas`.`user_account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`question` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_text` VARCHAR(500) NOT NULL,
  `question_type_id` INT(11) NOT NULL,
  `test_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_question_question_type1_idx` (`question_type_id` ASC) VISIBLE,
  INDEX `fk_question_test1_idx` (`test_id` ASC) VISIBLE,
  CONSTRAINT `fk_question_question_type1`
    FOREIGN KEY (`question_type_id`)
    REFERENCES `kas`.`question_type` (`id`),
  CONSTRAINT `fk_question_test1`
    FOREIGN KEY (`test_id`)
    REFERENCES `kas`.`test` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`test_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`test_result` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `result` INT(11) NOT NULL,
  `completion_time` DATETIME NOT NULL,
  `user_account_id` INT(11) NOT NULL,
  `test_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_result_user_account1_idx` (`user_account_id` ASC) VISIBLE,
  INDEX `fk_result_test1_idx` (`test_id` ASC) VISIBLE,
  CONSTRAINT `fk_result_test1`
    FOREIGN KEY (`test_id`)
    REFERENCES `kas`.`test` (`id`),
  CONSTRAINT `fk_result_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `kas`.`user_account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`answer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_id` INT(11) NOT NULL,
  `result_id` INT(11) NOT NULL,
  `is_correct` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_answer_question1_idx` (`question_id` ASC) VISIBLE,
  INDEX `fk_answer_result1_idx` (`result_id` ASC) VISIBLE,
  CONSTRAINT `fk_answer_question1`
    FOREIGN KEY (`question_id`)
    REFERENCES `kas`.`question` (`id`),
  CONSTRAINT `fk_answer_result1`
    FOREIGN KEY (`result_id`)
    REFERENCES `kas`.`test_result` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 128
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`answer_option`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`answer_option` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `option_text` VARCHAR(100) NOT NULL,
  `is_correct` INT(11) NOT NULL,
  `question_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_answer_option_question1_idx` (`question_id` ASC) VISIBLE,
  CONSTRAINT `fk_answer_option_question1`
    FOREIGN KEY (`question_id`)
    REFERENCES `kas`.`question` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 60
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`answer_option_has_answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`answer_option_has_answer` (
  `answer_option_id` INT(11) NOT NULL,
  `answer_id` INT(11) NOT NULL,
  PRIMARY KEY (`answer_option_id`, `answer_id`),
  INDEX `fk_answer_option_has_answer_answer1_idx` (`answer_id` ASC) VISIBLE,
  INDEX `fk_answer_option_has_answer_answer_option1_idx` (`answer_option_id` ASC) VISIBLE,
  CONSTRAINT `fk_answer_option_has_answer_answer1`
    FOREIGN KEY (`answer_id`)
    REFERENCES `kas`.`answer` (`id`),
  CONSTRAINT `fk_answer_option_has_answer_answer_option1`
    FOREIGN KEY (`answer_option_id`)
    REFERENCES `kas`.`answer_option` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`student_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`student_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(150) NOT NULL,
  `creator_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `creator_idx` (`creator_id` ASC) VISIBLE,
  CONSTRAINT `groupCreator`
    FOREIGN KEY (`creator_id`)
    REFERENCES `kas`.`user_account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`test_has_student_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`test_has_student_group` (
  `test_id` INT(11) NOT NULL,
  `student_group_id` INT(11) NOT NULL,
  PRIMARY KEY (`test_id`, `student_group_id`),
  INDEX `fk_test_has_student_group_student_group1_idx` (`student_group_id` ASC) VISIBLE,
  INDEX `fk_test_has_student_group_test1_idx` (`test_id` ASC) VISIBLE,
  CONSTRAINT `fk_test_has_student_group_student_group1`
    FOREIGN KEY (`student_group_id`)
    REFERENCES `kas`.`student_group` (`id`),
  CONSTRAINT `fk_test_has_student_group_test1`
    FOREIGN KEY (`test_id`)
    REFERENCES `kas`.`test` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`user_account_has_student_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`user_account_has_student_group` (
  `user_account_id` INT(11) NOT NULL,
  `student_group_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_account_id`, `student_group_id`),
  INDEX `fk_user_account_has_student_group_student_group1_idx` (`student_group_id` ASC) VISIBLE,
  INDEX `fk_user_account_has_student_group_user_account1_idx` (`user_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_account_has_student_group_student_group1`
    FOREIGN KEY (`student_group_id`)
    REFERENCES `kas`.`student_group` (`id`),
  CONSTRAINT `fk_user_account_has_student_group_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `kas`.`user_account` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`user_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`role_name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `kas`.`user_account_has_user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `kas`.`user_account_has_user_role` (
  `user_account_id` INT(11) NOT NULL,
  `user_role_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_account_id`, `user_role_id`),
  INDEX `fk_user_account_has_user_role_user_role1_idx` (`user_role_id` ASC) VISIBLE,
  INDEX `fk_user_account_has_user_role_user_account1_idx` (`user_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_account_has_user_role_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `kas`.`user_account` (`id`),
  CONSTRAINT `fk_user_account_has_user_role_user_role1`
    FOREIGN KEY (`user_role_id`)
    REFERENCES `kas`.`user_role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
