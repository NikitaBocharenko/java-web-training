package by.training.kas.command;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RunWith(JUnit4.class)
public class LogoutCommandTest {
    private Command command;
    private HttpSession mockSession;
    private HttpServletRequest mockRequest;

    @Before
    public void setUp() {
        mockSession = Mockito.mock(HttpSession.class);
        mockRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockRequest.getSession(Mockito.anyBoolean())).thenReturn(mockSession);
        command = new LogoutCommand();
    }

    @Test
    public void shouldInvalidateSession() {
        String result = command.process(mockRequest, null);
        Assert.assertEquals("redirect:command=GET_LOGIN", result);
        Mockito.verify(mockSession, Mockito.times(1)).invalidate();
    }
}
