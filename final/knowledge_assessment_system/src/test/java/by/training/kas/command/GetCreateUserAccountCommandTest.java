package by.training.kas.command;

import by.training.kas.entity.UserRole;
import by.training.kas.service.UserRoleService;
import by.training.kas.service.UserRoleServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

@RunWith(JUnit4.class)
public class GetCreateUserAccountCommandTest {
    private Command command;
    private HttpServletRequest mockRequest;
    private final Object[] result = new Object[1];
    private Set<UserRole> roles;

    @Before
    public void setUp() {
        mockRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.doAnswer(invocationOnMock -> {
            result[0] = invocationOnMock.getArgumentAt(1, Object.class);
            return null;
        }).when(mockRequest).setAttribute(Mockito.anyString(), Mockito.anyList());
        UserRoleService mockService = Mockito.mock(UserRoleServiceImpl.class);
        roles = new HashSet<>();
        roles.add(new UserRole(1, "test1"));
        roles.add(new UserRole(2, "test2"));
        Mockito.when(mockService.getPossibleRoles()).thenReturn(roles);
        command = new GetCreateUserAccountCommand(mockService);
    }

    @Test
    public void shouldPreparePossibleRoles() {

        String view = command.process(mockRequest, null);
        Assert.assertEquals("registration", view);
        Set<UserRole> actual = (Set<UserRole>) result[0];
        Assert.assertEquals(roles, actual);
    }
}
