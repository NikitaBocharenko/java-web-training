package by.training.kas.validator;

import by.training.kas.application.ApplicationConstants;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RunWith(JUnit4.class)
public class EditUserAccountDataValidatorTest {

    @Test
    public void shouldSuccessfullyValidate() {
        HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_ID_ATTRIBUTE_NAME))).thenReturn("1");
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME))).thenReturn("test first name");
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME))).thenReturn("test last name");
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME))).thenReturn("test email");

        Set<String> testKeySet = new HashSet<>();
        testKeySet.add("TestRole");
        Map mockParameterMap = Mockito.mock(Map.class);
        Mockito.when(mockParameterMap.keySet()).thenReturn(testKeySet);
        Mockito.when(mockRequest.getParameterMap()).thenReturn(mockParameterMap);

        RequestDataValidator validator = new EditUserAccountDataValidator();
        ValidationResult validationResult = validator.validate(mockRequest);
        Assert.assertTrue(validationResult.isValid());
    }

    @Test
    public void shouldFindErrors() {
        HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_ID_ATTRIBUTE_NAME))).thenReturn("zzz");
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME))).thenReturn(null);
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME))).thenReturn("");
        Mockito.when(mockRequest.getParameter(Mockito.eq(ApplicationConstants.USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME))).thenReturn("test email");

        Set<String> testKeySet = new HashSet<>();
        testKeySet.add("Test");
        Map mockParameterMap = Mockito.mock(Map.class);
        Mockito.when(mockParameterMap.keySet()).thenReturn(testKeySet);
        Mockito.when(mockRequest.getParameterMap()).thenReturn(mockParameterMap);

        RequestDataValidator validator = new EditUserAccountDataValidator();
        ValidationResult validationResult = validator.validate(mockRequest);
        Assert.assertFalse(validationResult.isValid());
        Set<String> errors = validationResult.getErrorsByType(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE);
        Assert.assertTrue(errors.contains("id.not-a-number"));
        Assert.assertTrue(errors.contains("first-name.null"));
        Assert.assertTrue(errors.contains("last-name.empty"));
        Assert.assertTrue(errors.contains("roles.no-selected"));
        Assert.assertEquals(4, errors.size());
    }
}
