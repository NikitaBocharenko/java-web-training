package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.TransactionManager;
import by.training.kas.dao.UserAccountDAO;
import by.training.kas.dao.UserRoleDAO;
import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)
public class UserAccountServiceImplTest {
    private UserAccountService service;
    private UserAccount expected;
    private final Map<Long, Long> actualRolesAssign = new HashMap<>();
    private Map<Long, Long> expectedRolesAssign;

    @Before
    public void setUp() throws SQLException, DAOException {
        expected = new UserAccount(1, "test", "test", "Тест", "Тестович", "test@mail.ru");
        Set<UserRole> expectedRoles = new HashSet<>();
        expectedRoles.add(new UserRole(1, "Role1"));
        expectedRoles.add(new UserRole(2, "Role2"));
        expected.setUserRoles(expectedRoles);
        UserAccountDAO mockUserAccountDAO = Mockito.mock(UserAccountDAO.class);
        Mockito.when(mockUserAccountDAO.selectByLoginPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.of(expected));
        Mockito.when(mockUserAccountDAO.isLoginAlreadyExists(Mockito.anyString())).thenReturn(true);
        Mockito.when(mockUserAccountDAO.insert(Mockito.any(UserAccount.class))).thenReturn(expected.getId());
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectRolesForAccount(Mockito.anyLong())).thenReturn(expectedRoles);
        expectedRolesAssign = new HashMap<>();
        expectedRolesAssign.put(expected.getId(), 1L);
        expectedRolesAssign.put(expected.getId(), 2L);
        Mockito.doAnswer(invocationResult -> {
            long userId = invocationResult.getArgumentAt(0, Long.class);
            long roleId = invocationResult.getArgumentAt(1, Long.class);
            actualRolesAssign.put(userId, roleId);
            return null;
        }).when(mockUserRoleDAO).assignRole(Mockito.anyLong(), Mockito.anyLong());
        TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);
        service = new UserAccountServiceImpl(mockUserAccountDAO, mockUserRoleDAO, mockTransactionManager);
    }

    @Test
    public void shouldReturnAccountByCredentials() {
        Optional<UserAccount> userAccountOptional = service.getByCredentials("bacharenka", "bacharenka");
        Assert.assertTrue(userAccountOptional.isPresent());
        UserAccount actual = userAccountOptional.get();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldCheckIsLoginExists() {
        boolean result = service.isLoginAlreadyExists("bacharenka");
        Assert.assertTrue(result);
    }

    @Test
    public void shouldRegisterAccount() {
        service.add(expected);
        Assert.assertEquals(expectedRolesAssign, actualRolesAssign);
    }

    @Test
    public void shouldSelectAllAccounts() throws SQLException, DAOException {
        List<UserAccount> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(new UserAccount(1, "test1", "test1", "Test 1", "Test 1", "test1@mail.com"));
        expectedAccounts.add(new UserAccount(2, "test2", "test2", "Test 2", "Test 2", "test2@mail.com"));
        expectedAccounts.add(new UserAccount(3, "test3", "test3", "Test 3", "Test 3", "test3@mail.com"));
        UserAccountDAO mockUserAccountDAO = Mockito.mock(UserAccountDAO.class);
        Mockito.when(mockUserAccountDAO.selectAll()).thenReturn(expectedAccounts);
        Set<UserRole> expectedRoles = new HashSet<>();
        expectedRoles.add(new UserRole(1, "Test role 1"));
        expectedRoles.add(new UserRole(1, "Test role 1"));
        for (UserAccount userAccount : expectedAccounts) {
            userAccount.setUserRoles(expectedRoles);
        }
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectRolesForAccount(Mockito.anyLong())).thenReturn(expectedRoles);
        TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);

        UserAccountService service = new UserAccountServiceImpl(mockUserAccountDAO, mockUserRoleDAO, mockTransactionManager);
        List<UserAccount> actualAccounts = service.getAll();
        Assert.assertEquals(expectedAccounts, actualAccounts);
    }

    @Test
    public void shouldSelectAccountById() throws SQLException, DAOException {
        UserAccount expectedAccount = new UserAccount(1, "test", "test", "Тест", "Тестович", "test@mail.ru");
        UserAccountDAO mockUserAccountDAO = Mockito.mock(UserAccountDAO.class);
        Mockito.when(mockUserAccountDAO.select(Mockito.anyLong())).thenReturn(expectedAccount);
        Set<UserRole> expectedRoles = new HashSet<>();
        expectedRoles.add(new UserRole(1, "Test role 1"));
        expectedRoles.add(new UserRole(1, "Test role 1"));
        expectedAccount.setUserRoles(expectedRoles);
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectRolesForAccount(Mockito.anyLong())).thenReturn(expectedRoles);
        TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);

        UserAccountService service = new UserAccountServiceImpl(mockUserAccountDAO, mockUserRoleDAO, mockTransactionManager);
        UserAccount actualAccount = service.getById(1);
        Assert.assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void shouldUpdateAccount() throws SQLException {
        UserAccount expectedAccount = new UserAccount(1, "test", "test", "Тест", "Тестович", "test@mail.ru");
        Set<UserRole> expectedRoles = new HashSet<>();
        expectedRoles.add(new UserRole(1, "Test role 1"));
        expectedRoles.add(new UserRole(2, "Test role 2"));
        expectedAccount.setUserRoles(expectedRoles);
        UserAccountDAO mockUserAccountDAO = Mockito.mock(UserAccountDAO.class);
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectRolesForAccount(Mockito.anyLong())).thenReturn(new HashSet<>());
        final List<Long> actualRolesId = new ArrayList<>();
        Mockito.doAnswer(invocationResult -> {
            actualRolesId.add(invocationResult.getArgumentAt(1, Long.class));
            return null;
        }).when(mockUserRoleDAO).assignRole(Mockito.anyLong(), Mockito.anyLong());
        TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);
        UserAccountService service = new UserAccountServiceImpl(mockUserAccountDAO, mockUserRoleDAO, mockTransactionManager);
        service.set(expectedAccount);
        List<Long> expectedRolesId = expectedRoles.stream().map(UserRole::getId).collect(Collectors.toList());
        Assert.assertEquals(expectedRolesId, actualRolesId);
    }

    @Test
    public void shouldDeleteAccount() throws SQLException, DAOException {
        UserAccountDAO mockUserAccountDAO = Mockito.mock(UserAccountDAO.class);
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        TransactionManager mockTransactionManager = Mockito.mock(TransactionManager.class);

        UserAccountService service = new UserAccountServiceImpl(mockUserAccountDAO, mockUserRoleDAO, mockTransactionManager);

        service.delete(1);
        Mockito.verify(mockUserRoleDAO, Mockito.times(1)).deleteRolesForAccount(Mockito.anyLong());
        Mockito.verify(mockUserAccountDAO, Mockito.times(1)).delete(Mockito.anyLong());
        Mockito.verify(mockTransactionManager, Mockito.times(1)).beginTransaction();
        Mockito.verify(mockTransactionManager, Mockito.times(1)).commitTransaction();
    }

}
