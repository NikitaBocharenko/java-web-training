package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.service.UserAccountService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

@RunWith(JUnit4.class)
public class DeleteAccountCommandTest {

    @Test
    public void shouldExecuteCommand() {
        HttpServletRequest mockHttpServletRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockHttpServletRequest.getParameter(ApplicationConstants.USER_ACCOUNT_ID_ATTRIBUTE_NAME)).thenReturn("1");
        UserAccountService mockUserAccountService = Mockito.mock(UserAccountService.class);
        Command mockViewUserListCommand = Mockito.mock(GetUsersListCommand.class);
        Mockito.when(mockViewUserListCommand.process(Mockito.any(), Mockito.any())).thenReturn("userList");
        CommandProvider.addCommand(CommandType.VIEW_USERS, mockViewUserListCommand);

        Command command = new DeleteAccountCommand(mockUserAccountService);
        String result = command.process(mockHttpServletRequest, null);
        Assert.assertEquals("redirect:command=VIEW_USERS", result);
        Mockito.verify(mockUserAccountService, Mockito.times(1)).delete(Mockito.anyLong());
    }
}
