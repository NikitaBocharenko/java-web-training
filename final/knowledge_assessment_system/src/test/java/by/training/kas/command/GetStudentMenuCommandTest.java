package by.training.kas.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class GetStudentMenuCommandTest {

    @Test
    public void shouldReturnStudentMenu() {
        Command command = new GetStudentMenuCommand();
        String result = command.process(null, null);
        Assert.assertEquals("studentMenu", result);
    }
}
