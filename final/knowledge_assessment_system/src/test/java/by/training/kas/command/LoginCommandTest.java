package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)
public class LoginCommandTest {
    private Command command;
    private HttpServletRequest mockRequest;
    private UserAccount expectedAccount;
    private final Long [] sessionUserId = new Long [1];
    private final String [] sessionAttributes = new String [2];
    private final Object [] sessionRoles = new Object [1];

    @Before
    public void setUp() {
        expectedAccount = new UserAccount(1, "test", "test", "Тест", "Тестович", "test@mail.ru");
        Set<UserRole> roles = new HashSet<>();
        roles.add(new UserRole(5, "тестовая"));
        expectedAccount.setUserRoles(roles);
        mockRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockRequest.getParameter(Mockito.eq("login"))).thenReturn(expectedAccount.getLogin());
        UserAccountService mockService = Mockito.mock(UserAccountService.class);
        Mockito.when(mockService.getByCredentials(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.of(expectedAccount));
        HttpSession mockSession = Mockito.mock(HttpSession.class);
        Mockito.doAnswer(invocationOnMock -> {
            sessionUserId[0] = invocationOnMock.getArgumentAt(1, Long.class);
            return null;
        }).when(mockSession).setAttribute(Mockito.eq("userId"), Mockito.anyLong());
        Mockito.doAnswer(invocationOnMock -> {
            sessionAttributes[0] = invocationOnMock.getArgumentAt(1, String.class);
            return null;
        }).when(mockSession).setAttribute(Mockito.eq("userFirstName"), Mockito.anyString());
        Mockito.doAnswer(invocationOnMock -> {
            sessionAttributes[1] = invocationOnMock.getArgumentAt(1, String.class);
            return null;
        }).when(mockSession).setAttribute(Mockito.eq("userLastName"), Mockito.anyString());
        Mockito.doAnswer(invocationOnMock -> {
            sessionRoles[0] = invocationOnMock.getArgumentAt(1, Object.class);
            return null;
        }).when(mockSession).setAttribute(Mockito.eq("userRoles"), Mockito.anyObject());
        Mockito.when(mockRequest.getSession()).thenReturn(mockSession);
        ValidationResult validationResult = new ValidationResult();
        RequestDataValidator mockRequestDataValidator = Mockito.mock(RequestDataValidator.class);
        Mockito.when(mockRequestDataValidator.validate(Mockito.any(HttpServletRequest.class))).thenReturn(validationResult);
        command = new LoginCommand(mockService, mockRequestDataValidator);
    }

    @Test
    public void shouldLogin() {
        String result = command.process(mockRequest, null);
        Assert.assertEquals("redirect:command=TO_WELCOME_PAGE", result);
        Assert.assertEquals(expectedAccount.getId(), sessionUserId[0].longValue());
        Assert.assertEquals(expectedAccount.getFirstName(), sessionAttributes[0]);
        Assert.assertEquals(expectedAccount.getLastName(), sessionAttributes[1]);
        Set<String> actualRoles = (Set<String>) sessionRoles[0];
        Set<String> expectedRoles = expectedAccount.getUserRoles().stream()
                .map(UserRole::getRoleName)
                .collect(Collectors.toSet());
        Assert.assertEquals(expectedRoles, actualRoles);
    }
}
