package by.training.kas.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class TransactionManagerImplTest {
    private static ConnectionPool connectionPool;
    private TransactionManager transactionManager;

    @BeforeClass
    public static void initConnectionPool() {
        connectionPool = ConnectionPoolImpl.getInstance();
    }

    @Before
    public void setUp() {
        transactionManager = new TransactionManagerImpl(connectionPool);
    }

    @Test
    public void shouldReturnNullWithoutTransaction() {
        Assert.assertNull(transactionManager.getConnection());
    }

    @Test
    public void shouldReturnConnectionWithinTransaction() {
        transactionManager.beginTransaction();
        Assert.assertNotNull(transactionManager.getConnection());
    }

    @Test
    public void shouldCommitTransaction() throws SQLException {
        executeSql("create table tmp (name varchar(100))");
        transactionManager.beginTransaction();
        Connection connection = transactionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("insert into tmp (name) values ('temp')");
        statement.executeUpdate();
        statement.close();
        transactionManager.commitTransaction();
        transactionManager.beginTransaction();
        connection = transactionManager.getConnection();
        statement = connection.prepareStatement("select * from tmp");
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            Assert.fail("Commited data should be returned");
        }
        String name = resultSet.getString("name");
        resultSet.close();
        statement.close();
        transactionManager.commitTransaction();
        Assert.assertEquals("temp", name);
        executeSql("drop table tmp");
    }

    @Test
    public void shouldRollbackTransaction() throws SQLException {
        executeSql("create table tmp (name varchar(100))");
        transactionManager.beginTransaction();
        Connection connection = transactionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement("insert into tmp (name) values ('temp')");
        statement.executeUpdate();
        statement.close();
        transactionManager.rollbackTransaction();
        transactionManager.beginTransaction();
        connection = transactionManager.getConnection();
        statement = connection.prepareStatement("select * from tmp");
        ResultSet resultSet = statement.executeQuery();
        Assert.assertFalse(resultSet.next());
        resultSet.close();
        statement.close();
        transactionManager.commitTransaction();
        executeSql("drop table tmp");
    }

    private void executeSql(String sql) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:kas", "sa", "");
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
        statement.close();
        connection.close();
    }

    @AfterClass
    public static void closeConnectionPool(){
        connectionPool.close();
    }
}
