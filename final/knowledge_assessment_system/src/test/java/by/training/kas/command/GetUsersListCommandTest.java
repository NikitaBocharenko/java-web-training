package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.service.UserAccountService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class GetUsersListCommandTest {

    @Test
    public void shouldPrepareUserList() {
        List<UserAccount> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(new UserAccount(1, "test1", "test1", "Test 1", "Test 1", "test1@mail.com"));
        expectedAccounts.add(new UserAccount(2, "test2", "test2", "Test 2", "Test 2", "test2@mail.com"));
        expectedAccounts.add(new UserAccount(3, "test3", "test3", "Test 3", "Test 3", "test3@mail.com"));
        UserAccountService mockService = Mockito.mock(UserAccountService.class);
        Mockito.when(mockService.getAll()).thenReturn(expectedAccounts);
        HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        final Object[] argument = new Object[1];
        Mockito.doAnswer(invocationOnMock -> {
            argument[0] = invocationOnMock.getArgumentAt(1, Object.class);
            return null;
        }).when(mockRequest).setAttribute(Mockito.eq("userAccounts"), Mockito.anyObject());
        Command command = new GetUsersListCommand(mockService);
        String result = command.process(mockRequest, null);
        Assert.assertEquals("userList", result);
        List<UserAccount> actualAccounts = (List<UserAccount>) argument[0];
        Assert.assertEquals(expectedAccounts, actualAccounts);

    }
}
