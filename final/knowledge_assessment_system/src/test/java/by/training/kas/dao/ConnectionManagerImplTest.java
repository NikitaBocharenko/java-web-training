package by.training.kas.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class ConnectionManagerImplTest {
    private static ConnectionPool connectionPool;
    private TransactionManager transactionManager;
    private ConnectionManager connectionManager;

    @BeforeClass
    public static void initConnectionPool() {
        connectionPool = ConnectionPoolImpl.getInstance();
    }

    @Before
    public void setUp() {
        transactionManager = new TransactionManagerImpl(connectionPool);
        connectionManager = new ConnectionManagerImpl(connectionPool, transactionManager);
    }

    @Test
    public void shouldGetConnectionFromConnectionPool() throws SQLException {
        Connection connection = connectionManager.getConnection();
        Assert.assertNotNull(connection);
        connection.close();
    }

    @AfterClass
    public static void closeConnectionPool(){
        connectionPool.close();
    }
}
