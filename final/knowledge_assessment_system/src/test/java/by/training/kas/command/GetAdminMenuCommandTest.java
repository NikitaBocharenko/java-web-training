package by.training.kas.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class GetAdminMenuCommandTest {

    @Test
    public void shouldReturnAdminMenu() {
        Command command = new GetAdminMenuCommand();
        String result = command.process(null, null);
        Assert.assertEquals("adminMenu", result);
    }
}
