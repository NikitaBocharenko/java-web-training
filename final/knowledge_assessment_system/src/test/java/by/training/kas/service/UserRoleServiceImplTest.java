package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.UserRoleDAO;
import by.training.kas.entity.UserRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(JUnit4.class)
public class UserRoleServiceImplTest {
    private UserRoleService userRoleService;
    private Set<UserRole> expected;
    private UserRole expectedRole;

    @Before
    public void setUpService() throws SQLException {
        expectedRole = new UserRole(1, "Possible role 1");
        expected = new HashSet<>();
        expected.add(expectedRole);
        expected.add(new UserRole(2, "Possible role 2"));
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectPossibleRoles()).thenReturn(expected);
        Mockito.when(mockUserRoleDAO.selectByName(Mockito.anyString())).thenReturn(expectedRole);
        userRoleService = new UserRoleServiceImpl(mockUserRoleDAO);
    }

    @Test
    public void shouldGetPossibleRoles() {
        Set<UserRole> actual = userRoleService.getPossibleRoles();
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldGetRoleByName() {
        UserRole actual = userRoleService.getByName(expectedRole.getRoleName());
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedRole, actual);
    }

    @Test
    public void shouldGetAllRoles() throws SQLException, DAOException {
        List<UserRole> expected = new ArrayList<>();
        expected.add(new UserRole(1, "Role 1"));
        expected.add(new UserRole(2, "Role 2"));
        expected.add(new UserRole(3, "Role 3"));
        UserRoleDAO mockUserRoleDAO = Mockito.mock(UserRoleDAO.class);
        Mockito.when(mockUserRoleDAO.selectAll()).thenReturn(expected);
        UserRoleService userRoleService = new UserRoleServiceImpl(mockUserRoleDAO);
        List<UserRole> actual = userRoleService.getAll();
        Assert.assertEquals(expected, actual);
    }
}
