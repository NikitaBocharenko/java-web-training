package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.service.UserRoleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class GetEditAccountCommandTest {

    @Test
    public void shouldPrepareUserAccountForEdit() {
        UserAccount expectedAccount = new UserAccount(1, "test", "test", "Тест", "Тестович", "test@mail.ru");
        HttpServletRequest mockHttpServletRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.when(mockHttpServletRequest.getParameter(Mockito.eq("id"))).thenReturn("1");
        final UserAccount[] actualAccount = new UserAccount[1];
        Mockito.doAnswer(invocationOnMock -> {
            actualAccount[0] = invocationOnMock.getArgumentAt(1, UserAccount.class);
            return null;
        }).when(mockHttpServletRequest).setAttribute(Mockito.eq("userAccount"), Mockito.any(UserAccount.class));
        UserAccountService mockUserAccountService = Mockito.mock(UserAccountService.class);
        Mockito.when(mockUserAccountService.getById(Mockito.anyLong())).thenReturn(expectedAccount);

        List<UserRole> expectedRoles = new ArrayList<>();
        expectedRoles.add(new UserRole(1, "Test role 1"));
        expectedRoles.add(new UserRole(2, "Test role 2"));
        UserRoleService mockUserRoleService = Mockito.mock(UserRoleService.class);
        Mockito.when(mockUserRoleService.getAll()).thenReturn(expectedRoles);
        final List[] actualRoles = new ArrayList[1];
        Mockito.doAnswer(invocationOnMock -> {
            actualRoles[0] = invocationOnMock.getArgumentAt(1, List.class);
            return null;
        }).when(mockHttpServletRequest).setAttribute(Mockito.eq("possibleRoles"), Mockito.any(List.class));

        Command command = new GetEditAccountCommand(mockUserAccountService, mockUserRoleService);
        String result = command.process(mockHttpServletRequest, null);
        Assert.assertEquals("editAccount", result);
        Assert.assertEquals(expectedAccount, actualAccount[0]);
        Assert.assertEquals(expectedRoles, (List<UserRole>)actualRoles[0]);
    }
}
