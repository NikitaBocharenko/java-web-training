package by.training.kas.dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class ConnectionPoolImplTest {
    private ConnectionPool connectionPool;
    private static final Logger log = Logger.getLogger(ConnectionPoolImplTest.class);

    @Before
    public void setUp() {
        connectionPool = ConnectionPoolImpl.getInstance();
    }

    @Test
    public void shouldGetConnection() {
        Connection connection = connectionPool.getConnection();
        Assert.assertNotNull(connection);
    }

    @Test(expected = ConnectionPoolException.class)
    public void shouldThrowExceptionWhenReleaseWrongConnection() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:hsqldb:mem:kas", "sa", "");
        connectionPool.releaseConnection(connection);
        Assert.fail("Test should throw ConnectionPoolException");
    }

    @After
    public void destroy(){
        connectionPool.close();
    }
}
