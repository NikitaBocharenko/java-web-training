<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${requestScope.get('lang')}"/>
<fmt:setBundle basename = "/pagecontent" scope="application"/>
<html>
    <head>
        <script src="js/locale.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <link rel="stylesheet" href="style/dropdown.css">
        <title><fmt:message key="layout.title"/></title>
    </head>
    <body>
        <div class="dropdown">
          <button class="dropbtn"><fmt:message key="layout.lang.title"/></button>
          <div class="dropdown-content">
            <a href="#" onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>
            <a href="#" onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
          </div>
        </div>
        <div class="login-page">
          <c:choose>
              <c:when test="${not empty viewName}">
                  <c:import url="views/${viewName}.jsp" />
              </c:when>
              <c:otherwise>
                  <c:import url="views/login.jsp" />
              </c:otherwise>
          </c:choose>
        </div>
    </body>
</html>