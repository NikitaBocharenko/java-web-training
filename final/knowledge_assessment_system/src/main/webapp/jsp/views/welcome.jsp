<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--12-col">
    <h4><fmt:message key="page.main.title"/></h4>
</div>
<c:if test="${userRoles.contains('teacher')}">
    <div class="mdl-cell mdl-cell--3-col">
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/teacher.png) bottom center no-repeat #3dbf5f">
            <h2 class="mdl-card__title-text"><fmt:message key="menu.teacher.title"/></h2>
          </div>
          <div class="mdl-card__supporting-text">
            <fmt:message key="menu.teacher.text"/>
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=teacher_menu">
              <fmt:message key="page.navigation.go-link"/>
            </a>
          </div>
        </div>
    </div>
</c:if>
<c:if test="${userRoles.contains('student')}">
    <div class="mdl-cell mdl-cell--3-col">
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/student.png) bottom center no-repeat #3dbf5f">
            <h2 class="mdl-card__title-text"><fmt:message key="menu.student.title"/></h2>
          </div>
          <div class="mdl-card__supporting-text">
            <fmt:message key="menu.student.text"/>
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=view_student_groups">
              <fmt:message key="page.navigation.go-link"/>
            </a>
          </div>
        </div>
    </div>
</c:if>
<c:if test="${userRoles.contains('admin')}">
    <div class="mdl-cell mdl-cell--3-col">
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/admin.png) bottom center no-repeat #3dbf5f">
            <h2 class="mdl-card__title-text"><fmt:message key="menu.admin.title"/></h2>
          </div>
          <div class="mdl-card__supporting-text">
            <fmt:message key="menu.admin.text"/>
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=admin_menu">
              <fmt:message key="page.navigation.go-link"/>
            </a>
          </div>
        </div>
    </div>
</c:if>