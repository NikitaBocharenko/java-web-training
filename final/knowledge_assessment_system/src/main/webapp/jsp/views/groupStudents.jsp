<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
  <h4><fmt:message key="page.student-group-edit.students.title" /></h4>
  <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
    <thead>
      <tr>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.user-account.first-name" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.user-account.last-name" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.actions" /></th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="student" items="${students}">
          <tr>
            <td class="mdl-data-table__cell--non-numeric">${student.firstName}</td>
            <td class="mdl-data-table__cell--non-numeric">${student.lastName}</td>
            <fmt:message key="admin.user-list.actions.delete" var="deleteLabel" />
            <fmt:message key="page.student-group.delete-student-from-group" var="deleteMessage" />
            <td class="mdl-data-table__cell--non-numeric">
              <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                  <input type="hidden" name="userAccountId" value="${student.id}"/>
                  <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
                  <input type="hidden" name="command" value="delete_student_from_group"/>
                  <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
              </form>
            </td>
          </tr>
      </c:forEach>
    </tbody>
  </table>
  <fmt:message key="page.student-group-edit.add-user" var="addLabel" />
  <form class="action-form" action="app" method="POST">
      <input type="hidden" name="command" value="get_add_student_to_group"/>
      <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
      <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
  </form>
  <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/kas/app?command=view_groups"><h6><fmt:message key="page.navigation.back" /></h6></a>
  </nav>
</div>