<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="page.answer-option-add.title" /></h4>
        <table>
            <tr>
                <td><fmt:message key="entity.answer-option.option-text" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="optionText" name="optionText" value="${optionText}"
                            pattern="[A-Za-zА-Яа-яЁё\s0-9\.,!?-_]{0,100}" required>
                      <label class="mdl-textfield__label" for="optionText"><fmt:message key="entity.answer-option.option-text" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="validation.answer-option.option-text.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="isCorrect">
                      <input type="checkbox" id="isCorrect" name="isCorrect" class="mdl-checkbox__input"
                      <c:if test="${not empty isCorrect}"> checked</c:if>>
                      <span class="mdl-checkbox__label"><fmt:message key="entity.answer-option.is-correct" /></span>
                    </label>
                </td>
            </tr>
        </table>
        <table>
            <c:forEach var="error" items="${answerOptionErrors}">
                <tr><td><fmt:message key="validation.answer-option.${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="page.answer-option-add.submit" var="submitLabel" />
        <input type="hidden" name="questionId" value="${questionId}"/>
        <input type="hidden" name="command" value="post_add_answer_option"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_options&questionId=${questionId}"><h6><fmt:message key="page.answer-option-add.cancel" /></h6></a>
    </nav>
</div>