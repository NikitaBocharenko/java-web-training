<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="page.question-edit.title" /></h4>
        <table>
            <tr>
                <td><fmt:message key="entity.question.question-text" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="questionText" name="questionText" value="${question.questionText}"
                            pattern="[A-Za-zА-Яа-яЁё\s0-9\.,!?-_]{0,500}" required>
                      <label class="mdl-textfield__label" for="questionText"><fmt:message key="entity.question.question-text" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="validation.question.question-text.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><fmt:message key="entity.question.question-type" /></td>
                <td>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="questionTypeSingle">
                      <input type="radio" id="questionTypeSingle" class="mdl-radio__button" name="questionType" value="SINGLE_ANSWER"
                      <c:if test="${question.questionType.equals('single')}"> checked</c:if>>
                      <span class="mdl-radio__label"><fmt:message key="entity.question.question-type.single" /></span>
                    </label>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="questionTypeMultiply">
                      <input type="radio" id="questionTypeMultiply" class="mdl-radio__button" name="questionType" value="MULTIPLY_ANSWERS"
                      <c:if test="${question.questionType.equals('multiply')}"> checked</c:if>>
                      <span class="mdl-radio__label"><fmt:message key="entity.question.question-type.multiply" /></span>
                    </label>
                </td>
            </tr>
        </table>
        <table>
            <c:forEach var="error" items="${questionErrors}">
                <tr><td><fmt:message key="validation.question.${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="page.question-edit.submit" var="submitLabel" />
        <input type="hidden" name="questionId" value="${question.id}"/>
        <input type="hidden" name="testId" value="${question.testId}"/>
        <input type="hidden" name="command" value="post_edit_question"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_questions&testId=${question.testId}"><h6><fmt:message key="page.question-edit.cancel" /></h6></a>
    </nav>
</div>