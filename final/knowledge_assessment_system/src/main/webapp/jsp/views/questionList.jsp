<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.question-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.question.question-text" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.question.question-type" /></th>
          <th class="mdl-data-table__cell--non-numeric" colspan=3><fmt:message key="page.question-list.actions.label" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="question" items="${questions}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${question.questionText}</td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.question.question-type.${question.questionType}" /></td>
              <fmt:message key="page.question-list.actions.options" var="viewOptionListLabel" />
              <fmt:message key="page.question-list.actions.update" var="updateLabel" />
              <fmt:message key="page.question-list.actions.delete" var="deleteLabel" />
              <fmt:message key="page.question-list.actions.delete.message" var="deleteMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="questionId" value="${question.id}"/>
                    <input type="hidden" name="command" value="view_options"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewOptionListLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="questionId" value="${question.id}"/>
                    <input type="hidden" name="command" value="get_edit_question"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${updateLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                    <input type="hidden" name="questionId" value="${question.id}"/>
                    <input type="hidden" name="testId" value="${question.testId}"/>
                    <input type="hidden" name="command" value="delete_question"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <fmt:message key="page.question-list.actions.add" var="addLabel" />
    <form class="action-form" action="app" method="POST">
        <input type="hidden" name="command" value="get_add_question"/>
        <input type="hidden" name="testId" value="${testId}"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_tests"><h6><fmt:message key="page.question-list.navigation.back" /></h6></a>
    </nav>
</div>