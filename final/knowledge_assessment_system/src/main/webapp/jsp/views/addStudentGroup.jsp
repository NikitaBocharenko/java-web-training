<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="page.student-group-add.title" /></h4>
        <table>
            <tr>
                <td><fmt:message key="entity.student-group.group-name" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="groupName" name="groupName" value="${groupName}"
                            pattern="[A-Za-zА-Яа-яЁё\s0-9\.,!?-_]{0,150}" required>
                      <label class="mdl-textfield__label" for="groupName"><fmt:message key="entity.student-group.group-name" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="validation.student-group.group-name.not-matches" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <c:forEach var="error" items="${studentGroupErrors}">
                <tr><td><fmt:message key="validation.student-group.${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="page.student-group-add.submit" var="submitLabel" />
        <input type="hidden" name="creatorId" value="${sessionScope.get('userId')}"/>
        <input type="hidden" name="command" value="post_add_student_group"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_groups"><h6><fmt:message key="page.student-group-add.cancel" /></h6></a>
    </nav>
</div>