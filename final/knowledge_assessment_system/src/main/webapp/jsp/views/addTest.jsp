<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="page.test-add.title" /></h4>
        <table>
            <tr>
                <td><fmt:message key="entity.test.test-name" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="testName" name="testName" value="${testName}"
                            pattern="[A-Za-zА-Яа-яЁё\s0-9\.,!?-_]{0,100}" required>
                      <label class="mdl-textfield__label" for="testName"><fmt:message key="entity.test.test-name" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="validation.test.test-name.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><fmt:message key="entity.test.description" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="description" name="description" value="${description}"
                            pattern="[A-Za-zА-Яа-яЁё\s0-9\.,!?-_]{0,500}">
                      <label class="mdl-textfield__label" for="description"><fmt:message key="entity.test.description" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="validation.test.description.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><label for="deadlineTime"><fmt:message key="entity.test.deadline-time" /></label></td>
                <td><input type="datetime-local" id="deadlineTime" name="deadlineTime" value="${deadlineTime}"></td>
            </tr>
        </table>
        <table>
            <c:forEach var="error" items="${testErrors}">
                <tr><td><fmt:message key="validation.test.${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="page.test-add.submit" var="submitLabel" />
        <input type="hidden" name="creatorId" value="${sessionScope.get('userId')}"/>
        <input type="hidden" name="command" value="post_add_test"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_tests"><h6><fmt:message key="page.test-edit.cancel" /></h6></a>
    </nav>
</div>