<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.answer-option-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.answer-option.option-text" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.answer-option.is-correct" /></th>
          <th class="mdl-data-table__cell--non-numeric" colspan=2><fmt:message key="page.answer-option-list.actions.label" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="option" items="${answerOptions}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${option.optionText}</td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.answer-option.is-correct.${option.isCorrect}" /></td>
              <fmt:message key="page.answer-option-list.actions.update" var="updateLabel" />
              <fmt:message key="page.answer-option-list.actions.delete" var="deleteLabel" />
              <fmt:message key="page.answer-option-list.actions.delete.message" var="deleteMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="answerOptionId" value="${option.id}"/>
                    <input type="hidden" name="command" value="get_edit_answer_option"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${updateLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                    <input type="hidden" name="answerOptionId" value="${option.id}"/>
                    <input type="hidden" name="questionId" value="${option.questionId}"/>
                    <input type="hidden" name="command" value="delete_answer_option"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <fmt:message key="page.answer-option-list.actions.add" var="addLabel" />
    <form class="action-form" action="app" method="POST">
        <input type="hidden" name="command" value="get_add_answer_option"/>
        <input type="hidden" name="questionId" value="${questionId}"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_questions&testId=${testId}"><h6><fmt:message key="page.navigation.back" /></h6></a>
    </nav>
</div>