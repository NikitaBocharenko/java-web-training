<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="admin.user-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.login" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.first-name" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.last-name" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.email" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="admin.user-list.header.roles" /></th>
          <th class="mdl-data-table__cell--non-numeric" colspan=2><fmt:message key="admin.user-list.header.actions" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="account" items="${userAccounts}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${account.login}</td>
              <td class="mdl-data-table__cell--non-numeric">${account.firstName}</td>
              <td class="mdl-data-table__cell--non-numeric">${account.lastName}</td>
              <td class="mdl-data-table__cell--non-numeric">${account.email}</td>
              <td class="mdl-data-table__cell--non-numeric">
                  <c:forEach var="role" items="${account.userRoles}">
                      <fmt:message key="application.roles.${role.roleName}" />
                  </c:forEach>
              </td>
              <fmt:message key="admin.user-list.actions.update" var="updateLabel" />
              <fmt:message key="admin.user-list.actions.delete" var="deleteLabel" />
              <fmt:message key="admin.user-list.actions.delete.message" var="deleteMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="id" value="${account.id}"/>
                    <input type="hidden" name="command" value="get_edit_account"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${updateLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                    <input type="hidden" name="id" value="${account.id}"/>
                    <input type="hidden" name="command" value="delete_account"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=admin_menu"><fmt:message key="admin.menu.title"/></a>
    </nav>
</div>