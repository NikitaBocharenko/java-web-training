<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
  <h4><fmt:message key="page.student-group-edit.tests.title" /></h4>
  <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
    <thead>
      <tr>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.test-name" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.description" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.deadline-time" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creator" /></th>
        <th class="mdl-data-table__cell--non-numeric"><fmt:message key="page.actions.title" /></th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="test" items="${tests}">
          <tr>
            <td class="mdl-data-table__cell--non-numeric">${test.testName}</td>
            <td class="mdl-data-table__cell--non-numeric">${test.description}</td>
            <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.deadlineTime}" type="both" /></td>
            <td class="mdl-data-table__cell--non-numeric">${test.creator.firstName} ${test.creator.lastName}</td>
            <fmt:message key="page.actions.delete" var="deleteLabel" />
            <fmt:message key="page.student-group.delete-test-from-group" var="deleteMessage" />
            <td class="mdl-data-table__cell--non-numeric">
              <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                  <input type="hidden" name="testId" value="${test.id}"/>
                  <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
                  <input type="hidden" name="command" value="delete_test_from_group"/>
                  <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
              </form>
            </td>
          </tr>
      </c:forEach>
    </tbody>
  </table>
  <fmt:message key="page.student-group-edit.tests.assign" var="addLabel" />
  <form class="action-form" action="app" method="POST">
      <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
      <input type="hidden" name="command" value="get_assign_test_to_group"/>
      <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
  </form>
  <nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="/kas/app?command=view_groups"><h6><fmt:message key="page.navigation.back" /></h6></a>
  </nav>
</div>