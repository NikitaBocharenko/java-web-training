<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--2-col">
    <nav class="mdl-navigation">
        <table>
            <tr>
                <td><a class="mdl-navigation__link" href="/kas/app?command=view_student_groups"><fmt:message key="page.student-menu.groups" /></a></td>
            </tr>
        </table>
    </nav>
</div>