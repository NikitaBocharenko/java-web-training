<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="edit-user.title" /></h4>
        <table>
            <tr>
                <td><fmt:message key="login.input.login-placeholder" /></td>
                <td>${userAccount.login}</td>
            </tr>
            <tr>
                <td><fmt:message key="entity.user-account.first-name" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="firstName" name="firstName" value="${userAccount.firstName}"
                            pattern="[A-Za-zА-Яа-яЁё]{1,100}" required>
                      <label class="mdl-textfield__label" for="firstName"><fmt:message key="entity.user-account.first-name" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="registration.errors.first-name.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><fmt:message key="entity.user-account.last-name" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="lastName" name="lastName" value="${userAccount.lastName}"
                            pattern="[A-Za-zА-Яа-яЁё]{1,100}" required>
                      <label class="mdl-textfield__label" for="lastName"><fmt:message key="entity.user-account.last-name" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="registration.errors.last-name.not-matches" /></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><fmt:message key="entity.user-account.email" /></td>
                <td>
                    <div class="mdl-textfield mdl-js-textfield">
                      <input class="mdl-textfield__input" type="text" id="email" name="email" value="${userAccount.email}"
                            pattern="[A-Za-z0-9_\.-]+@[A-Za-z0-9_]+\.[A-Za-z]{2,4}" required>
                      <label class="mdl-textfield__label" for="email"><fmt:message key="entity.user-account.email" /></label>
                      <span class="mdl-textfield__error"><fmt:message key="registration.errors.email.not-matches" /></span>
                    </div>
                </td>
            </tr>
        </table>
        <h6><fmt:message key="admin.user-list.header.roles" /></h6>
        <table>
            <c:forEach var="role" items="${possibleRoles}">
                <tr>
                    <td>
                         <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="${ role.roleName }">
                           <input type="checkbox" id="${ role.roleName }" name="${ role.roleName }Role" class="mdl-switch__input"
                           <c:if test="${userAccount.userRoles.contains(role)}"> checked</c:if>>
                           <span class="mdl-switch__label"><fmt:message key="application.roles.${role.roleName}" /></span>
                         </label>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <table>
            <c:forEach var="error" items="${editUserAccountErrors}">
                <tr><td><fmt:message key="validation.user-account.${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="edit-user.submit" var="submitLabel" />
        <input type="hidden" name="id" value="${userAccount.id}"/>
        <input type="hidden" name="command" value="post_edit_account"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_users"><h6><fmt:message key="edit-user.cancel" /></h6></a>
    </nav>
</div>