<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:message key="login.input.login-placeholder" var="loginPlaceholder" />
<fmt:message key="login.input.password-placeholder" var="passwordPlaceholder" />
<fmt:message key="registration.errors.login.not-matches" var="loginTitle" />
<fmt:message key="registration.errors.password.not-matches" var="passwordTitle" />
<div class="form">
    <h3><fmt:message key="login.heading"/></h3>
    <form class="login-form" action="app" method="POST">
          <input type="text" placeholder="${loginPlaceholder}" title="${loginTitle}" name="login" pattern="[A-Za-z]{4,100}" required/>
          <input type="password" placeholder="${passwordPlaceholder}" title="${passwordTitle}" name="password" pattern="[A-Za-z0-9_]{4,100}" required/>
          <input type="hidden" name="command" value="login"/>
          <button><fmt:message key="login.input.submit"/></button>
          <p class="message"><fmt:message key="login.sign-up.text"/> <a href="/kas/app?command=prepare_register"><fmt:message key="login.sign-up.link"/></a></p>
    </form>
    <table>
        <c:forEach var="error" items="${loginErrors}">
            <tr>
                <td><fmt:message key="login.errors.${error}" /></td>
            </tr>
        </c:forEach>
    </table>
</div>