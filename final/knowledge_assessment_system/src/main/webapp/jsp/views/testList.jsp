<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.test-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.test-name" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.description" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creation-time" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.deadline-time" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creator" /></th>
          <th class="mdl-data-table__cell--non-numeric" colspan=4><fmt:message key="page.test-list.actions.label" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="test" items="${testList}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${test.testName}</td>
              <td class="mdl-data-table__cell--non-numeric">${test.description}</td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.creationTime}" type="both" /></td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.deadlineTime}" type="both" /></td>
              <td class="mdl-data-table__cell--non-numeric">${test.creator.firstName} ${test.creator.lastName}</td>
              <fmt:message key="page.test-list.actions.questions" var="viewQuestionsLabel" />
              <fmt:message key="page.test-list.actions.results" var="viewResultsLabel" />
              <fmt:message key="page.test-list.actions.update" var="updateLabel" />
              <fmt:message key="page.test-list.actions.delete" var="deleteLabel" />
              <fmt:message key="page.test-list.actions.delete.message" var="deleteMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="testId" value="${test.id}"/>
                    <input type="hidden" name="command" value="view_questions"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewQuestionsLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="testId" value="${test.id}"/>
                    <input type="hidden" name="command" value="view_results"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewResultsLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="testId" value="${test.id}"/>
                    <input type="hidden" name="command" value="get_edit_test"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${updateLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                    <input type="hidden" name="testId" value="${test.id}"/>
                    <input type="hidden" name="command" value="delete_test"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <fmt:message key="page.test-list.actions.add" var="addLabel" />
    <form class="action-form" action="app" method="POST">
        <input type="hidden" name="command" value="get_add_test"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <c:choose>
            <c:when test="${sessionScope.get('userRoles').contains('admin')}">
                <a class="mdl-navigation__link" href="/kas/app?command=admin_menu"><fmt:message key="admin.menu.title"/></a>
            </c:when>
            <c:otherwise>
                <a class="mdl-navigation__link" href="/kas/app?command=teacher_menu"><fmt:message key="teacher.menu.title"/></a>
            </c:otherwise>
        </c:choose>
    </nav>
</div>