<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <form action="app" method="POST">
        <h4><fmt:message key="page.student-group.add-student.title" /></h4>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
          <thead>
            <tr>
              <th class="mdl-data-table__cell--non-numeric"></th>
              <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.user-account.first-name" /></th>
              <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.user-account.last-name" /></th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="student" items="${students}" varStatus="status">
            <tr>
              <td><input type="checkbox" name="studentN${student.id}" class="mdl-checkbox__input"></td>
              <td class="mdl-data-table__cell--non-numeric">${student.firstName}</td>
              <td class="mdl-data-table__cell--non-numeric">${student.lastName}</td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
        <table>
            <c:forEach var="error" items="${editTestErrors}">
                <tr><td><fmt:message key="${error}" /></td></tr>
            </c:forEach>
        </table>
        <fmt:message key="page.actions.add" var="submitLabel" />
        <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
        <input type="hidden" name="command" value="post_add_student_to_group"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${submitLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_students_in_group&studentGroupId=${studentGroupId}"><h6><fmt:message key="page.navigation.back" /></h6></a>
    </nav>
</div>