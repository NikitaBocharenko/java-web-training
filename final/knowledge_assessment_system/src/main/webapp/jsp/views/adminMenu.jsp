<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--12-col">
    <h4><fmt:message key="admin-menu.title"/></h4>
</div>
<div class="mdl-cell mdl-cell--3-col">
    <div class="demo-card-square mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/users.png) bottom center no-repeat #3dbf5f">
        <h2 class="mdl-card__title-text"><fmt:message key="admin.menu.user-list.link"/></h2>
      </div>
      <div class="mdl-card__supporting-text">
        <fmt:message key="admin-menu.users.text"/>
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=view_users">
          <fmt:message key="page.navigation.go-link"/>
        </a>
      </div>
    </div>
</div>
<div class="mdl-cell mdl-cell--3-col">
    <div class="demo-card-square mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/student_group.png) bottom center no-repeat #3dbf5f">
        <h2 class="mdl-card__title-text"><fmt:message key="admin.menu.group-list.link"/></h2>
      </div>
      <div class="mdl-card__supporting-text">
        <fmt:message key="admin-menu.groups.text"/>
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=view_groups">
          <fmt:message key="page.navigation.go-link"/>
        </a>
      </div>
    </div>
</div>
<div class="mdl-cell mdl-cell--3-col">
    <div class="demo-card-square mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/test.png) bottom center no-repeat #3dbf5f">
        <h2 class="mdl-card__title-text"><fmt:message key="admin.menu.test-list.link"/></h2>
      </div>
      <div class="mdl-card__supporting-text">
        <fmt:message key="admin-menu.tests.text"/>
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=view_tests">
          <fmt:message key="page.navigation.go-link"/>
        </a>
      </div>
    </div>
</div>
<div class="mdl-cell mdl-cell--3-col">
    <div class="demo-card-square mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title mdl-card--expand" style="background: url(/kas/style/home.png) bottom center no-repeat #3dbf5f">
        <h2 class="mdl-card__title-text"><fmt:message key="welcome.link"/></h2>
      </div>
      <div class="mdl-card__actions mdl-card--border">
        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="/kas/app?command=to_welcome_page">
          <fmt:message key="page.navigation.go-link"/>
        </a>
      </div>
    </div>
</div>






