<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
  <h4><fmt:message key="page.student-group.assign-test.title" /></h4>
  <form class="action-form" action="app" method="POST">
      <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
          <tr>
            <th class="mdl-data-table__cell--non-numeric"></th>
            <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.test-name" /></th>
            <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.description" /></th>
            <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creation-time" /></th>
            <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.deadline-time" /></th>
            <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creator" /></th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="test" items="${tests}">
              <tr>
                <td><input type="checkbox" name="test${test.id}" class="mdl-checkbox__input"></td>
                <td class="mdl-data-table__cell--non-numeric">${test.testName}</td>
                <td class="mdl-data-table__cell--non-numeric">${test.description}</td>
                <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.creationTime}" type="both" /></td>
                <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.deadlineTime}" type="both" /></td>
                <td class="mdl-data-table__cell--non-numeric">${test.creator.firstName} ${test.creator.lastName}</td>
              </tr>
          </c:forEach>
        </tbody>
      </table>
      <fmt:message key="page.actions.assign" var="addLabel" />
      <input type="hidden" name="studentGroupId" value="${studentGroupId}"/>
      <input type="hidden" name="command" value="post_assign_test_to_group"/>
      <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
  </form>
  <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="/kas/app?command=view_test_assigned_to_group&studentGroupId=${studentGroupId}"><h6><fmt:message key="page.navigation.back" /></h6></a>
  </nav>
</div>