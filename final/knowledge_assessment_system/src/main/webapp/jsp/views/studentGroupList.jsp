<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.student-group.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
          <thead>
            <tr>
              <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.student-group.group-name" /></th>
              <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.student-group.creator" /></th>
              <th class="mdl-data-table__cell--non-numeric" colspan=2><fmt:message key="page.actions.title" /></th>
            </tr>
          </thead>
      <tbody>
        <c:forEach var="group" items="${groups}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${group.groupName}</td>
              <td class="mdl-data-table__cell--non-numeric">${group.creator.firstName} ${group.creator.lastName}</td>
              <fmt:message key="page.student-group.action.view-tests" var="viewTests" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="get_available_tests"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewTests}"/>
                </form>
              </td>
              <fmt:message key="page.student-group.action.leave" var="leaveLabel" />
              <fmt:message key="page.student-group.action.leave.message" var="leaveMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${leaveMessage}');">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="leave_student_group"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${leaveLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=to_welcome_page"><fmt:message key="welcome.link" /></a>
    </nav>
</div>