<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.test-result-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test-result.rank" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test-result.student" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test-result.result" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test-result.completion-time" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="page.actions.title" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="result" items="${results}" varStatus="status">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${status.count}</td>
              <td class="mdl-data-table__cell--non-numeric">${result.userAccount.firstName} ${result.userAccount.lastName}</td>
              <td class="mdl-data-table__cell--non-numeric">${result.result}</td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${result.completionTime}" type="both" /></td>
              <fmt:message key="page.test-result-list.result-details" var="resultDetailsLabel" />
              <td class="mdl-data-table__cell--non-numeric">
                  <form class="action-form" action="app" method="POST">
                      <input type="hidden" name="resultId" value="${result.id}"/>
                      <input type="hidden" name="command" value="view_test_result"/>
                      <input class="mdl-button mdl-js-button" type="submit" value="${resultDetailsLabel}"/>
                  </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_tests"><h6><fmt:message key="page.navigation.back" /></h6></a>
    </nav>
</div>