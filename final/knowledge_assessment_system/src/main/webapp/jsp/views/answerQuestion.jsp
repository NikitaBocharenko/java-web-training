<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4>${currentQuestionNumber + 1}/${questionQuantity}: ${question.questionText}</h4>
    <c:if test="${question.questionType.equals('single')}" var="isSingleOptionAnswer" />
    <form action="app">
        <table>
            <c:forEach items="${question.answerOptions}" var="option" varStatus="status">
                <tr>
                    <td>
                        <c:choose>
                            <c:when test="${isSingleOptionAnswer}">
                                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option${status.count}">
                                  <input type="radio" id="option${status.count}" class="mdl-radio__button" name="options" value="${option.id}">
                                  <span class="mdl-radio__label">${option.optionText}</span>
                                </label>
                            </c:when>
                            <c:otherwise>
                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox${status.count}">
                                  <input type="checkbox" id="checkbox${status.count}" class="mdl-checkbox__input" name="option${option.id}">
                                  <span class="mdl-checkbox__label">${option.optionText}</span>
                                </label>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <input type="hidden" name="questionId" value="${question.id}"/>
        <input type="hidden" name="command" value="do_test_passing"/>
        <fmt:message key="page.test.next-question" var="nextLabel" />
        <input class="mdl-button mdl-js-button" type="submit" value="${nextLabel}"/>
    </form>
    <table>
        <c:forEach var="error" items="${testTakingErrors}">
            <tr><td><fmt:message key="validation.test-taking.${error}" /></td></tr>
        </c:forEach>
    </table>
    <c:forEach items="${sessionScope.get('questionNumberList')}" var="item">
        <form action="app">
            <input type="hidden" name="questionNumber" value="${item}"/>
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab">
              ${item}
            </button>
        </form>
    </c:forEach>
</div>
