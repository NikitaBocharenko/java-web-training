<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.test-result.title" /></h4>
    <h6><fmt:message key="application.roles.student" />: ${student.firstName} ${student.lastName}</h6>
    <h6><fmt:message key="page.test-result.result-announcement" />: ${testResult.result}%</h6>
    <c:forEach items="${questions}" var="question" varStatus="questionStatus">
        <c:if test="${testResult.correctAnsweredQuestions.contains(question.id)}" var="isAnswerCorrect" />
        <h6>${questionStatus.count}. ${question.questionText} - <fmt:message key="page.test-result.question.is-correct.${isAnswerCorrect}" /></h6>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
            <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric"><fmt:message key="page.test-result.option.user-selection" /></th>
                    <th class="mdl-data-table__cell--non-numeric"><fmt:message key="page.test-result.option.is-correct" /></th>
                </tr>
            </thead>
            <c:forEach items="${question.answerOptions}" var="option" varStatus="optionStatus">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <c:if test="${testResult.answers[question.id].contains(option.id)}" var="isOptionSelected" />
                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox${optionStatus.count}">
                          <input type="checkbox" id="checkbox${optionStatus.count}" class="mdl-checkbox__input" name="option${option.id}"
                           <c:if test="${isOptionSelected}"> checked</c:if> disabled>
                          <span class="mdl-checkbox__label">${option.optionText}</span>
                        </label>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <fmt:message key="page.test-result.option.is-correct.${option.isCorrect}" />
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:forEach>
    <nav class="mdl-navigation">
        <c:choose>
            <c:when test="${!sessionScope.get('userRoles').contains('student')}">
                <a class="mdl-navigation__link" href="/kas/app?command=view_results&testId=${testResult.testId}"><fmt:message key="page.navigation.back"/></a>
            </c:when>
            <c:otherwise>
                <a class="mdl-navigation__link" href="/kas/app?command=view_student_groups"><fmt:message key="page.student-menu.groups"/></a>
            </c:otherwise>
        </c:choose>
    </nav>
</div>