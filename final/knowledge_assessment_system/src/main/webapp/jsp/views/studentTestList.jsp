<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.student-test.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.test-name" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.description" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.deadline-time" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.test.creator" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="page.actions.title" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="test" items="${tests}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${test.testName}</td>
              <td class="mdl-data-table__cell--non-numeric">${test.description}</td>
              <td class="mdl-data-table__cell--non-numeric"><fmt:formatDate value="${test.deadlineTime}" type="both" /></td>
              <td class="mdl-data-table__cell--non-numeric">${test.creator.firstName} ${test.creator.lastName}</td>
              <fmt:message key="page.student-test.actions.pass" var="passLabel" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="passingTestId" value="${test.id}"/>
                    <input type="hidden" name="command" value="start_test_passing"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${passLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="/kas/app?command=view_student_groups"><fmt:message key="page.student-group.title"/></a>
    </nav>
</div>