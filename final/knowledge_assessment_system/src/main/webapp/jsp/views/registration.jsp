<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:message key="registration.input.first-name-placeholder" var="firstNamePlaceholder" />
<fmt:message key="registration.input.last-name-placeholder" var="lastNamePlaceholder" />
<fmt:message key="registration.input.email-placeholder" var="emailPlaceholder" />
<fmt:message key="registration.input.login-placeholder" var="loginPlaceholder" />
<fmt:message key="registration.input.password-placeholder" var="passwordPlaceholder" />
<fmt:message key="registration.input.password-repeat-placeholder" var="passwordRepeatPlaceholder" />
<fmt:message key="registration.errors.first-name.not-matches" var="firstNameTitle" />
<fmt:message key="registration.errors.last-name.not-matches" var="lastNameTitle" />
<fmt:message key="registration.errors.email.not-matches" var="emailTitle" />
<fmt:message key="registration.errors.login.not-matches" var="loginTitle" />
<fmt:message key="registration.errors.password.not-matches" var="passwordTitle" />

<div class="form">
    <h3><fmt:message key="registration.heading"/></h3>
    <form class="register-form" action="app" method="POST">
        <input type="text" placeholder="${firstNamePlaceholder}" title="${firstNameTitle}" name="firstName" value="${firstName}" pattern="[A-Za-zА-Яа-яЁё]{1,100}" required/>
        <input type="text" placeholder="${lastNamePlaceholder}" title="${lastNameTitle}" name="lastName" value="${lastName}" pattern="[A-Za-zА-Яа-яЁё]{1,100}" required/>
        <input type="text" placeholder="${emailPlaceholder}" title="${emailTitle}" name="email" value="${email}" pattern="[A-Za-z0-9_\.-]+@[A-Za-z0-9_]+\.[A-Za-z]{2,4}" required/>
        <input type="text" placeholder="${loginPlaceholder}" title="${loginTitle}" name="login" value="${login}" pattern="[A-Za-z]{4,100}" required/>
        <input type="password" placeholder="${passwordPlaceholder}" title="${passwordTitle}" name="password" pattern="[A-Za-z0-9_]{4,100}" required/>
        <input type="password" placeholder="${passwordRepeatPlaceholder}" title="${passwordTitle}" name="passwordRepeat" pattern="[A-Za-z0-9_]{4,100}" required/>
        <fmt:message key="registration.roles.text"/>
        <table>
            <c:forEach var="role" items="${possibleRolesList}" varStatus="status">
                <tr>
                    <td><input type="checkbox" id="roles" name="${ role.roleName }Role"
                    <c:if test="${selectedRoles.contains(role)}"> checked</c:if>></td>
                    <td><fmt:message key="application.roles.${role.roleName}" /></td>
                </tr>
            </c:forEach>
        </table>
        <input type="hidden" name="command" value="register"/>
        <button><fmt:message key="registration.input.submit"/></button>
        <p class="message"><fmt:message key="registration.sign-in.text"/> <a href="/kas/app?command=get_login"><fmt:message key="registration.sign-in.link"/></a></p>
    </form>
    <table>
        <c:forEach var="error" items="${registerUserAccountErrors}">
            <tr>
                <td><fmt:message key="registration.errors.${error}" /></td>
            </tr>
        </c:forEach>
    </table>
</div>