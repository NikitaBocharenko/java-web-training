<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="mdl-cell mdl-cell--4-col">
    <h4><fmt:message key="page.student-group-list.title" /></h4>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
      <thead>
        <tr>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.student-group.group-name" /></th>
          <th class="mdl-data-table__cell--non-numeric"><fmt:message key="entity.student-group.creator" /></th>
          <th class="mdl-data-table__cell--non-numeric" colspan=4><fmt:message key="page.student-group-list.actions.label" /></th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="group" items="${studentGroups}">
            <tr>
              <td class="mdl-data-table__cell--non-numeric">${group.groupName}</td>
              <td class="mdl-data-table__cell--non-numeric">${group.creator.firstName} ${group.creator.lastName}</td>
              <fmt:message key="page.student-group-list.actions.students" var="viewStudentsLabel" />
              <fmt:message key="page.student-group-list.actions.tests" var="viewTestsLabel" />
              <fmt:message key="page.student-group-list.actions.update" var="updateLabel" />
              <fmt:message key="page.student-group-list.actions.delete" var="deleteLabel" />
              <fmt:message key="page.student-group-list.actions.delete.message" var="deleteMessage" />
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="view_students_in_group"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewStudentsLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="view_tests_assigned_to_group"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${viewTestsLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="get_edit_student_group"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${updateLabel}"/>
                </form>
              </td>
              <td class="mdl-data-table__cell--non-numeric">
                <form class="action-form" action="app" method="POST" onsubmit="return confirm('${deleteMessage}');">
                    <input type="hidden" name="studentGroupId" value="${group.id}"/>
                    <input type="hidden" name="command" value="delete_student_group"/>
                    <input class="mdl-button mdl-js-button" type="submit" value="${deleteLabel}"/>
                </form>
              </td>
            </tr>
        </c:forEach>
      </tbody>
    </table>
    <fmt:message key="page.student-group-list.actions.add" var="addLabel" />
    <form class="action-form" action="app" method="POST">
        <input type="hidden" name="command" value="get_add_student_group"/>
        <input class="mdl-button mdl-js-button" type="submit" value="${addLabel}"/>
    </form>
    <nav class="mdl-navigation">
        <c:choose>
            <c:when test="${sessionScope.get('userRoles').contains('admin')}">
                <a class="mdl-navigation__link" href="/kas/app?command=admin_menu"><fmt:message key="admin.menu.title"/></a>
            </c:when>
            <c:otherwise>
                <a class="mdl-navigation__link" href="/kas/app?command=teacher_menu"><fmt:message key="teacher.menu.title"/></a>
            </c:otherwise>
        </c:choose>
    </nav>
</div>