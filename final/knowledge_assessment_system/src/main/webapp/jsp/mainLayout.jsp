<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${requestScope.get('lang')}"/>
<fmt:setBundle basename = "/pagecontent" scope="application"/>
<html>
  <head>
    <!-- Material Design Lite -->
    <script src="js/material.min.js"></script>
    <script src="js/locale.js"></script>
    <link rel="stylesheet" href="style/material.light_green-green.min.css">
    <!-- Material Design icon font -->
    <link rel="stylesheet" href="style/material-and-icons.css">
    <link rel="stylesheet" href="style/dropdown.css">
    <link rel="stylesheet" href="style/forms.css">
    <link rel="stylesheet" href="style/cards.css">
    <title><fmt:message key="layout.title"/></title>
  </head>
  <body>
    <!-- Always shows a header, even in smaller screens. -->
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
          <!-- Title -->
          <span class="mdl-layout-title"><fmt:message key="layout.title"/></span>
          <!-- Add spacer, to align navigation to the right -->
          <div class="mdl-layout-spacer"></div>
          <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--2-col">
                ${userFirstName} ${userLastName}
            </div>
          </div>
          <div class="dropdown">
            <button class="dropbtn"><fmt:message key="layout.lang.title"/></button>
            <div class="dropdown-content">
              <a href="#" onclick="changeLang('en_US')"><fmt:message key="layout.lang.en"/></a>
              <a href="#" onclick="changeLang('ru_RU')"><fmt:message key="layout.lang.ru"/></a>
            </div>
          </div>
        </div>
      </header>
      <div class="mdl-layout__drawer">
        <span class="mdl-layout-title"><fmt:message key="layout.menu.title"/></span>
        <nav class="mdl-navigation">
              <c:if test="${userRoles.contains('teacher')}"><a class="mdl-navigation__link" href="/kas/app?command=teacher_menu"><fmt:message key="application.roles.teacher"/></a></c:if>
              <c:if test="${userRoles.contains('student')}"><a class="mdl-navigation__link" href="/kas/app?command=view_student_groups"><fmt:message key="application.roles.student"/></a></c:if>
              <c:if test="${userRoles.contains('admin')}"><a class="mdl-navigation__link" href="/kas/app?command=admin_menu"><fmt:message key="application.roles.admin"/></a></c:if>
          <a class="mdl-navigation__link" href="/kas/app?command=logout"><fmt:message key="layout.menu.exit"/></a>
        </nav>
      </div>
      <main class="mdl-layout__content">
        <div class="page-content">
            <div class="mdl-grid">
                <jsp:include page="views/${viewName}.jsp" />
            </div>
        </div>
      </main>
    </div>
  </body>
</html>