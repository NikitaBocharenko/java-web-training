package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.validator.RequestDataValidator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import by.training.kas.validator.ValidationResult;
import org.apache.log4j.Logger;

public class LoginCommand implements Command {
    private UserAccountService service;
    private RequestDataValidator requestDataValidator;

    public LoginCommand(UserAccountService service, RequestDataValidator requestDataValidator) {
        this.service = service;
        this.requestDataValidator = requestDataValidator;
    }

    private static final Logger log = Logger.getLogger(LoginCommand.class);

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Login command");
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            UserAccount userAccount = service.getByCredentials(login, password)
                    .orElseThrow(() -> new IllegalStateException("User account wasn't found by login/password"));
            HttpSession session = request.getSession();
            session.setAttribute("userId", userAccount.getId());
            session.setAttribute("userFirstName", userAccount.getFirstName());
            session.setAttribute("userLastName", userAccount.getLastName());
            Set<String> roleNames = userAccount.getUserRoles().stream()
                    .map(UserRole::getRoleName)
                    .collect(Collectors.toSet());
            session.setAttribute("userRoles", roleNames);
            log.info("User logged in: " + userAccount);
            return RedirectParamStringBuilder.build(CommandType.TO_WELCOME_PAGE, new HashMap<>());
        } else {
            request.setAttribute(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE,
                    validationResult.getErrorsByType(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE));
            log.debug(validationResult);
            return "login";
        }
    }
}
