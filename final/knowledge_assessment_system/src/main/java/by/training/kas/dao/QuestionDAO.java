package by.training.kas.dao;

import by.training.kas.entity.Question;

import java.sql.SQLException;
import java.util.List;

public interface QuestionDAO extends EntityDAO<Question> {
    List<Question> selectByTestId(long testId) throws SQLException, DAOException;

    Question selectByTestIdAndNumber(long testId, int number) throws SQLException, DAOException;
}
