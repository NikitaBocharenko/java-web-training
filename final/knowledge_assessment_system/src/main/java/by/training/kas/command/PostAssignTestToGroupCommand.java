package by.training.kas.command;

import by.training.kas.service.StudentGroupService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PostAssignTestToGroupCommand implements Command {
    private StudentGroupService studentGroupService;
    private static final Logger log = Logger.getLogger(PostAssignTestToGroupCommand.class);

    public PostAssignTestToGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        Set<String> paramNames = request.getParameterMap().keySet();
        List<Long> testIdsToAssignToGroup = paramNames.stream()
                .filter(paramName -> paramName.startsWith("test"))
                .map(paramName -> paramName.replace("test", ""))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        log.debug(testIdsToAssignToGroup);
        studentGroupService.assignTestToGroup(testIdsToAssignToGroup, studentGroupId);
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("studentGroupId", studentGroupIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_TESTS_ASSIGNED_TO_GROUP, redirectParams);
    }
}
