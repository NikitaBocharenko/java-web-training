package by.training.kas.command;

import by.training.kas.dto.TestDTO;
import by.training.kas.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetAvailableTestsCommand implements Command {
    private TestService testService;

    public GetAvailableTestsCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        HttpSession session = request.getSession();
        long studentId = (Long) session.getAttribute("userId");
        List<TestDTO> tests = testService.getAvailableForPassing(studentGroupId, studentId);
        request.setAttribute("tests", tests);
        return "studentTestList";
    }
}
