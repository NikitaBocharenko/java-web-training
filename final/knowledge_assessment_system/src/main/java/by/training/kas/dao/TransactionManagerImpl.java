package by.training.kas.dao;

import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManagerImpl implements TransactionManager {
    private ConnectionPool connectionPool;
    private ThreadLocal<Connection> connectionThreadLocal;

    private static final Logger log = Logger.getLogger(TransactionManagerImpl.class);

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        connectionThreadLocal = new ThreadLocal<>();
    }

    @Override
    public void beginTransaction() {
        try {
            if (connectionThreadLocal.get() == null) {
                Connection connection = connectionPool.getConnection();
                connection.setAutoCommit(false);
                log.debug("Transaction begin: " + connection);
                connectionThreadLocal.set(connection);
            } else {
                log.error("Transaction already started");
            }
        } catch (SQLException e) {
            throw new TransactionException("SQLException was thrown during creating transaction: " + e.getMessage(), e);
        }

    }

    @Override
    public void commitTransaction() {
        try {
            Connection connection = connectionThreadLocal.get();
            if (connection != null) {
                connection.commit();
                connection.setAutoCommit(true);
                log.debug("Transaction commit: " + connection);
                connectionThreadLocal.remove();
                connection.close();
            } else {
                log.error("Connection is null");
            }
        } catch (SQLException e) {
            throw new TransactionException("SQLException was thrown during transaction commit: " + e.getMessage(), e);
        }
    }

    @Override
    public void rollbackTransaction() {
        try {
            Connection connection = connectionThreadLocal.get();
            if (connection != null) {
                connection.rollback();
                connection.setAutoCommit(true);
                log.debug("Transaction rollback: " + connection);
                connectionThreadLocal.remove();
                connection.close();
            } else {
                log.error("Connection is null");
            }
        } catch (SQLException e) {
            throw new TransactionException("SQLException was thrown during transaction rollback: " + e.getMessage(), e);
        }
    }

    @Override
    public Connection getConnection() {
        Connection connection = connectionThreadLocal.get();
        if (connection != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            return method.invoke(connection, args);
                        }
                    });
        }
        return null;
    }
}
