package by.training.kas.command;

import by.training.kas.entity.AnswerOption;
import by.training.kas.service.AnswerOptionService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static by.training.kas.application.ApplicationConstants.VALIDATION_ANSWER_OPTION_ERRORS_TYPE;

public class PostEditAnswerOptionCommand implements Command {
    private AnswerOptionService answerOptionService;
    private RequestDataValidator requestDataValidator;

    public PostEditAnswerOptionCommand(AnswerOptionService answerOptionService, RequestDataValidator requestDataValidator) {
        this.answerOptionService = answerOptionService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            AnswerOption answerOption = buildAnswerOption(request);
            answerOptionService.update(answerOption);
            String questionIdStr = request.getParameter("questionId");
            Map<String, String> redirectParams = new HashMap<>();
            redirectParams.put("questionId", questionIdStr);
            return RedirectParamStringBuilder.build(CommandType.VIEW_OPTIONS, redirectParams);
        } else {
            request.setAttribute(VALIDATION_ANSWER_OPTION_ERRORS_TYPE, validationResult.getErrorsByType(VALIDATION_ANSWER_OPTION_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_EDIT_ANSWER_OPTION);
            return command.process(request, response);
        }
    }

    private AnswerOption buildAnswerOption(HttpServletRequest request) {
        String idStr = request.getParameter("answerOptionId");
        long id = Long.parseLong(idStr);
        String optionText = request.getParameter("optionText");
        String isCorrectStr = request.getParameter("isCorrect");
        boolean isCorrect = isCorrectStr != null;
        return new AnswerOption(id, optionText, isCorrect, -1);
    }
}
