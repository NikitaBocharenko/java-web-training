package by.training.kas.service;

import by.training.kas.entity.Test;
import by.training.kas.dto.TestDTO;

import java.util.List;

public interface TestService {
    List<TestDTO> getAll();
    TestDTO getById(long id);
    void update(Test test);
    void add(Test test);
    void delete(long id);
    List<TestDTO> getAllAssignedToGroup(long studentGroupId);
    List<TestDTO> getAllNotInGroup(long studentGroupId);
    List<TestDTO> getAllByCreator(long creatorId);
    List<TestDTO> getAllNotInGroupByCreator(long studentGroupId, long creatorId);
    List<TestDTO> getAvailableForPassing(long studentGroupId, long studentId);
}
