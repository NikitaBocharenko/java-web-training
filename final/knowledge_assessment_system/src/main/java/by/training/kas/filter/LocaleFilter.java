package by.training.kas.filter;

import org.apache.log4j.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebFilter(urlPatterns = {"/"}, servletNames = {"app"})
public class LocaleFilter implements Filter {
    private static final Logger log = Logger.getLogger(LocaleFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Cookie[] requestCookies = request.getCookies();
        Optional<Cookie> localeCookieOptional = Optional.empty();
        if (requestCookies != null) {
            localeCookieOptional = Arrays.stream(requestCookies)
                    .filter(cookie -> cookie.getName().equalsIgnoreCase("lang"))
                    .filter(cookie -> cookie.getValue().equalsIgnoreCase("ru_ru") ||
                            cookie.getValue().equalsIgnoreCase("en_us"))
                    .findFirst();
        }
        String langValue;
        if (localeCookieOptional.isPresent()) {
            log.debug("locale cookie found");
            log.debug(localeCookieOptional.get().getName() + " " + localeCookieOptional.get().getValue());
            langValue = localeCookieOptional.get().getValue();
        } else {
            log.debug("locale cookie wasn't found");
            ((HttpServletResponse) servletResponse).addCookie(new Cookie("lang", "en_US"));
            langValue = "en_US";
        }
        request.setAttribute("lang", langValue);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
