package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetAddStudentToGroupCommand implements Command {
    private UserAccountService userAccountService;

    public GetAddStudentToGroupCommand(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        List<UserAccount> students = userAccountService.getStudentsNotInGroup(studentGroupId);
        request.setAttribute("students", students);
        request.setAttribute("studentGroupId", studentGroupId);
        return "addStudentToGroup";
    }
}
