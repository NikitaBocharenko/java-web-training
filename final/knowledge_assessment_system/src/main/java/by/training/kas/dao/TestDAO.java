package by.training.kas.dao;

import by.training.kas.entity.Test;

import java.sql.SQLException;
import java.util.List;

public interface TestDAO extends EntityDAO<Test> {
    List<Test> selectAssignedToGroup(long studentGroupId) throws SQLException;

    List<Test> selectAllNotAssignedToGroup(long studentGroupId) throws SQLException;

    List<Test> selectByCreatorId(long creatorId) throws SQLException;

    List<Test> selectAllNotAssignedToGroupByCreatorId(long studentGroupId, long creatorId) throws SQLException;

    List<Test> selectAvailableForPassing(long studentGroupId, long studentId) throws SQLException;
}
