package by.training.kas.command;

import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class DeleteStudentGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public DeleteStudentGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("studentGroupId");
        long id = Long.parseLong(idStr);
        studentGroupService.delete(id);
        return RedirectParamStringBuilder.build(CommandType.VIEW_GROUPS, new HashMap<>());
    }
}
