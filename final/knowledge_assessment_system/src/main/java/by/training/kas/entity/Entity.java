package by.training.kas.entity;

public abstract class Entity {
    protected long id;

    public long getId() {
        return id;
    }
}
