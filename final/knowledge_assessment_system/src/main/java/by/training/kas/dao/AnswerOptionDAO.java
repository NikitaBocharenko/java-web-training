package by.training.kas.dao;

import by.training.kas.entity.AnswerOption;

import java.sql.SQLException;
import java.util.List;

public interface AnswerOptionDAO extends EntityDAO<AnswerOption> {
    List<AnswerOption> selectByQuestionId(long questionId) throws SQLException;
}
