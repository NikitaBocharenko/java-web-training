package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.UserRoleDAO;
import by.training.kas.entity.UserRole;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class UserRoleServiceImpl implements UserRoleService {
    private UserRoleDAO dao;

    public UserRoleServiceImpl(UserRoleDAO dao) {
        this.dao = dao;
    }

    @Override
    public Set<UserRole> getPossibleRoles() {
        try {
            return dao.selectPossibleRoles();
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<UserRole> getAll() {
        try {
            return dao.selectAll();
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public UserRole getByName(String roleName) {
        try {
            return dao.selectByName(roleName);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }
}
