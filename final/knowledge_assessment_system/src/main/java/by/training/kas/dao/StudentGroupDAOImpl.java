package by.training.kas.dao;

import by.training.kas.entity.StudentGroup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT;

public class StudentGroupDAOImpl implements StudentGroupDAO {
    private ConnectionManager connectionManager;

    private static final String SELECT_ALL = "select id, group_name, creator_id from student_group";
    private static final String SELECT_BY_ID = SELECT_ALL + " where id = ?";
    private static final String SELECT_BY_CREATOR_ID = SELECT_ALL + " where creator_id = ?";
    private static final String SELECT_BY_MEMBER_ID = SELECT_ALL + " where id in " +
            "(select student_group_id from user_account_has_student_group where user_account_id = ?)";
    private static final String INSERT = "insert into student_group (group_name, creator_id) values(?, ?)";
    private static final String UPDATE = "update student_group set group_name = ? where id = ?";
    private static final String DELETE = "delete from student_group where id = ?";
    private static final String ADD_STUDENT_TO_GROUP = "insert into user_account_has_student_group (user_account_id, student_group_id) values (?, ?)";
    private static final String DELETE_STUDENT_FROM_GROUP = "delete from user_account_has_student_group where user_account_id = ? and student_group_id = ?";
    private static final String ASSIGN_TEST_TO_GROUP = "insert into test_has_student_group (test_id, student_group_id) values (?, ?)";
    private static final String DELETE_TEST_FROM_GROUP = "delete from test_has_student_group where test_id = ? and student_group_id = ?";

    public StudentGroupDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    private StudentGroup parse(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String groupName = resultSet.getString("group_name");
        long creatorId = resultSet.getLong("creator_id");
        return new StudentGroup(id, groupName, creatorId);
    }

    @Override
    public long insert(StudentGroup entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)) {
            int i = 0;
            statement.setString(++i, entity.getGroupName());
            statement.setLong(++i, entity.getCreatorId());
            return statement.executeUpdate();
        }
    }

    @Override
    public StudentGroup select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            statement.setLong(++i, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return parse(resultSet);
                } else {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + StudentGroupDAOImpl.class.getSimpleName() + ", " + id);
                }
            }
        }
    }

    @Override
    public List<StudentGroup> selectAll() throws SQLException {
        List<StudentGroup> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                StudentGroup studentGroup = parse(resultSet);
                result.add(studentGroup);
            }
        }
        return result;
    }

    @Override
    public void update(StudentGroup entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            int i = 0;
            statement.setString(++i, entity.getGroupName());
            statement.setLong(++i, entity.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE)) {
            int i = 0;
            statement.setLong(++i, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void addStudentToGroup(long userAccountId, long studentGroupId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(ADD_STUDENT_TO_GROUP)) {
            int i = 0;
            statement.setLong(++i, userAccountId);
            statement.setLong(++i, studentGroupId);
            statement.executeUpdate();
        }
    }

    @Override
    public void deleteStudentFromGroup(long userAccountId, long studentGroupId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_STUDENT_FROM_GROUP)) {
            int i = 0;
            statement.setLong(++i, userAccountId);
            statement.setLong(++i, studentGroupId);
            statement.executeUpdate();
        }
    }

    @Override
    public void assignTestToGroup(long testId, long studentGroupId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(ASSIGN_TEST_TO_GROUP)) {
            int i = 0;
            statement.setLong(++i, testId);
            statement.setLong(++i, studentGroupId);
            statement.executeUpdate();
        }
    }

    @Override
    public void deleteTestFromGroup(long testId, long studentGroupId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_TEST_FROM_GROUP)) {
            int i = 0;
            statement.setLong(++i, testId);
            statement.setLong(++i, studentGroupId);
            statement.executeUpdate();
        }
    }

    @Override
    public List<StudentGroup> selectByCreatorId(long creatorId) throws SQLException {
        List<StudentGroup> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_CREATOR_ID)) {
            int i = 0;
            statement.setLong(++i, creatorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    StudentGroup studentGroup = parse(resultSet);
                    result.add(studentGroup);
                }
            }
        }
        return result;
    }

    @Override
    public List<StudentGroup> selectByMemberId(long memberId) throws SQLException {
        List<StudentGroup> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_MEMBER_ID)) {
            int i = 0;
            statement.setLong(++i, memberId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    StudentGroup studentGroup = parse(resultSet);
                    result.add(studentGroup);
                }
            }
        }
        return result;
    }
}
