package by.training.kas.dto;

import by.training.kas.entity.StudentGroup;
import by.training.kas.entity.UserAccount;

public class StudentGroupDTO {
    private long id;
    private String groupName;
    private UserAccount creator;

    public StudentGroupDTO(StudentGroup studentGroup, UserAccount creator) {
        this.id = studentGroup.getId();
        this.groupName = studentGroup.getGroupName();
        this.creator = creator;
    }

    public long getId() {
        return id;
    }

    public String getGroupName() {
        return groupName;
    }

    public UserAccount getCreator() {
        return creator;
    }
}
