package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class DeleteAccountCommand implements Command {
    private UserAccountService userAccountService;

    public DeleteAccountCommand(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter(ApplicationConstants.USER_ACCOUNT_ID_ATTRIBUTE_NAME);
        long id = Long.parseLong(idStr);
        userAccountService.delete(id);
        return RedirectParamStringBuilder.build(CommandType.VIEW_USERS, new HashMap<>());
    }
}
