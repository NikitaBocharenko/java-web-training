package by.training.kas.dto;

import by.training.kas.entity.AnswerOption;
import by.training.kas.entity.Question;
import by.training.kas.entity.QuestionType;

import java.util.List;

public class QuestionDTO {
    private long id;
    private String questionText;
    private String questionType;
    private long testId;
    private List<AnswerOption> answerOptions;
    private boolean isCorrect;

    public QuestionDTO(Question question, List<AnswerOption> answerOptions) {
        this.id = question.getId();
        this.questionText = question.getQuestionText();
        this.questionType = question.getQuestionType().equals(QuestionType.SINGLE_ANSWER) ? "single" : "multiply";
        this.testId = question.getTestId();
        this.answerOptions = answerOptions;
    }

    public long getId() {
        return id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getQuestionType() {
        return questionType;
    }

    public long getTestId() {
        return testId;
    }

    public List<AnswerOption> getAnswerOptions() {
        return answerOptions;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }
}
