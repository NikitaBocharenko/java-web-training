package by.training.kas.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum QuestionType {
    SINGLE_ANSWER, MULTIPLY_ANSWERS;

    public static Optional<QuestionType> fromString(String questionTypeName) {
        return Stream.of(QuestionType.values()).filter(questionType -> questionType.name().equalsIgnoreCase(questionTypeName)).findFirst();
    }
}
