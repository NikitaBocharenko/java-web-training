package by.training.kas.command;

import by.training.kas.dto.TestDTO;
import by.training.kas.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetTestsAssignedToGroupCommand implements Command {
    private TestService testService;

    public GetTestsAssignedToGroupCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("studentGroupId");
        long id = Long.parseLong(idStr);
        List<TestDTO> tests = testService.getAllAssignedToGroup(id);
        request.setAttribute("tests", tests);
        request.setAttribute("studentGroupId", id);
        return "groupTests";
    }
}
