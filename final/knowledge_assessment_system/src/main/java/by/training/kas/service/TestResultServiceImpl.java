package by.training.kas.service;

import by.training.kas.dao.*;
import by.training.kas.dto.TestResultDTO;
import by.training.kas.entity.Answer;
import by.training.kas.entity.AnswerOption;
import by.training.kas.entity.TestResult;
import by.training.kas.entity.UserAccount;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class TestResultServiceImpl implements TestResultService {
    private AnswerOptionDAO answerOptionDAO;
    private TestResultDAO testResultDAO;
    private UserAccountDAO userAccountDAO;
    private AnswerDAO answerDAO;
    private TransactionManager transactionManager;

    private static final Logger log = Logger.getLogger(TestResultServiceImpl.class);

    public TestResultServiceImpl(TestResultDAO testResultDAO, AnswerDAO answerDAO, UserAccountDAO userAccountDAO, AnswerOptionDAO answerOptionDAO, TransactionManager transactionManager) {
        this.answerOptionDAO = answerOptionDAO;
        this.testResultDAO = testResultDAO;
        this.answerDAO = answerDAO;
        this.userAccountDAO = userAccountDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public long submitTest(long testId, long userAccountId, Map<Long, Set<Long>> answers) {
        long resultId;
        transactionManager.beginTransaction();
        try {
            //calculate test result
            int correctQuestionQuantity = 0;
            Set<Long> correctQuestionIdSet = new HashSet<>();
            Set<Map.Entry<Long, Set<Long>>> userAnswers = answers.entrySet();
            for (Map.Entry<Long, Set<Long>> userAnswer : userAnswers) {
                List<AnswerOption> answerOptions = answerOptionDAO.selectByQuestionId(userAnswer.getKey());
                Set<Long> correctOptions = answerOptions.stream()
                        .filter(AnswerOption::getIsCorrect)
                        .map(AnswerOption::getId)
                        .collect(Collectors.toSet());
                if (correctOptions.equals(userAnswer.getValue())) {
                    correctQuestionQuantity++;
                    correctQuestionIdSet.add(userAnswer.getKey());
                }
            }
            int totalQuestionQuantity = answers.size();
            int result = correctQuestionQuantity * 100 / totalQuestionQuantity;
            log.debug("result: " + result);
            //saving test result
            TestResult testResult = new TestResult(-1, result, new Date(), userAccountId, testId);
            resultId = testResultDAO.insert(testResult);
            log.debug("resultId: " + resultId);
            //saving answers
            for (Map.Entry<Long, Set<Long>> userAnswer : userAnswers) {
                boolean isCorrectAnswer = correctQuestionIdSet.contains(userAnswer.getKey());
                Answer answer = new Answer(-1, userAnswer.getKey(), resultId, isCorrectAnswer);
                long answerId = answerDAO.insert(answer);
                log.debug("answerId: " + answerId);
                //saving selected answer options
                for (Long selectedAnswerOptionId : userAnswer.getValue()) {
                    answerDAO.insertSelectedOptions(answerId, selectedAnswerOptionId);
                }
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return resultId;
    }

    @Override
    public TestResultDTO getById(long resultId) {
        TestResult testResult;
        Map<Long, Set<Long>> answersWithSelectedOptions = new HashMap<>();
        Set<Long> correctAnsweredQuestions = new HashSet<>();
        transactionManager.beginTransaction();
        try {
            testResult = testResultDAO.select(resultId);
            List<Answer> answers = answerDAO.selectByResultId(testResult.getId());
            for (Answer answer : answers) {
                if (answer.getIsCorrect()) {
                    correctAnsweredQuestions.add(answer.getQuestionId());
                }
                Set<Long> userSelectedOptions = answerDAO.selectUserOptions(answer.getId());
                answersWithSelectedOptions.put(answer.getQuestionId(), userSelectedOptions);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return new TestResultDTO(testResult, answersWithSelectedOptions, correctAnsweredQuestions);
    }

    @Override
    public List<TestResultDTO> getByTestId(long testId) {
        List<TestResultDTO> testResultDTOS = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<TestResult> testResults = testResultDAO.selectByTestId(testId);
            for (TestResult result : testResults) {
                UserAccount studentAccount = userAccountDAO.select(result.getUserAccountId());
                TestResultDTO resultDTO = new TestResultDTO(result, studentAccount);
                testResultDTOS.add(resultDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testResultDTOS;
    }
}
