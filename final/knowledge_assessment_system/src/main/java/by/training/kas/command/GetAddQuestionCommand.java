package by.training.kas.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetAddQuestionCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("testId", request.getParameter("testId"));
        return "addQuestion";
    }
}
