package by.training.kas.service;

import by.training.kas.entity.UserAccount;

import java.util.List;
import java.util.Optional;

public interface UserAccountService {
    Optional<UserAccount> getByCredentials(String login, String password);
    void add(UserAccount userAccount);
    boolean isLoginAlreadyExists(String login);
    List<UserAccount> getAll();
    UserAccount getById(long id);
    void set(UserAccount userAccount);
    void delete(long id);
    List<UserAccount> getStudentsInGroup(long studentGroupId);
    List<UserAccount> getStudentsNotInGroup(long studentGroupId);
}
