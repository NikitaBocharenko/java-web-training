package by.training.kas.dao;

import by.training.kas.entity.UserAccount;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static by.training.kas.application.ApplicationConstants.*;

public class UserAccountDAOImpl implements UserAccountDAO {
    private ConnectionManager connectionManager;

    private static final String SELECT_ALL = "select a.id, a.login, a.password, a.first_name, a.last_name, a.email from user_account a ";
    private static final String SELECT_LOGIN_COUNT = "select count(*) from user_account where login = ?";
    private static final String INSERT_USER_ACCOUNT = "insert into user_account (login, password, first_name, last_name, email) " +
                                                        "values (?, ?, ?, ?, ?)";
    private static final String SELECT_BY_LOGIN_PASSWORD = SELECT_ALL + "where a.login = ? and a.password = ?";
    private static final String UPDATE_ACCOUNT = "update user_account set first_name = ?, last_name = ?, email = ? where id = ?";
    private static final String DELETE_ACCOUNT = "delete from user_account where id = ?";
    private static final String SELECT_BY_STUDENT_GROUP_ID = SELECT_ALL + "join user_account_has_student_group t on a.id = t.user_account_id " +
                                                                "where t.student_group_id = ?";
    private static final String SELECT_STUDENTS_NOT_IN_GROUP =  SELECT_ALL + "join user_account_has_user_role b on a.id = b.user_account_id " +
                                                                    "join user_role c on b.user_role_id = c.id where c.role_name = 'student' " +
                                                                    "and a.id not in (select d.user_account_id from user_account_has_student_group d " +
                                                                    "where d.student_group_id = ?)";

    private static final Logger log = Logger.getLogger(UserAccountDAOImpl.class);

    public UserAccountDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public long insert(UserAccount entity) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(INSERT_USER_ACCOUNT, Statement.RETURN_GENERATED_KEYS)){
                int i = 0;
                statement.setString(++i, entity.getLogin());
                statement.setString(++i, entity.getPassword());
                statement.setString(++i, entity.getFirstName());
                statement.setString(++i, entity.getLastName());
                statement.setString(++i, entity.getEmail());
                statement.executeUpdate();
                try (ResultSet resultSet = statement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        return resultSet.getLong(1);
                    } else {
                        throw new DAOException(EXCEPTION_KEY_NOT_GENERATED_TEXT + " in class " + UserAccountDAOImpl.class.getSimpleName());
                    }
                }
            }
        }
    }

    @Override
    public UserAccount select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection()) {
            String sql = SELECT_ALL + " where id = " + id;
            try (PreparedStatement statement = connection.prepareStatement(sql);
                 ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return this.buildAccount(resultSet);
                } else {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + UserAccountDAOImpl.class.getSimpleName() + ", " + id);
                }
            }
        }
    }

    private UserAccount buildAccount(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String email = resultSet.getString("email");
        return new UserAccount(id, login, password, firstName, lastName, email);
    }

    @Override
    public List<UserAccount> selectAll() throws SQLException {
        try (Connection connection = connectionManager.getConnection()) {
            List<UserAccount> accounts = new ArrayList<>();
            try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
                 ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    UserAccount userAccount = buildAccount(resultSet);
                    accounts.add(userAccount);
                }
            }
            return accounts;
        }
    }

    @Override
    public void update(UserAccount entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(UPDATE_ACCOUNT)) {
                int i = 0;
                statement.setString(++i, entity.getFirstName());
                statement.setString(++i, entity.getLastName());
                statement.setString(++i, entity.getEmail());
                statement.setLong(++i, entity.getId());
                log.debug(statement);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(DELETE_ACCOUNT)) {
                int i = 0;
                statement.setLong(++i, id);
                log.debug(statement);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public boolean isLoginAlreadyExists(String login) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SELECT_LOGIN_COUNT)) {
                statement.setString(1, login);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt(1);
                        return count > 0;
                    } else {
                        throw new DAOException("Cannot check is user already exists");
                    }
                }
            }
        }
    }

    @Override
    public Optional<UserAccount> selectByLoginPassword(String login, String password) throws SQLException {
        try (Connection connection = connectionManager.getConnection()) {
            try (PreparedStatement accountStatement = connection.prepareStatement(SELECT_BY_LOGIN_PASSWORD)) {
                int i = 0;
                accountStatement.setString(++i, login);
                accountStatement.setString(++i, password);
                log.debug(accountStatement);
                try (ResultSet accountResultSet = accountStatement.executeQuery()) {
                    if (accountResultSet.next()) {
                        UserAccount userAccount = this.buildAccount(accountResultSet);
                        return Optional.of(userAccount);
                    }
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public List<UserAccount> selectStudentsInGroup(long studentGroupId) throws SQLException {
        List<UserAccount> accounts = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_STUDENT_GROUP_ID)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    UserAccount userAccount = buildAccount(resultSet);
                    accounts.add(userAccount);
                }
            }
            return accounts;
        }
    }

    @Override
    public List<UserAccount> selectStudentsNotInGroup(long studentGroupId) throws SQLException {
        List<UserAccount> accounts = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_STUDENTS_NOT_IN_GROUP)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
             try (ResultSet resultSet = statement.executeQuery()) {
                 while (resultSet.next()) {
                     UserAccount userAccount = buildAccount(resultSet);
                     accounts.add(userAccount);
                 }
             }
            return accounts;
        }
    }
}
