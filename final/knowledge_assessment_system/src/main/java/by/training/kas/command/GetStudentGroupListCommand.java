package by.training.kas.command;

import by.training.kas.dto.StudentGroupDTO;
import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

public class GetStudentGroupListCommand implements Command {
    private StudentGroupService studentGroupService;

    public GetStudentGroupListCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Set<String> roleNames = (Set<String>) session.getAttribute("userRoles");
        List<StudentGroupDTO> groups;
        if (roleNames.contains("admin")) {
            groups = studentGroupService.getAll();
        } else {
            long creatorId = (long) session.getAttribute("userId");
            groups = studentGroupService.getByCreator(creatorId);
        }
        request.setAttribute("studentGroups", groups);
        return "groupList";
    }
}
