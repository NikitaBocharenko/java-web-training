package by.training.kas.service;

import by.training.kas.dao.AnswerOptionDAO;
import by.training.kas.dao.DAOException;
import by.training.kas.dao.QuestionDAO;
import by.training.kas.dao.TransactionManager;
import by.training.kas.dto.QuestionDTO;
import by.training.kas.entity.AnswerOption;
import by.training.kas.entity.Question;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class QuestionServiceImpl implements QuestionService {
    private QuestionDAO questionDAO;
    private AnswerOptionDAO answerOptionDAO;
    private TransactionManager transactionManager;

    public QuestionServiceImpl(QuestionDAO questionDAO, AnswerOptionDAO answerOptionDAO, TransactionManager transactionManager) {
        this.questionDAO = questionDAO;
        this.answerOptionDAO = answerOptionDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public void add(Question question) {
        try {
            questionDAO.insert(question);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public QuestionDTO getById(long id) {
        Question question;
        List<AnswerOption> answerOptions;
        transactionManager.beginTransaction();
        try {
            question = questionDAO.select(id);
            answerOptions = answerOptionDAO.selectByQuestionId(id);
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return new QuestionDTO(question, answerOptions);
    }

    @Override
    public void update(Question question) {
        try {
            questionDAO.update(question);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void delete(long id) {
        try {
            questionDAO.delete(id);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public QuestionDTO getByTestIdAndNumber(long testId, int number) {
        Question question;
        List<AnswerOption> answerOptions;
        transactionManager.beginTransaction();
        try {
            question = questionDAO.selectByTestIdAndNumber(testId, number);
            answerOptions = answerOptionDAO.selectByQuestionId(question.getId());
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return new QuestionDTO(question, answerOptions);
    }

    @Override
    public int getQuestionQuantityInTest(long testId) {
        try {
            return questionDAO.selectByTestId(testId).size();
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<QuestionDTO> getByTestId(long testId) {
        List<QuestionDTO> questionDTOList = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Question> questions = questionDAO.selectByTestId(testId);
            for (Question question : questions) {
                List<AnswerOption> answerOptions = answerOptionDAO.selectByQuestionId(question.getId());
                QuestionDTO questionDTO = new QuestionDTO(question, answerOptions);
                questionDTOList.add(questionDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return questionDTOList;
    }
}
