package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetUsersListCommand implements Command {
    private UserAccountService service;

    public GetUsersListCommand(UserAccountService service) {
        this.service = service;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        List<UserAccount> userAccounts = service.getAll();
        request.setAttribute("userAccounts", userAccounts);
        return "userList";
    }
}
