package by.training.kas.command;

import by.training.kas.entity.Question;
import by.training.kas.entity.QuestionType;
import by.training.kas.service.QuestionService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static by.training.kas.application.ApplicationConstants.VALIDATION_QUESTION_ERRORS_TYPE;

public class PostEditQuestionCommand implements Command {
    private QuestionService questionService;
    private RequestDataValidator requestDataValidator;

    public PostEditQuestionCommand(QuestionService questionService, RequestDataValidator requestDataValidator) {
        this.questionService = questionService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            Question question = buildQuestion(request);
            questionService.update(question);
            String testIdStr = request.getParameter("testId");
            Map<String, String> redirectParams = new HashMap<>();
            redirectParams.put("testId", testIdStr);
            return RedirectParamStringBuilder.build(CommandType.VIEW_QUESTIONS, redirectParams);
        } else {
            request.setAttribute(VALIDATION_QUESTION_ERRORS_TYPE, validationResult.getErrorsByType(VALIDATION_QUESTION_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_EDIT_QUESTION);
            return command.process(request, response);
        }
    }

    private Question buildQuestion(HttpServletRequest request) {
        String idStr = request.getParameter("questionId");
        long id = Long.parseLong(idStr);
        String questionText = request.getParameter("questionText");
        String questionTypeName = request.getParameter("questionType");
        QuestionType questionType = QuestionType.fromString(questionTypeName)
                .orElseThrow(() -> new CommandException("Unknown question type: " + questionTypeName));
        return new Question(id, questionText, questionType, -1);
    }
}
