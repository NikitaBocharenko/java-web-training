package by.training.kas.dao;

import java.sql.Connection;

public class ConnectionManagerImpl implements ConnectionManager {
    private ConnectionPool connectionPool;
    private TransactionManager transactionManager;

    public ConnectionManagerImpl(ConnectionPool connectionPool, TransactionManager transactionManager) {
        this.connectionPool = connectionPool;
        this.transactionManager = transactionManager;
    }

    @Override
    public Connection getConnection() {
        Connection connection = transactionManager.getConnection();
        return connection != null ? connection : connectionPool.getConnection();
    }
}
