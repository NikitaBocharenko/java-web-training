package by.training.kas.validator;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

import static by.training.kas.application.ApplicationConstants.VALIDATION_STUDENT_GROUP_ERRORS_TYPE;

public class StudentGroupValidator implements RequestDataValidator {
    private static final String GROUP_NAME_REGEX = "[\\p{L}\\w \\p{Punct}]{1,150}";
    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();
        String groupName = request.getParameter("groupName");
        if (groupName == null) {
            validationResult.addError(VALIDATION_STUDENT_GROUP_ERRORS_TYPE, "group-name.null");
        } else if (groupName.isEmpty()) {
            validationResult.addError(VALIDATION_STUDENT_GROUP_ERRORS_TYPE, "group-name.empty");
        } else if (!Pattern.matches(GROUP_NAME_REGEX, groupName)) {
            validationResult.addError(VALIDATION_STUDENT_GROUP_ERRORS_TYPE, "group-name.not-matches");
        }
        return validationResult;
    }
}
