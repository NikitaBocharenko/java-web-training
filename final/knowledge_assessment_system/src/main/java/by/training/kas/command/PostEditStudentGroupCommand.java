package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.entity.StudentGroup;
import by.training.kas.service.StudentGroupService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class PostEditStudentGroupCommand implements Command {
    private StudentGroupService studentGroupService;
    private RequestDataValidator requestDataValidator;

    public PostEditStudentGroupCommand(StudentGroupService studentGroupService, RequestDataValidator requestDataValidator) {
        this.studentGroupService = studentGroupService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            StudentGroup studentGroup = buildStudentGroup(request);
            studentGroupService.edit(studentGroup);
            return RedirectParamStringBuilder.build(CommandType.VIEW_GROUPS, new HashMap<>());
        } else {
            request.setAttribute(ApplicationConstants.VALIDATION_STUDENT_GROUP_ERRORS_TYPE,
                    validationResult.getErrorsByType(ApplicationConstants.VALIDATION_STUDENT_GROUP_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_EDIT_STUDENT_GROUP);
            return command.process(request, response);
        }
    }

    private StudentGroup buildStudentGroup(HttpServletRequest request) {
        String idStr = request.getParameter("studentGroupId");
        long id = Long.parseLong(idStr);
        String groupName = request.getParameter("groupName");
        return new StudentGroup(id, groupName, -1);
    }
}
