package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.TestDAO;
import by.training.kas.dao.TransactionManager;
import by.training.kas.dao.UserAccountDAO;
import by.training.kas.dto.TestDTO;
import by.training.kas.entity.Test;
import by.training.kas.entity.UserAccount;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class TestServiceImpl implements TestService {
    private TestDAO testDAO;
    private UserAccountDAO userAccountDAO;
    private TransactionManager transactionManager;

    public TestServiceImpl(TestDAO testDAO, UserAccountDAO userAccountDAO, TransactionManager transactionManager) {
        this.testDAO = testDAO;
        this.userAccountDAO = userAccountDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<TestDTO> getAll() {
        List<TestDTO> testDTOs = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectAll();
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOs.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOs;
    }

    @Override
    public TestDTO getById(long id) {
        Test test;
        UserAccount creatorAccount;
        transactionManager.beginTransaction();
        try {
            test = testDAO.select(id);
            creatorAccount = userAccountDAO.select(test.getCreatorId());
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return new TestDTO(test, creatorAccount);
    }

    @Override
    public void update(Test test) {
        try {
            testDAO.update(test);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void add(Test test) {
        try {
            testDAO.insert(test);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void delete(long id) {
        try {
            testDAO.delete(id);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<TestDTO> getAllAssignedToGroup(long studentGroupId) {
        List<TestDTO> testDTOs = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectAssignedToGroup(studentGroupId);
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOs.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOs;
    }

    @Override
    public List<TestDTO> getAllNotInGroup(long studentGroupId) {
        List<TestDTO> testDTOS = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectAllNotAssignedToGroup(studentGroupId);
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOS.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOS;
    }

    @Override
    public List<TestDTO> getAllByCreator(long creatorId) {
        List<TestDTO> testDTOs = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectByCreatorId(creatorId);
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOs.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOs;
    }

    @Override
    public List<TestDTO> getAllNotInGroupByCreator(long studentGroupId, long creatorId) {
        List<TestDTO> testDTOS = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectAllNotAssignedToGroupByCreatorId(studentGroupId, creatorId);
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOS.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOS;
    }

    @Override
    public List<TestDTO> getAvailableForPassing(long studentGroupId, long studentId) {
        List<TestDTO> testDTOs = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<Test> tests = testDAO.selectAvailableForPassing(studentGroupId, studentId);
            for (Test test : tests) {
                UserAccount creatorAccount = userAccountDAO.select(test.getCreatorId());
                TestDTO testDTO = new TestDTO(test, creatorAccount);
                testDTOs.add(testDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return testDTOs;
    }
}
