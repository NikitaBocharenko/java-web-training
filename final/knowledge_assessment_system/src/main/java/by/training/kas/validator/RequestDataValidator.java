package by.training.kas.validator;

import javax.servlet.http.HttpServletRequest;

public interface RequestDataValidator {
    ValidationResult validate(HttpServletRequest request);
}
