package by.training.kas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.training.kas.entity.Test;
import org.apache.log4j.Logger;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT;

public class TestDAOImpl implements TestDAO {
    private static final String SELECT_ALL = "select id, test_name, description, creation_time, deadline_time, creator_id from test ";
    private static final String SELECT_BY_ID = SELECT_ALL + "where id = ?";
    private static final String SELECT_BY_STUDENT_GROUP_ID = SELECT_ALL + "join test_has_student_group on id = test_id " +
                                                                            "where student_group_id = ?";
    private static final String SELECT_AVAILABLE_FOR_PASSING = SELECT_BY_STUDENT_GROUP_ID + " and deadline_time > now() " +
                                                                "and id not in (select test_id from test_result where user_account_id = ?)";
    private static final String SELECT_BY_CREATOR_ID = SELECT_ALL + " where creator_id = ?";
    private static final String SELECT_ALL_NOT_IN_STUDENT_GROUP = SELECT_ALL + "where id not in " +
                                                                    "(select test_id from test_has_student_group where student_group_id = ?)";
    private static final String SELECT_ALL_NOT_IN_STUDENT_GROUP_BY_CREATOR = SELECT_ALL_NOT_IN_STUDENT_GROUP + " and creator_id = ?";
    private static final String UPDATE = "update test set test_name = ?, description = ?, deadline_time = ? " +
                                            "where id = ?";
    private static final String INSERT = "insert into test (test_name, description, creation_time, deadline_time, creator_id) " +
                                            "values (?, ?, ?, ?, ?)";
    private static final String DELETE = "delete from test where id = ?";

    private static final Logger log = Logger.getLogger(TestDAOImpl.class);

    private ConnectionManager connectionManager;

    public TestDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    private Test parse(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String testName = resultSet.getString("test_name");
        String description = resultSet.getString("description");
        Timestamp creationTimestamp = resultSet.getTimestamp("creation_time");
        Date creationTime = new Date(creationTimestamp.getTime());
        Timestamp deadlineTimestamp = resultSet.getTimestamp("deadline_time");
        Date deadlineTime = new Date(deadlineTimestamp.getTime());
        long creatorId = resultSet.getLong("creator_id");
        return new Test(id, testName, description, creationTime, deadlineTime, creatorId);
    }

    @Override
    public long insert(Test entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement(INSERT)) {
            int i = 0;
            statement.setString(++i, entity.getTestName());
            statement.setString(++i, entity.getDescription());
            Timestamp creationTimestamp = new Timestamp(entity.getCreationTime().getTime());
            statement.setTimestamp(++i, creationTimestamp);
            Timestamp deadlineTimestamp = new Timestamp(entity.getDeadlineTime().getTime());
            statement.setTimestamp(++i, deadlineTimestamp);
            statement.setLong(++i, entity.getCreatorId());
            log.debug(statement);
            return statement.executeUpdate();
        }
    }

    @Override
    public Test select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)){
            int i = 0;
            statement.setLong(++i, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + TestDAOImpl.class.getSimpleName() + ", " + id);
                }
                return parse(resultSet);
            }
        }
    }

    @Override
    public List<Test> selectAll() throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Test test = parse(resultSet);
                result.add(test);
            }
        }
        return result;
    }

    @Override
    public void update(Test entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            int i = 0;
            statement.setString(++i, entity.getTestName());
            statement.setString(++i, entity.getDescription());
            Timestamp deadlineTimeTimestamp = new Timestamp(entity.getDeadlineTime().getTime());
            log.debug(deadlineTimeTimestamp);
            statement.setTimestamp(++i, deadlineTimeTimestamp);
            statement.setLong(++i, entity.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE)) {
            int i = 0;
            statement.setLong(++i, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<Test> selectAssignedToGroup(long studentGroupId) throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_STUDENT_GROUP_ID)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Test test = parse(resultSet);
                    result.add(test);
                }
            }
        }
        return result;
    }

    @Override
    public List<Test> selectAllNotAssignedToGroup(long studentGroupId) throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_NOT_IN_STUDENT_GROUP)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Test test = parse(resultSet);
                    result.add(test);
                }
            }
        }
        return result;
    }

    @Override
    public List<Test> selectByCreatorId(long creatorId) throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_CREATOR_ID)) {
            int i = 0;
            statement.setLong(++i, creatorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Test test = parse(resultSet);
                    result.add(test);
                }
            }
        }
        return result;
    }

    @Override
    public List<Test> selectAllNotAssignedToGroupByCreatorId(long studentGroupId, long creatorId) throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL_NOT_IN_STUDENT_GROUP_BY_CREATOR)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
            statement.setLong(++i, creatorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Test test = parse(resultSet);
                    result.add(test);
                }
            }
        }
        return result;
    }

    @Override
    public List<Test> selectAvailableForPassing(long studentGroupId, long studentId) throws SQLException {
        List<Test> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_AVAILABLE_FOR_PASSING)) {
            int i = 0;
            statement.setLong(++i, studentGroupId);
            statement.setLong(++i, studentId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Test test = parse(resultSet);
                    result.add(test);
                }
            }
        }
        return result;
    }
}
