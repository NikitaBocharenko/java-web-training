package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.entity.StudentGroup;
import by.training.kas.service.StudentGroupService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class PostAddStudentGroupCommand implements Command {
    private StudentGroupService studentGroupService;
    private RequestDataValidator requestDataValidator;

    public PostAddStudentGroupCommand(StudentGroupService studentGroupService, RequestDataValidator requestDataValidator) {
        this.studentGroupService = studentGroupService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            StudentGroup studentGroup = buildStudentGroup(request);
            studentGroupService.add(studentGroup);
            return RedirectParamStringBuilder.build(CommandType.VIEW_GROUPS, new HashMap<>());
        } else {
            request.setAttribute("groupName", request.getParameter("groupName"));
            request.setAttribute(ApplicationConstants.VALIDATION_STUDENT_GROUP_ERRORS_TYPE,
                    validationResult.getErrorsByType(ApplicationConstants.VALIDATION_STUDENT_GROUP_ERRORS_TYPE));
            return "addStudentGroup";
        }

    }

    private StudentGroup buildStudentGroup(HttpServletRequest request) {
        String groupName = request.getParameter("groupName");
        String creatorIdStr = request.getParameter("creatorId");
        long creatorId = Long.parseLong(creatorIdStr);
        return new StudentGroup(-1, groupName, creatorId);
    }
}
