package by.training.kas.command;

import by.training.kas.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class DeleteTestCommand implements Command {
    private TestService testService;

    public DeleteTestCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        testService.delete(testId);
        return RedirectParamStringBuilder.build(CommandType.VIEW_TESTS, new HashMap<>());
    }
}
