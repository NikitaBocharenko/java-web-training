package by.training.kas.dao;

import by.training.kas.entity.AnswerOption;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT;
import static by.training.kas.application.ApplicationConstants.EXCEPTION_METHOD_UNSUPPORTED_TEXT;

public class AnswerOptionDAOImpl implements AnswerOptionDAO {
    private ConnectionManager connectionManager;

    private static final String SELECT_BY_QUESTION_ID = "select id, option_text, is_correct, question_id from answer_option where question_id = ?";
    private static final String SELECT_BY_ID = "select id, option_text, is_correct, question_id from answer_option where id = ?";
    private static final String INSERT = "insert into answer_option (option_text, is_correct, question_id) values (?, ?, ?)";
    private static final String UPDATE = "update answer_option set option_text = ?, is_correct = ? where id = ?";
    private static final String DELETE = "delete from answer_option where id = ?";

    private static final Logger log = Logger.getLogger(AnswerOptionDAOImpl.class);

    public AnswerOptionDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    private AnswerOption parse(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String optionText = resultSet.getString("option_text");
        int isCorrectInt = resultSet.getInt("is_correct");
        boolean isCorrect = isCorrectInt == 1;
        long questionId = resultSet.getLong("question_id");
        return new AnswerOption(id, optionText, isCorrect, questionId);
    }

    @Override
    public List<AnswerOption> selectByQuestionId(long questionId) throws SQLException {
        List<AnswerOption> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_QUESTION_ID)) {
            int i = 0;
            statement.setLong(++i, questionId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    AnswerOption answerOption = parse(resultSet);
                    result.add(answerOption);
                }
            }
        }
        return result;
    }

    @Override
    public long insert(AnswerOption entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)) {
            int i = 0;
            statement.setString(++i, entity.getOptionText());
            int isCorrectInt = entity.getIsCorrect() ? 1 : 0;
            statement.setInt(++i, isCorrectInt);
            statement.setLong(++i, entity.getQuestionId());
            log.debug(statement);
            return statement.executeUpdate();
        }
    }

    @Override
    public AnswerOption select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            statement.setLong(++i, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return parse(resultSet);
                } else {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + AnswerOptionDAOImpl.class.getSimpleName() + ", " + id);
                }
            }
        }
    }

    @Override
    public List<AnswerOption> selectAll() throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                AnswerOptionDAOImpl.class.getSimpleName() + ", method: List<AnswerOption> selectAll()");
    }

    @Override
    public void update(AnswerOption entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            int i = 0;
            statement.setString(++i, entity.getOptionText());
            int isCorrectInt = entity.getIsCorrect() ? 1 : 0;
            statement.setInt(++i, isCorrectInt);
            statement.setLong(++i, entity.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE)) {
            int i = 0;
            statement.setLong(++i, id);
            statement.executeUpdate();
        }
    }
}
