package by.training.kas.dao;

import by.training.kas.entity.TestResult;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.*;

public class TestResultDAOImpl implements TestResultDAO {
    private ConnectionManager connectionManager;

    private static final String SELECT_DEFAULT = "select id, result, completion_time, user_account_id, test_id from test_result ";
    private static final String SELECT_BY_ID = SELECT_DEFAULT + "where id = ?";
    private static final String SELECT_BY_TEST_ID = SELECT_DEFAULT + "where test_id = ? order by result desc";
    private static final String INSERT = "insert into test_result (result, completion_time, user_account_id, test_id) values (?, ?, ?, ?)";

    private static final Logger log = Logger.getLogger(TestResultDAOImpl.class);

    public TestResultDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public long insert(TestResult entity) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            statement.setInt(++i, entity.getResult());
            Date completionTime = entity.getCompletionTime();
            log.debug("completionTime: " + completionTime);
            Timestamp completionTimestamp = new Timestamp(completionTime.getTime());
            log.debug("completionTimestamp: " + completionTimestamp);
            statement.setTimestamp(++i, completionTimestamp);
            statement.setLong(++i, entity.getUserAccountId());
            statement.setLong(++i, entity.getTestId());
            log.debug("statement: " + statement);
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getLong(1);
                } else {
                    throw new DAOException(EXCEPTION_KEY_NOT_GENERATED_TEXT + " in class " + TestResultDAOImpl.class.getSimpleName());
                }
            }
        }
    }

    @Override
    public TestResult select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            statement.setLong(++i, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return parseResultSet(resultSet);
                } else {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + TestResultDAOImpl.class.getSimpleName() + ", " + id);
                }
            }
        }
    }

    @Override
    public List<TestResult> selectAll() throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                TestResultDAOImpl.class.getSimpleName() + ", method: List<TestResult> selectAll()");
    }

    @Override
    public void update(TestResult entity) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                TestResultDAOImpl.class.getSimpleName() + ", method: void update(TestResult entity)");
    }

    @Override
    public void delete(long id) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                TestResultDAOImpl.class.getSimpleName() + ", method: void delete(long id)");
    }

    @Override
    public List<TestResult> selectByTestId(long testId) throws SQLException {
        List<TestResult> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_TEST_ID)) {
            int i = 0;
            statement.setLong(++i, testId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    TestResult testResult = parseResultSet(resultSet);
                    result.add(testResult);
                }
            }
        }
        return result;
    }

    private TestResult parseResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        int result = resultSet.getInt("result");
        Timestamp completionTimestamp = resultSet.getTimestamp("completion_time");
        Date completionTime = new Date(completionTimestamp.getTime());
        long userAccountId = resultSet.getLong("user_account_id");
        long testId = resultSet.getLong("test_id");
        return new TestResult(id, result, completionTime, userAccountId, testId);
    }
}
