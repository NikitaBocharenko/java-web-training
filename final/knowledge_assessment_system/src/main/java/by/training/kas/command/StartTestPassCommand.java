package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.service.QuestionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class StartTestPassCommand implements Command {
    private QuestionService questionService;

    public StartTestPassCommand(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String passingTestIdStr = request.getParameter("passingTestId");
        long passingTestId = Long.parseLong(passingTestIdStr);
        HttpSession session = request.getSession();
        session.setAttribute("passingTestId", passingTestId);
        int currentQuestionNumber = 0;
        session.setAttribute("currentQuestionNumber", currentQuestionNumber);
        int questionQuantity = questionService.getQuestionQuantityInTest(passingTestId);
        session.setAttribute("questionQuantity", questionQuantity);
        List<Integer> questionNumberList = new ArrayList<>();
        for (int i = 1; i <= questionQuantity; i++) {
            questionNumberList.add(i);
        }
        session.setAttribute("questionNumberList", questionNumberList);
        QuestionDTO question = questionService.getByTestIdAndNumber(passingTestId, currentQuestionNumber);
        request.setAttribute("question", question);
        Map<Long, Set<Long>> answers = new HashMap<>();
        session.setAttribute("answers", answers);
        return "answerQuestion";
    }
}
