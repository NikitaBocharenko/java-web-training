package by.training.kas.entity;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Objects;

public class Test extends Entity {
    private String testName;
    private String description;
    private Date creationTime;
    private Date deadlineTime;
    private long creatorId;

    private static final Logger log = Logger.getLogger(Test.class);

    public Test(long id, String testName, String description, Date creationTime, Date deadlineTime, long creatorId) {
        this.id = id;
        this.testName = testName;
        this.description = description;
        this.creationTime = creationTime;
        this.deadlineTime = deadlineTime;
        this.creatorId = creatorId;
    }

    public String getTestName() {
        return testName;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getDeadlineTime() {
        return deadlineTime;
    }

    public long getCreatorId() {
        return creatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return creatorId == test.creatorId &&
                Objects.equals(testName, test.testName) &&
                Objects.equals(description, test.description) &&
                Objects.equals(creationTime, test.creationTime) &&
                Objects.equals(deadlineTime, test.deadlineTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testName, description, creationTime, deadlineTime, creatorId);
    }
}
