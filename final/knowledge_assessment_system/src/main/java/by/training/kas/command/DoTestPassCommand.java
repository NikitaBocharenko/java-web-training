package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.service.QuestionService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.kas.application.ApplicationConstants.VALIDATION_TEST_TAKING_ERRORS_TYPE;

public class DoTestPassCommand implements Command {
    private QuestionService questionService;
    private RequestDataValidator requestDataValidator;

    private static final Logger log = Logger.getLogger(DoTestPassCommand.class);

    public DoTestPassCommand(QuestionService questionService, RequestDataValidator requestDataValidator) {
        this.questionService = questionService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        HttpSession session = request.getSession();
        int currentQuestionNumber = (int) session.getAttribute("currentQuestionNumber");
        if (validationResult.isValid()) {
            setAnswer(request);
            int questionQuantity = (int) session.getAttribute("questionQuantity");
            if (questionQuantity == ++currentQuestionNumber) {
                Command command = CommandProvider.getByType(CommandType.FINISH_TEST_PASSING);
                return command.process(request, response);
            }
            session.setAttribute("currentQuestionNumber", currentQuestionNumber);
        } else {
            request.setAttribute(VALIDATION_TEST_TAKING_ERRORS_TYPE, validationResult.getErrorsByType(VALIDATION_TEST_TAKING_ERRORS_TYPE));
        }
        Long passingTestId = (Long) session.getAttribute("passingTestId");
        QuestionDTO question = questionService.getByTestIdAndNumber(passingTestId, currentQuestionNumber);
        request.setAttribute("question", question);
        return "answerQuestion";
    }

    private void setAnswer(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Set<Long> selectedOptions;
        String singleOptionStr = request.getParameter("options");
        if (singleOptionStr != null) {
            Long singleOption = Long.valueOf(singleOptionStr);
            selectedOptions = new HashSet<>();
            selectedOptions.add(singleOption);
        } else {
            Set<String> paramNames = request.getParameterMap().keySet();
            selectedOptions = paramNames.stream()
                    .filter(paramName -> paramName.startsWith("option"))
                    .map(paramName -> paramName.replace("option", ""))
                    .map(Long::parseLong)
                    .collect(Collectors.toSet());
        }
        String questionIdStr = request.getParameter("questionId");
        long questionId = Long.parseLong(questionIdStr);
        Map<Long, Set<Long>> answers = (Map<Long, Set<Long>>) session.getAttribute("answers");
        answers.put(questionId, selectedOptions);
        log.debug(answers);
    }
}
