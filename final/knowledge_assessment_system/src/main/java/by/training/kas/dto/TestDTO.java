package by.training.kas.dto;

import by.training.kas.entity.Test;
import by.training.kas.entity.UserAccount;

import java.util.Date;

public class TestDTO {
    private long id;
    private String testName;
    private String description;
    private Date creationTime;
    private Date deadlineTime;
    private UserAccount creator;

    public TestDTO(Test test, UserAccount creator) {
        this.id = test.getId();
        this.testName = test.getTestName();
        this.description = test.getDescription();
        this.creationTime = test.getCreationTime();
        this.deadlineTime = test.getDeadlineTime();
        this.creator = creator;
    }

    public long getId() {
        return id;
    }

    public String getTestName() {
        return testName;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getDeadlineTime() {
        return deadlineTime;
    }

    public UserAccount getCreator() {
        return creator;
    }
}
