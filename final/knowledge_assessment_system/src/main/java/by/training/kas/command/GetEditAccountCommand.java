package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.service.UserRoleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetEditAccountCommand implements Command {
    private UserAccountService userAccountService;
    private UserRoleService userRoleService;

    public GetEditAccountCommand(UserAccountService userAccountService, UserRoleService userRoleService) {
        this.userAccountService = userAccountService;
        this.userRoleService = userRoleService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String userAccountIdStr = request.getParameter("id");
        long userAccountId = Long.parseLong(userAccountIdStr);
        UserAccount userAccount = userAccountService.getById(userAccountId);
        request.setAttribute("userAccount", userAccount);
        List<UserRole> possibleRoles = userRoleService.getAll();
        request.setAttribute("possibleRoles", possibleRoles);
        return "editAccount";
    }
}
