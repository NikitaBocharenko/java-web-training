package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.TransactionManager;
import by.training.kas.dao.UserAccountDAO;
import by.training.kas.dao.UserRoleDAO;
import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class UserAccountServiceImpl implements UserAccountService {
    private UserAccountDAO userAccountDAO;
    private UserRoleDAO userRoleDAO;
    private TransactionManager transactionManager;

    public UserAccountServiceImpl(UserAccountDAO userAccountDAO, UserRoleDAO userRoleDAO, TransactionManager transactionManager) {
        this.userAccountDAO = userAccountDAO;
        this.userRoleDAO = userRoleDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public Optional<UserAccount> getByCredentials(String login, String password) {
        Optional<UserAccount> userAccountOptional;
        transactionManager.beginTransaction();
        try {
            userAccountOptional = userAccountDAO.selectByLoginPassword(login, password);
            if (userAccountOptional.isPresent()) {
                UserAccount userAccount = userAccountOptional.get();
                Set<UserRole> userRoles = userRoleDAO.selectRolesForAccount(userAccount.getId());
                userAccount.setUserRoles(userRoles);
            }
        } catch (SQLException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return userAccountOptional;
    }

    @Override
    public void add(UserAccount userAccount) {
        transactionManager.beginTransaction();
        try {
            long userId = userAccountDAO.insert(userAccount);
            for (UserRole role : userAccount.getUserRoles()) {
                userRoleDAO.assignRole(userId, role.getId());
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
    }

    @Override
    public boolean isLoginAlreadyExists(String login) {
        try {
            return userAccountDAO.isLoginAlreadyExists(login);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<UserAccount> getAll() {
        List<UserAccount> userAccounts;
        transactionManager.beginTransaction();
        try {
            userAccounts = userAccountDAO.selectAll();
            for (UserAccount userAccount : userAccounts) {
                Set<UserRole> userRoles = userRoleDAO.selectRolesForAccount(userAccount.getId());
                userAccount.setUserRoles(userRoles);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return userAccounts;
    }

    @Override
    public UserAccount getById(long id) {
        UserAccount userAccount;
        transactionManager.beginTransaction();
        try {
            userAccount = userAccountDAO.select(id);
            Set<UserRole> userRoles = userRoleDAO.selectRolesForAccount(userAccount.getId());
            userAccount.setUserRoles(userRoles);
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return userAccount;
    }

    @Override
    public void set(UserAccount userAccount) {
        transactionManager.beginTransaction();
        try {
            userAccountDAO.update(userAccount);
            Set<UserRole> currentRoles = userRoleDAO.selectRolesForAccount(userAccount.getId());
            if (!currentRoles.equals(userAccount.getUserRoles())) {
                userRoleDAO.deleteRolesForAccount(userAccount.getId());
                for (UserRole role : userAccount.getUserRoles()) {
                    userRoleDAO.assignRole(userAccount.getId(), role.getId());
                }
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
    }

    @Override
    public void delete(long id) {
        transactionManager.beginTransaction();
        try {
            userRoleDAO.deleteRolesForAccount(id);
            userAccountDAO.delete(id);
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
    }

    @Override
    public List<UserAccount> getStudentsInGroup(long studentGroupId) {
        try {
            return userAccountDAO.selectStudentsInGroup(studentGroupId);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<UserAccount> getStudentsNotInGroup(long studentGroupId) {
        try {
            return userAccountDAO.selectStudentsNotInGroup(studentGroupId);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }
}
