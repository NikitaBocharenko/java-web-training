package by.training.kas.entity;

import java.util.Objects;

public class AnswerOption extends Entity {
    private String optionText;
    private boolean isCorrect;
    private long questionId;

    public AnswerOption(long id, String optionText, boolean isCorrect, long questionId) {
        this.id = id;
        this.optionText = optionText;
        this.isCorrect = isCorrect;
        this.questionId = questionId;
    }

    public String getOptionText() {
        return optionText;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public long getQuestionId() {
        return questionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerOption that = (AnswerOption) o;
        return isCorrect == that.isCorrect &&
                questionId == that.questionId &&
                Objects.equals(optionText, that.optionText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(optionText, isCorrect, questionId);
    }
}
