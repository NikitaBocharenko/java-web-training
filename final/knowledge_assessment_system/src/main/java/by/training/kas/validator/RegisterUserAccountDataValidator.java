package by.training.kas.validator;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.service.UserAccountService;
import by.training.kas.service.UserRoleService;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;
import java.util.regex.Pattern;

public class RegisterUserAccountDataValidator implements RequestDataValidator {
    private UserAccountService userAccountService;
    private UserRoleService userRoleService;

    private static final String LOGIN_REGEX = "[A-z]{4,100}";
    private static final String FIRST_NAME_REGEX = "\\p{L}{1,100}";
    private static final String LAST_NAME_REGEX = "\\p{L}{1,100}";
    private static final String PASSWORD_REGEX = "\\w{4,100}";
    private static final String EMAIL_REGEX = "(([\\w\\.-]+)@(\\w+)\\.([A-z]{2,4})){1,100}";

    public RegisterUserAccountDataValidator(UserAccountService userAccountService, UserRoleService userRoleService) {
        this.userAccountService = userAccountService;
        this.userRoleService = userRoleService;
    }

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();

        String login = request.getParameter(ApplicationConstants.USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME);
        checkLogin(login, validationResult);

        String firstName = request.getParameter(ApplicationConstants.USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME);
        checkFirstName(firstName, validationResult);

        String lastName = request.getParameter(ApplicationConstants.USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME);
        checkLastName(lastName, validationResult);

        String password = request.getParameter(ApplicationConstants.USER_ACCOUNT_PASSWORD_ATTRIBUTE_NAME);
        String passwordRepeat = request.getParameter(ApplicationConstants.USER_ACCOUNT_REQUEST_PASSWORD_REPEAT_PARAMETER_NAME);
        checkPassword(password, passwordRepeat, validationResult);

        String email = request.getParameter(ApplicationConstants.USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME);
        checkEmail(email, validationResult);

        Set<String> paramNames = request.getParameterMap().keySet();
        checkRoles(paramNames, validationResult);

        return validationResult;
    }

    private void checkLogin(String login, ValidationResult validationResult) {
        if (login == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "login.null");
        } else if (login.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "login.empty");
        } else if (!Pattern.matches(LOGIN_REGEX, login)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "login.not-matches");
        } else if (userAccountService.isLoginAlreadyExists(login)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "login.already-exist");
        }
    }

    private void checkFirstName(String firstName, ValidationResult validationResult) {
        if (firstName == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "first-name.null");
        } else if (firstName.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "first-name.empty");
        } else if (!Pattern.matches(FIRST_NAME_REGEX, firstName)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "first-name.not-matches");
        }
    }

    private void checkLastName(String lastName, ValidationResult validationResult) {
        if (lastName == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "last-name.null");
        } else if (lastName.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "last-name.empty");
        } else if (!Pattern.matches(LAST_NAME_REGEX, lastName)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "last-name.not-matches");
        }
    }

    private void checkPassword(String password, String passwordRepeat, ValidationResult validationResult) {
        if (password == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "password.null");
        } else if (password.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "password.empty");
        } else if (!Pattern.matches(PASSWORD_REGEX, password)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "password.not-matches");
        } else if (!password.equals(passwordRepeat)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "password.different");
        }
    }

    private void checkEmail(String email, ValidationResult validationResult) {
        if (email == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "email.null");
        } else if (email.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "email.empty");
        } else if (!Pattern.matches(EMAIL_REGEX, email)) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "email.not-matches");
        }
    }

    private void checkRoles(Set<String> paramNames, ValidationResult validationResult) {
        long selectedRolesQuantity = paramNames.stream()
                .filter(paramName -> paramName.endsWith("Role"))
                .map(paramName -> paramName.replace("Role", ""))
                .map(paramName -> userRoleService.getByName(paramName))
                .count();
        if (selectedRolesQuantity == 0) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE, "roles.no-selected");
        }
    }
}
