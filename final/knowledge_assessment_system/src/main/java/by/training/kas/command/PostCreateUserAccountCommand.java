package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.service.UserRoleService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.kas.application.ApplicationConstants.USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME;
import static by.training.kas.application.ApplicationConstants.USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME;
import static by.training.kas.application.ApplicationConstants.USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME;
import static by.training.kas.application.ApplicationConstants.USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME;
import static by.training.kas.application.ApplicationConstants.USER_ACCOUNT_PASSWORD_ATTRIBUTE_NAME;
import static by.training.kas.application.ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE;

public class PostCreateUserAccountCommand implements Command {
    private UserAccountService userAccountService;
    private UserRoleService userRoleService;
    private RequestDataValidator requestDataValidator;

    public PostCreateUserAccountCommand(UserAccountService userAccountService, UserRoleService userRoleService, RequestDataValidator requestDataValidator) {
        this.userAccountService = userAccountService;
        this.userRoleService = userRoleService;
        this.requestDataValidator = requestDataValidator;
    }

    private static final Logger log = Logger.getLogger(PostCreateUserAccountCommand.class);

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Register command");
        Set<UserRole> selectedRoles = parseAccountRoles(request);
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            UserAccount userAccount = buildUserAccount(request);
            userAccount.setUserRoles(selectedRoles);
            userAccountService.add(userAccount);
            return RedirectParamStringBuilder.build(CommandType.GET_LOGIN, new HashMap<>());
        } else {
            request.setAttribute(USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME,
                    request.getParameter(USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME));
            request.setAttribute(USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME,
                    request.getParameter(USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME));
            request.setAttribute(USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME,
                    request.getParameter(USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME));
            request.setAttribute(USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME,
                    request.getParameter(USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME));
            request.setAttribute("selectedRoles", selectedRoles);
            request.setAttribute(VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE,
                    validationResult.getErrorsByType(ApplicationConstants.VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.PREPARE_REGISTER);
            return command.process(request, response);
        }
    }

    private UserAccount buildUserAccount(HttpServletRequest request) {
        String login = request.getParameter(USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME);
        String password = request.getParameter(USER_ACCOUNT_PASSWORD_ATTRIBUTE_NAME);
        String firstName = request.getParameter(USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME);
        String lastName = request.getParameter(USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME);
        String email = request.getParameter(USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME);
        return new UserAccount(-1, login, password, firstName, lastName, email);
    }

    private Set<UserRole> parseAccountRoles(HttpServletRequest request) {
        Set<String> paramNames = request.getParameterMap().keySet();
        return paramNames.stream()
                .filter(paramName -> paramName.endsWith("Role"))
                .map(paramName -> paramName.replace("Role", ""))
                .map(roleName -> userRoleService.getByName(roleName))
                .collect(Collectors.toSet());
    }
}
