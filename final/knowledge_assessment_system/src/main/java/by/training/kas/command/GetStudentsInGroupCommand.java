package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetStudentsInGroupCommand implements Command {
    private UserAccountService userAccountService;

    public GetStudentsInGroupCommand(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("studentGroupId");
        long id = Long.parseLong(idStr);
        List<UserAccount> students = userAccountService.getStudentsInGroup(id);
        request.setAttribute("students", students);
        request.setAttribute("studentGroupId", id);
        return "groupStudents";
    }
}
