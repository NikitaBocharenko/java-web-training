package by.training.kas.command;

import by.training.kas.entity.UserAccount;
import by.training.kas.entity.UserRole;
import by.training.kas.service.UserAccountService;
import by.training.kas.service.UserRoleService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.kas.application.ApplicationConstants.*;

public class PostEditAccountCommand implements Command {
    private UserAccountService userAccountService;
    private UserRoleService userRoleService;
    private RequestDataValidator requestDataValidator;

    public PostEditAccountCommand(UserAccountService userAccountService, UserRoleService userRoleService, RequestDataValidator requestDataValidator) {
        this.userAccountService = userAccountService;
        this.userRoleService = userRoleService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = this.requestDataValidator.validate(request);
        Command command;
        if (validationResult.isValid()) {
            UserAccount userAccount = buildUserAccount(request);
            Set<UserRole> userRoles = this.parseAccountRoles(request);
            userAccount.setUserRoles(userRoles);
            userAccountService.set(userAccount);
            return RedirectParamStringBuilder.build(CommandType.VIEW_USERS, new HashMap<>());
        } else {
            request.setAttribute(VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE,
                    validationResult.getErrorsByType(VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE));
            command = CommandProvider.getByType(CommandType.GET_EDIT_ACCOUNT);
            return command.process(request, response);
        }
    }

    private UserAccount buildUserAccount(HttpServletRequest request) {
        String idStr = request.getParameter(USER_ACCOUNT_ID_ATTRIBUTE_NAME);
        long id = Long.parseLong(idStr);
        String login = request.getParameter(USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME);
        String password = request.getParameter(USER_ACCOUNT_PASSWORD_ATTRIBUTE_NAME);
        String firstName = request.getParameter(USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME);
        String lastName = request.getParameter(USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME);
        String email = request.getParameter(USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME);
        return new UserAccount(id, login, password, firstName, lastName, email);
    }

    private Set<UserRole> parseAccountRoles(HttpServletRequest request) {
        Set<String> paramNames = request.getParameterMap().keySet();
        return paramNames.stream()
                .filter(paramName -> paramName.endsWith("Role"))
                .map(paramName -> paramName.replace("Role", ""))
                .map(roleName -> userRoleService.getByName(roleName))
                .collect(Collectors.toSet());
    }
}
