package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.dto.TestResultDTO;
import by.training.kas.entity.UserAccount;
import by.training.kas.service.QuestionService;
import by.training.kas.service.TestResultService;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ViewTestResultCommand implements Command {
    private TestResultService testResultService;
    private QuestionService questionService;
    private UserAccountService userAccountService;

    public ViewTestResultCommand(TestResultService testResultService, QuestionService questionService, UserAccountService userAccountService) {
        this.testResultService = testResultService;
        this.questionService = questionService;
        this.userAccountService = userAccountService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String resultIdStr = request.getParameter("resultId");
        long resultId = Long.parseLong(resultIdStr);
        TestResultDTO testResult = testResultService.getById(resultId);
        request.setAttribute("testResult", testResult);
        List<QuestionDTO> questions = questionService.getByTestId(testResult.getTestId());
        request.setAttribute("questions", questions);
        UserAccount userAccount = userAccountService.getById(testResult.getUserAccountId());
        request.setAttribute("student", userAccount);
        return "testResult";
    }
}
