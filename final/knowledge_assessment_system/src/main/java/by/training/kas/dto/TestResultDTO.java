package by.training.kas.dto;

import by.training.kas.entity.TestResult;
import by.training.kas.entity.UserAccount;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class TestResultDTO {
    private long id;
    private int result;
    private Date completionTime;
    private long testId;
    private long userAccountId;
    private UserAccount userAccount;
    private Map<Long, Set<Long>> answers;
    private Set<Long> correctAnsweredQuestions;

    public TestResultDTO(TestResult testResult, UserAccount userAccount) {
        this.id = testResult.getId();
        this.result = testResult.getResult();
        this.completionTime = testResult.getCompletionTime();
        this.testId = testResult.getTestId();
        this.userAccount = userAccount;
    }

    public TestResultDTO(TestResult testResult, Map<Long, Set<Long>> answers, Set<Long> correctAnsweredQuestions) {
        this.id = testResult.getId();
        this.result = testResult.getResult();
        this.completionTime = testResult.getCompletionTime();
        this.testId = testResult.getTestId();
        this.userAccountId = testResult.getUserAccountId();
        this.answers = answers;
        this.correctAnsweredQuestions = correctAnsweredQuestions;
    }

    public long getId() {
        return id;
    }

    public int getResult() {
        return result;
    }

    public Date getCompletionTime() {
        return completionTime;
    }

    public long getTestId() {
        return testId;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public Map<Long, Set<Long>> getAnswers() {
        return answers;
    }

    public Set<Long> getCorrectAnsweredQuestions() {
        return correctAnsweredQuestions;
    }

    public long getUserAccountId() {
        return userAccountId;
    }
}
