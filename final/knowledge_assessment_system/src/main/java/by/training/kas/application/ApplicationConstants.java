package by.training.kas.application;

public class ApplicationConstants {
    private ApplicationConstants(){}
    public static final String USER_ACCOUNT_ID_ATTRIBUTE_NAME = "id";
    public static final String USER_ACCOUNT_LOGIN_ATTRIBUTE_NAME = "login";
    public static final String USER_ACCOUNT_PASSWORD_ATTRIBUTE_NAME = "password";
    public static final String USER_ACCOUNT_REQUEST_PASSWORD_REPEAT_PARAMETER_NAME = "passwordRepeat";
    public static final String USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME = "firstName";
    public static final String USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME = "lastName";
    public static final String USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME = "email";

    public static final String VALIDATION_LOGIN_ERRORS_TYPE = "loginErrors";
    public static final String VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE = "editUserAccountErrors";
    public static final String VALIDATION_USER_ACCOUNT_REGISTER_ERRORS_TYPE = "registerUserAccountErrors";
    public static final String VALIDATION_STUDENT_GROUP_ERRORS_TYPE = "studentGroupErrors";
    public static final String VALIDATION_TEST_ERRORS_TYPE = "testErrors";
    public static final String VALIDATION_QUESTION_ERRORS_TYPE = "questionErrors";
    public static final String VALIDATION_ANSWER_OPTION_ERRORS_TYPE = "answerOptionErrors";
    public static final String VALIDATION_TEST_TAKING_ERRORS_TYPE = "testTakingErrors";

    public static final String EXCEPTION_DATABASE_KEY = "exception.database";
    public static final String EXCEPTION_METHOD_UNSUPPORTED_TEXT = "Method is unsupported";
    public static final String EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT = "Entity wasn't found by id";
    public static final String EXCEPTION_KEY_NOT_GENERATED_TEXT = "Auto-generated key wasn't return after insert";
}
