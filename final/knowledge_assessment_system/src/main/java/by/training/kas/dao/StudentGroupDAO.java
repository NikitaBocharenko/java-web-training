package by.training.kas.dao;

import by.training.kas.entity.StudentGroup;

import java.sql.SQLException;
import java.util.List;

public interface StudentGroupDAO extends EntityDAO<StudentGroup> {
    void addStudentToGroup(long userAccountId, long studentGroupId) throws SQLException;
    void deleteStudentFromGroup(long userAccountId, long studentGroupId) throws SQLException;
    void assignTestToGroup(long testId, long studentGroupId) throws SQLException;
    void deleteTestFromGroup(long testId, long studentGroupId) throws SQLException;
    List<StudentGroup> selectByCreatorId(long creatorId) throws SQLException;
    List<StudentGroup> selectByMemberId(long memberId) throws SQLException;
}
