package by.training.kas.dao;

import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPoolImpl implements ConnectionPool{
    private Queue<Connection> availableConnections;
    private Queue<Connection> usingConnections;
    private Condition emptyPool;
    private static final Lock singletonLock = new ReentrantLock();
    private final Lock connectionLock;
    private static ConnectionPoolImpl instance;
    private Driver driver;

    private static final Logger log = Logger.getLogger(ConnectionPoolImpl.class);

    private ConnectionPoolImpl() {
        Properties property = new Properties();
        try {
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL resource = classLoader.getResource("database.properties");
            String path = resource.getPath().substring(1);
            FileReader reader = new FileReader(path);
            property.load(reader);
            String dbUrl = property.getProperty("db.url");
            int dbPoolSize = Integer.parseInt(property.getProperty("db.pool_size"));
            String dbUser = property.getProperty("db.user");
            String dbPassword = property.getProperty("db.password");
            String dbDriverClass = property.getProperty("db.driver.class");
            Class driverClass = Class.forName(dbDriverClass);
            this.driver = (Driver) driverClass.newInstance();
            DriverManager.registerDriver(this.driver);
            availableConnections = new LinkedList<>();
            for (int i = 0; i < dbPoolSize; i++) {
                Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
                availableConnections.add(connection);
            }
        } catch (SQLException | IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new ConnectionPoolException("Exception was thrown during creation of connection pool: " + e.getMessage(), e);
        }
        usingConnections = new LinkedList<>();
        connectionLock = new ReentrantLock();
        emptyPool = connectionLock.newCondition();
        log.info("Connection pool created");
    }

    public static ConnectionPoolImpl getInstance() {
        if (instance == null) {
            singletonLock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPoolImpl();
                }
            } finally {
                singletonLock.unlock();
            }
        }
        return instance;
    }

    @Override
    public Connection getConnection() {
        connectionLock.lock();
        try {
            while (availableConnections.isEmpty()) {
                emptyPool.await();
            }
            Connection connection = availableConnections.poll();
            usingConnections.add(connection);
            log.debug("Connection got: " + connection);
            return this.createConnectionProxy(connection);
        } catch (InterruptedException e) {
            log.error("InterruptedException: " + e.getMessage());
            throw new ConnectionPoolException(e.getMessage());
        } finally {
            connectionLock.unlock();
        }
    }

    private Connection createConnectionProxy(Connection realConnection) {
        return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if (method.getName().equals("close")) {
                        this.releaseConnection(realConnection);
                        return null;
                    } else {
                        return method.invoke(realConnection, args);
                    }
                });
    }

    @Override
    public void releaseConnection(Connection connection) {
        connectionLock.lock();
        try {
            if (!usingConnections.contains(connection)) {
                throw new ConnectionPoolException("Releasable connection not from connection pool");
            }
            usingConnections.remove(connection);
            availableConnections.add(connection);
            emptyPool.signal();
        } finally {
            connectionLock.unlock();
        }
        log.debug("Connection released: " + connection);
    }

    @Override
    public void close() {
        try {
            for (Connection connection : availableConnections) {
                connection.close();
            }
            for (Connection connection : usingConnections) {
                connection.close();
            }
            ConnectionPoolImpl.destroyInstance();
            DriverManager.deregisterDriver(this.driver);
        } catch (SQLException e) {
            throw new ConnectionPoolException("SQLException was thrown during destruction of connection pool: " + e.getMessage(), e);
        }
        log.info("Connection pool destroyed");
    }

    private static void destroyInstance() {
        instance = null;
    }
}
