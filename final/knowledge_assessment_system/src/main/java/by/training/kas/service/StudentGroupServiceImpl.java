package by.training.kas.service;

import by.training.kas.dao.DAOException;
import by.training.kas.dao.StudentGroupDAO;
import by.training.kas.dao.TransactionManager;
import by.training.kas.dao.UserAccountDAO;
import by.training.kas.dto.StudentGroupDTO;
import by.training.kas.entity.StudentGroup;
import by.training.kas.entity.UserAccount;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class StudentGroupServiceImpl implements StudentGroupService {
    private StudentGroupDAO studentGroupDAO;
    private UserAccountDAO userAccountDAO;
    private TransactionManager transactionManager;

    public StudentGroupServiceImpl(StudentGroupDAO studentGroupDAO, UserAccountDAO userAccountDAO, TransactionManager transactionManager) {
        this.studentGroupDAO = studentGroupDAO;
        this.userAccountDAO = userAccountDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<StudentGroupDTO> getAll() {
        List<StudentGroupDTO> groupDTOList = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<StudentGroup> groups = studentGroupDAO.selectAll();
            for (StudentGroup studentGroup : groups) {
                UserAccount creatorAccount = userAccountDAO.select(studentGroup.getCreatorId());
                StudentGroupDTO studentGroupDTO = new StudentGroupDTO(studentGroup, creatorAccount);
                groupDTOList.add(studentGroupDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return groupDTOList;
    }

    @Override
    public void add(StudentGroup studentGroup) {
        try {
            studentGroupDAO.insert(studentGroup);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public StudentGroupDTO get(long studentGroupId) {
        StudentGroup studentGroup;
        UserAccount creatorAccount;
        transactionManager.beginTransaction();
        try {
            studentGroup = studentGroupDAO.select(studentGroupId);
            creatorAccount = userAccountDAO.select(studentGroup.getCreatorId());
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return new StudentGroupDTO(studentGroup, creatorAccount);
    }

    @Override
    public void edit(StudentGroup studentGroup) {
        try {
            studentGroupDAO.update(studentGroup);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void delete(long studentGroupId) {
        try {
            studentGroupDAO.delete(studentGroupId);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void addStudentsToGroup(List<Long> studentsId, long studentGroupId) {
        transactionManager.beginTransaction();
        try {
            for (Long studentId : studentsId) {
                studentGroupDAO.addStudentToGroup(studentId, studentGroupId);
            }
        } catch (SQLException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
    }

    @Override
    public void deleteStudentFromGroup(long studentId, long studentGroupId) {
        try {
            studentGroupDAO.deleteStudentFromGroup(studentId, studentGroupId);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void assignTestToGroup(List<Long> testIds, long studentGroupId) {
        transactionManager.beginTransaction();
        try {
            for (Long testId : testIds) {
                studentGroupDAO.assignTestToGroup(testId, studentGroupId);
            }
        } catch (SQLException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
    }

    @Override
    public void deleteTestFromGroup(long testId, long studentGroupId) {
        try {
            studentGroupDAO.deleteTestFromGroup(testId, studentGroupId);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<StudentGroupDTO> getByCreator(long creatorId) {
        List<StudentGroupDTO> groupDTOList = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<StudentGroup> groups = studentGroupDAO.selectByCreatorId(creatorId);
            for (StudentGroup studentGroup : groups) {
                UserAccount creatorAccount = userAccountDAO.select(studentGroup.getCreatorId());
                StudentGroupDTO studentGroupDTO = new StudentGroupDTO(studentGroup, creatorAccount);
                groupDTOList.add(studentGroupDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return groupDTOList;
    }

    @Override
    public List<StudentGroupDTO> getByMember(long memberId) {
        List<StudentGroupDTO> groupDTOList = new ArrayList<>();
        transactionManager.beginTransaction();
        try {
            List<StudentGroup> groups = studentGroupDAO.selectByMemberId(memberId);

            for (StudentGroup studentGroup : groups) {
                UserAccount creatorAccount = userAccountDAO.select(studentGroup.getCreatorId());
                StudentGroupDTO studentGroupDTO = new StudentGroupDTO(studentGroup, creatorAccount);
                groupDTOList.add(studentGroupDTO);
            }
        } catch (SQLException | DAOException e) {
            transactionManager.rollbackTransaction();
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
        transactionManager.commitTransaction();
        return groupDTOList;
    }
}
