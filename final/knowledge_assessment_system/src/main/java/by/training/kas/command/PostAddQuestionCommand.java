package by.training.kas.command;

import by.training.kas.entity.Question;
import by.training.kas.entity.QuestionType;
import by.training.kas.service.QuestionService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static by.training.kas.application.ApplicationConstants.VALIDATION_QUESTION_ERRORS_TYPE;

public class PostAddQuestionCommand implements Command {
    private QuestionService questionService;
    private RequestDataValidator requestDataValidator;

    public PostAddQuestionCommand(QuestionService questionService, RequestDataValidator requestDataValidator) {
        this.questionService = questionService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            Question question = buildQuestion(request);
            questionService.add(question);
            String testIdStr = request.getParameter("testId");
            Map<String, String> redirectParams = new HashMap<>();
            redirectParams.put("testId", testIdStr);
            return RedirectParamStringBuilder.build(CommandType.VIEW_QUESTIONS, redirectParams);
        } else {
            request.setAttribute("questionText", request.getParameter("questionText"));
            request.setAttribute(VALIDATION_QUESTION_ERRORS_TYPE, validationResult.getErrorsByType(VALIDATION_QUESTION_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_ADD_QUESTION);
            return command.process(request, response);
        }
    }

    private Question buildQuestion(HttpServletRequest request) {
        String questionText = request.getParameter("questionText");
        String questionTypeName = request.getParameter("questionType");
        QuestionType questionType = QuestionType.fromString(questionTypeName)
                .orElseThrow(() -> new CommandException("Unknown question type: " + questionTypeName));
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        return new Question(-1, questionText, questionType, testId);
    }
}
