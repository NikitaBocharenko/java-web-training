package by.training.kas.application;

import by.training.kas.command.Command;
import by.training.kas.command.CommandProvider;
import by.training.kas.command.CommandType;
import by.training.kas.dao.TransactionException;
import by.training.kas.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/", name = "app")
public class ApplicationServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(ApplicationServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        Optional<CommandType> commandTypeOptional = CommandType.fromString(request.getParameter("command"));
        Command command = CommandProvider.getByType(commandTypeOptional.orElse(CommandType.GET_LOGIN));
        try {
            String result = executeCommand(command, request, response);
            if (result.startsWith("redirect")) {
                String url = prepareRedirect(request, result);
                response.sendRedirect(url);
            } else {
                RequestDispatcher requestDispatcher = prepareForward(request, result);
                requestDispatcher.forward(request, response);
            }
        } catch (ServletException | IOException e) {
            log.error(e.getMessage());
        }
    }

    private String executeCommand(Command command, HttpServletRequest request, HttpServletResponse response) {
        try {
            return command.process(request, response);
        } catch (ServiceException | TransactionException e) {
            request.setAttribute("exceptionType", e.getMessage());
            request.setAttribute("exceptionMessage", e.getCause().getMessage());
            return "exception";
        } catch (RuntimeException e) {
            request.setAttribute("exceptionType", e.getMessage());
            request.setAttribute("exceptionMessage", e.getMessage());
            log.error(e.getMessage());
            return "exception";
        }
    }

    private String prepareRedirect(HttpServletRequest request, String commandResult) {
        return request.getContextPath() + "/app?" + commandResult.substring(commandResult.indexOf(':') + 1);
    }

    private RequestDispatcher prepareForward(HttpServletRequest request, String commandResult) {
        request.setAttribute("viewName", commandResult);
        if (commandResult.equals("login") || commandResult.equals("registration")) {
            return request.getRequestDispatcher("/jsp/logRegLayout.jsp");
        }
        return request.getRequestDispatcher("/jsp/mainLayout.jsp");
    }
}
