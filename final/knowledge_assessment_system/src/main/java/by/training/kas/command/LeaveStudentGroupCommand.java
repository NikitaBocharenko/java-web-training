package by.training.kas.command;

import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class LeaveStudentGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public LeaveStudentGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        HttpSession session = request.getSession();
        long memberId = (long) session.getAttribute("userId");
        studentGroupService.deleteStudentFromGroup(memberId, studentGroupId);
        return RedirectParamStringBuilder.build(CommandType.VIEW_STUDENT_GROUPS, new HashMap<>());
    }
}
