package by.training.kas.entity;

import java.util.Objects;

public class Question extends Entity {
    private String questionText;
    private QuestionType questionType;
    private long testId;

    public Question(long id, String questionText, QuestionType questionType, long testId) {
        this.id = id;
        this.questionText = questionText;
        this.questionType = questionType;
        this.testId = testId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public long getTestId() {
        return testId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return testId == question.testId &&
                Objects.equals(questionText, question.questionText) &&
                questionType == question.questionType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionText, questionType, testId);
    }
}
