package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.entity.AnswerOption;
import by.training.kas.service.AnswerOptionService;
import by.training.kas.service.QuestionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetAnswerOptionsCommand implements Command {
    private AnswerOptionService answerOptionService;
    private QuestionService questionService;

    public GetAnswerOptionsCommand(AnswerOptionService answerOptionService, QuestionService questionService) {
        this.answerOptionService = answerOptionService;
        this.questionService = questionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String questionIdStr = request.getParameter("questionId");
        long questionId = Long.parseLong(questionIdStr);
        List<AnswerOption> answerOptions = answerOptionService.getByQuestionId(questionId);
        request.setAttribute("answerOptions", answerOptions);
        QuestionDTO questionDTO = questionService.getById(questionId);
        request.setAttribute("testId", questionDTO.getTestId());
        request.setAttribute("questionId", questionIdStr);
        return "answerOptionList";
    }
}
