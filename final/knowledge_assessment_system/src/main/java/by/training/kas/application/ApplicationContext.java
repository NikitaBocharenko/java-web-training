package by.training.kas.application;

import by.training.kas.command.*;
import by.training.kas.dao.*;
import by.training.kas.service.*;
import by.training.kas.validator.*;

public class ApplicationContext {
    private static ConnectionPool connectionPool;

    private ApplicationContext() {
    }

    public static void init() {
        //database essentials
        connectionPool = ConnectionPoolImpl.getInstance();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManagerImpl(connectionPool, transactionManager);

        //dao
        UserAccountDAO userAccountDAO = new UserAccountDAOImpl(connectionManager);
        UserRoleDAO userRoleDAO = new UserRoleDAOImpl(connectionManager);
        TestDAO testDAO = new TestDAOImpl(connectionManager);
        QuestionDAO questionDAO = new QuestionDAOImpl(connectionManager);
        AnswerOptionDAO answerOptionDAO = new AnswerOptionDAOImpl(connectionManager);
        StudentGroupDAO studentGroupDAO = new StudentGroupDAOImpl(connectionManager);
        AnswerDAO answerDAO = new AnswerDAOImpl(connectionManager);
        TestResultDAO testResultDAO = new TestResultDAOImpl(connectionManager);

        //services
        UserAccountService userAccountService = new UserAccountServiceImpl(userAccountDAO, userRoleDAO, transactionManager);
        UserRoleService userRoleService = new UserRoleServiceImpl(userRoleDAO);
        TestService testService = new TestServiceImpl(testDAO, userAccountDAO, transactionManager);
        QuestionService questionService = new QuestionServiceImpl(questionDAO, answerOptionDAO, transactionManager);
        AnswerOptionService answerOptionService = new AnswerOptionServiceImpl(answerOptionDAO);
        StudentGroupService studentGroupService = new StudentGroupServiceImpl(studentGroupDAO, userAccountDAO, transactionManager);
        TestResultService testResultService = new TestResultServiceImpl(testResultDAO, answerDAO, userAccountDAO, answerOptionDAO, transactionManager);

        //validators
        RequestDataValidator loginDataValidator = new LoginDataValidator(userAccountService);
        RequestDataValidator editUserAccountValidator = new EditUserAccountDataValidator();
        RequestDataValidator registerUserAccountDataValidator = new RegisterUserAccountDataValidator(userAccountService, userRoleService);
        RequestDataValidator studentGroupValidator = new StudentGroupValidator();
        RequestDataValidator testValidator = new TestValidator();
        RequestDataValidator questionValidator = new QuestionValidator();
        RequestDataValidator answerOptionValidator = new AnswerOptionValidator();
        RequestDataValidator testTakingValidator = new TestTakingValidator();

        //commands
        Command getLoginCommand = new GetLoginCommand();
        Command loginCommand = new LoginCommand(userAccountService, loginDataValidator);
        Command prepareRegisterCommand = new GetCreateUserAccountCommand(userRoleService);
        Command registerCommand = new PostCreateUserAccountCommand(userAccountService, userRoleService, registerUserAccountDataValidator);
        Command logoutCommand = new LogoutCommand();
        Command getAdminMenuCommand = new GetAdminMenuCommand();
        Command getTeacherMenuCommand = new GetTeacherMenuCommand();
        Command getStudentMenuCommand = new GetStudentMenuCommand();
        Command viewUsersCommand = new GetUsersListCommand(userAccountService);
        Command getEditAccountCommand = new GetEditAccountCommand(userAccountService, userRoleService);
        Command postEditAccountCommand = new PostEditAccountCommand(userAccountService, userRoleService, editUserAccountValidator);
        Command deleteAccountCommand = new DeleteAccountCommand(userAccountService);
        Command getTestListCommand = new GetTestListCommand(testService);
        Command getEditTestCommand = new GetEditTestCommand(testService);
        Command postEditTestCommand = new PostEditTestCommand(testService, testValidator);
        Command getAddTestCommand = new GetAddTestCommand();
        Command postAddTestCommand = new PostAddTestCommand(testService, testValidator);
        Command deleteTestCommand = new DeleteTestCommand(testService);
        Command getQuestionsCommand = new GetQuestionsCommand(questionService);
        Command getAddQuestionCommand = new GetAddQuestionCommand();
        Command postAddQuestionCommand = new PostAddQuestionCommand(questionService, questionValidator);
        Command getEditQuestionCommand = new GetEditQuestionCommand(questionService);
        Command postEditQuestionCommand = new PostEditQuestionCommand(questionService, questionValidator);
        Command deleteQuestionCommand = new DeleteQuestionCommand(questionService);
        Command getAnswerOptionsCommand = new GetAnswerOptionsCommand(answerOptionService, questionService);
        Command getAddAnswerOptionCommand = new GetAddAnswerOptionCommand();
        Command postAddAnswerOptionCommand = new PostAddAnswerOptionCommand(answerOptionService, answerOptionValidator);
        Command getEditAnswerOptionCommand = new GetEditAnswerOptionCommand(answerOptionService);
        Command postEditAnswerOptionCommand = new PostEditAnswerOptionCommand(answerOptionService, answerOptionValidator);
        Command deleteAnswerOptionCommand = new DeleteAnswerOptionCommand(answerOptionService);
        Command viewStudentGroupsCommand = new GetStudentGroupListCommand(studentGroupService);
        Command getAddStudentGroupCommand = new GetAddStudentGroupCommand();
        Command postAddStudentGroupCommand = new PostAddStudentGroupCommand(studentGroupService, studentGroupValidator);
        Command getEditStudentGroupCommand = new GetEditStudentGroupCommand(studentGroupService);
        Command postEditStudentGroupCommand = new PostEditStudentGroupCommand(studentGroupService, studentGroupValidator);
        Command deleteStudentGroupCommand = new DeleteStudentGroupCommand(studentGroupService);
        Command getStudentsInGroupCommand = new GetStudentsInGroupCommand(userAccountService);
        Command getAddStudentToGroupCommand = new GetAddStudentToGroupCommand(userAccountService);
        Command postAddStudentToGroupCommand = new PostAddStudentToGroupCommand(studentGroupService);
        Command deleteStudentFromGroupCommand = new DeleteStudentFromGroupCommand(studentGroupService);
        Command getTestsAssignedToGroupCommand = new GetTestsAssignedToGroupCommand(testService);
        Command getAssignAnyTestToGroupCommand = new GetAssignTestToGroupCommand(testService);
        Command postAssignTestToGroupCommand = new PostAssignTestToGroupCommand(studentGroupService);
        Command deleteTestFromGroupCommand = new DeleteTestFromGroupCommand(studentGroupService);
        Command toWelcomePageCommand = new ToWelcomePageCommand();
        Command viewCurrentStudentGroupsCommand = new ViewCurrentStudentGroupsCommand(studentGroupService);
        Command leaveStudentGroupCommand = new LeaveStudentGroupCommand(studentGroupService);
        Command getAvailableTestsCommand = new GetAvailableTestsCommand(testService);
        Command startTestPassingCommand = new StartTestPassCommand(questionService);
        Command doTestPassingCommand = new DoTestPassCommand(questionService, testTakingValidator);
        Command finishTestPassingCommand = new FinishTestPassCommand(testResultService);
        Command getTestResultsCommand = new GetTestResultsCommand(testResultService);
        Command viewTestResultCommand = new ViewTestResultCommand(testResultService, questionService, userAccountService);
        Command exceptionCommand = new ExceptionCommand();

        //register commands
        CommandProvider.addCommand(CommandType.GET_LOGIN, getLoginCommand);
        CommandProvider.addCommand(CommandType.LOGIN, loginCommand);
        CommandProvider.addCommand(CommandType.PREPARE_REGISTER, prepareRegisterCommand);
        CommandProvider.addCommand(CommandType.REGISTER, registerCommand);
        CommandProvider.addCommand(CommandType.LOGOUT, logoutCommand);
        CommandProvider.addCommand(CommandType.ADMIN_MENU, getAdminMenuCommand);
        CommandProvider.addCommand(CommandType.TEACHER_MENU, getTeacherMenuCommand);
        CommandProvider.addCommand(CommandType.STUDENT_MENU, getStudentMenuCommand);
        CommandProvider.addCommand(CommandType.VIEW_USERS, viewUsersCommand);
        CommandProvider.addCommand(CommandType.GET_EDIT_ACCOUNT, getEditAccountCommand);
        CommandProvider.addCommand(CommandType.POST_EDIT_ACCOUNT, postEditAccountCommand);
        CommandProvider.addCommand(CommandType.DELETE_ACCOUNT, deleteAccountCommand);
        CommandProvider.addCommand(CommandType.VIEW_TESTS, getTestListCommand);
        CommandProvider.addCommand(CommandType.GET_EDIT_TEST, getEditTestCommand);
        CommandProvider.addCommand(CommandType.POST_EDIT_TEST, postEditTestCommand);
        CommandProvider.addCommand(CommandType.GET_ADD_TEST, getAddTestCommand);
        CommandProvider.addCommand(CommandType.POST_ADD_TEST, postAddTestCommand);
        CommandProvider.addCommand(CommandType.DELETE_TEST, deleteTestCommand);
        CommandProvider.addCommand(CommandType.VIEW_QUESTIONS, getQuestionsCommand);
        CommandProvider.addCommand(CommandType.GET_ADD_QUESTION, getAddQuestionCommand);
        CommandProvider.addCommand(CommandType.POST_ADD_QUESTION, postAddQuestionCommand);
        CommandProvider.addCommand(CommandType.GET_EDIT_QUESTION, getEditQuestionCommand);
        CommandProvider.addCommand(CommandType.POST_EDIT_QUESTION, postEditQuestionCommand);
        CommandProvider.addCommand(CommandType.DELETE_QUESTION, deleteQuestionCommand);
        CommandProvider.addCommand(CommandType.VIEW_OPTIONS, getAnswerOptionsCommand);
        CommandProvider.addCommand(CommandType.GET_ADD_ANSWER_OPTION, getAddAnswerOptionCommand);
        CommandProvider.addCommand(CommandType.POST_ADD_ANSWER_OPTION, postAddAnswerOptionCommand);
        CommandProvider.addCommand(CommandType.GET_EDIT_ANSWER_OPTION, getEditAnswerOptionCommand);
        CommandProvider.addCommand(CommandType.POST_EDIT_ANSWER_OPTION, postEditAnswerOptionCommand);
        CommandProvider.addCommand(CommandType.DELETE_ANSWER_OPTION, deleteAnswerOptionCommand);
        CommandProvider.addCommand(CommandType.VIEW_GROUPS, viewStudentGroupsCommand);
        CommandProvider.addCommand(CommandType.GET_ADD_STUDENT_GROUP, getAddStudentGroupCommand);
        CommandProvider.addCommand(CommandType.POST_ADD_STUDENT_GROUP, postAddStudentGroupCommand);
        CommandProvider.addCommand(CommandType.GET_EDIT_STUDENT_GROUP, getEditStudentGroupCommand);
        CommandProvider.addCommand(CommandType.POST_EDIT_STUDENT_GROUP, postEditStudentGroupCommand);
        CommandProvider.addCommand(CommandType.DELETE_STUDENT_GROUP, deleteStudentGroupCommand);
        CommandProvider.addCommand(CommandType.VIEW_STUDENTS_IN_GROUP, getStudentsInGroupCommand);
        CommandProvider.addCommand(CommandType.GET_ADD_STUDENT_TO_GROUP, getAddStudentToGroupCommand);
        CommandProvider.addCommand(CommandType.POST_ADD_STUDENT_TO_GROUP, postAddStudentToGroupCommand);
        CommandProvider.addCommand(CommandType.DELETE_STUDENT_FROM_GROUP, deleteStudentFromGroupCommand);
        CommandProvider.addCommand(CommandType.VIEW_TESTS_ASSIGNED_TO_GROUP, getTestsAssignedToGroupCommand);
        CommandProvider.addCommand(CommandType.GET_ASSIGN_TEST_TO_GROUP, getAssignAnyTestToGroupCommand);
        CommandProvider.addCommand(CommandType.POST_ASSIGN_TEST_TO_GROUP, postAssignTestToGroupCommand);
        CommandProvider.addCommand(CommandType.DELETE_TEST_FROM_GROUP, deleteTestFromGroupCommand);
        CommandProvider.addCommand(CommandType.TO_WELCOME_PAGE, toWelcomePageCommand);
        CommandProvider.addCommand(CommandType.VIEW_STUDENT_GROUPS, viewCurrentStudentGroupsCommand);
        CommandProvider.addCommand(CommandType.LEAVE_STUDENT_GROUP, leaveStudentGroupCommand);
        CommandProvider.addCommand(CommandType.GET_AVAILABLE_TESTS, getAvailableTestsCommand);
        CommandProvider.addCommand(CommandType.START_TEST_PASSING, startTestPassingCommand);
        CommandProvider.addCommand(CommandType.DO_TEST_PASSING, doTestPassingCommand);
        CommandProvider.addCommand(CommandType.FINISH_TEST_PASSING, finishTestPassingCommand);
        CommandProvider.addCommand(CommandType.VIEW_RESULTS, getTestResultsCommand);
        CommandProvider.addCommand(CommandType.VIEW_TEST_RESULT, viewTestResultCommand);
        CommandProvider.addCommand(CommandType.EXCEPTION_COMMAND, exceptionCommand);
    }

    public static void destroy() {
        connectionPool.close();
    }
}
