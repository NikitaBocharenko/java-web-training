package by.training.kas.validator;

import by.training.kas.application.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class TestValidator implements RequestDataValidator {
    private static final String TEST_NAME_REGEX = "[\\p{L}\\p{Punct}[0-9] ]{1,100}";
    private static final String DESCRIPTION_REGEX = "[\\p{L}\\p{Punct}[0-9] ]{0,500}";
    private static final String REQUEST_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm";

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();

        String testName = request.getParameter("testName");
        checkTestName(testName, validationResult);

        String description = request.getParameter("description");
        checkDescription(description, validationResult);

        String deadlineTimeStr = request.getParameter("deadlineTime");
        checkDeadlineTime(deadlineTimeStr, validationResult);

        return validationResult;
    }

    private void checkTestName(String testName, ValidationResult validationResult) {
        if (testName == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "test-name.null");
        } else if (testName.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "test-name.empty");
        } else if (!Pattern.matches(TEST_NAME_REGEX, testName)) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "test-name.not-matches");
        }
    }

    private void checkDescription(String description, ValidationResult validationResult) {
        if (description == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "description.null");
        } else if (!Pattern.matches(DESCRIPTION_REGEX, description)) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "description.not-matches");
        }
    }

    private void checkDeadlineTime(String deadlineTimeStr, ValidationResult validationResult) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(REQUEST_DATE_PATTERN);
        Date deadlineTime = null;
        try {
            deadlineTime = dateFormat.parse(deadlineTimeStr);
        } catch (ParseException e) {
            validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "deadline-time.not-matches");
        }
        if (deadlineTime != null) {
            Date currentTime = new Date();
            if (currentTime.after(deadlineTime)) {
                validationResult.addError(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE, "deadline-time.before-now");
            }
        }
    }
}
