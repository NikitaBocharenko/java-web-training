package by.training.kas.validator;

import by.training.kas.entity.QuestionType;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

import static by.training.kas.application.ApplicationConstants.VALIDATION_QUESTION_ERRORS_TYPE;

public class QuestionValidator implements RequestDataValidator {
    private static final String QUESTION_TEXT_REGEX = "[\\p{L}\\p{Punct}[0-9] ]{1,500}";

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();

        String questionText = request.getParameter("questionText");
        checkQuestionText(questionText, validationResult);

        String questionTypeName = request.getParameter("questionType");
        checkQuestionTypeName(questionTypeName, validationResult);

        return validationResult;
    }

    private void checkQuestionText(String questionText, ValidationResult validationResult) {
        if (questionText == null) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "question-text.null");
        } else if (questionText.isEmpty()) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "question-text.empty");
        } else if (!Pattern.matches(QUESTION_TEXT_REGEX, questionText)) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "question-text.not-matches");
        }
    }

    private void checkQuestionTypeName(String questionTypeName, ValidationResult validationResult) {
        if (questionTypeName == null) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "type.null");
        } else if (questionTypeName.isEmpty()) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "type.empty");
        } else if (!QuestionType.fromString(questionTypeName).isPresent()) {
            validationResult.addError(VALIDATION_QUESTION_ERRORS_TYPE, "type.illegal");
        }
    }
}
