package by.training.kas.dao;

import by.training.kas.entity.UserRole;

import java.sql.SQLException;
import java.util.Set;

public interface UserRoleDAO extends EntityDAO<UserRole> {
    UserRole selectByName(String roleName) throws SQLException;

    Set<UserRole> selectPossibleRoles() throws SQLException;

    Set<UserRole> selectRolesForAccount(long userAccountId) throws SQLException;

    void assignRole(long userAccountId, long userRoleId) throws SQLException;

    void deleteRolesForAccount(long userAccountId) throws SQLException;
}
