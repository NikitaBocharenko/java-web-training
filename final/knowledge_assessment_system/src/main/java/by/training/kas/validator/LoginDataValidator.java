package by.training.kas.validator;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;

public class LoginDataValidator implements RequestDataValidator {
    private UserAccountService userAccountService;

    public LoginDataValidator(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();
        String login = request.getParameter("login");
        if (login == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE, "login.null");
        } else if (login.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE, "login.empty");
        }

        String password = request.getParameter("password");
        if (password == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE, "password.null");
        } else if (password.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE, "password.empty");
        }

        if (validationResult.isValid() && !userAccountService.getByCredentials(login, password).isPresent()) {
            validationResult.addError(ApplicationConstants.VALIDATION_LOGIN_ERRORS_TYPE, "account.not-found");
        }
        return validationResult;
    }
}
