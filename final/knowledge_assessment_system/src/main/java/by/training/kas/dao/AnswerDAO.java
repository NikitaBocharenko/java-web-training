package by.training.kas.dao;

import by.training.kas.entity.Answer;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public interface AnswerDAO extends EntityDAO<Answer> {
    void insertSelectedOptions(long answerId, long optionId) throws SQLException;

    List<Answer> selectByResultId(long resultId) throws SQLException;

    Set<Long> selectUserOptions(long answerId) throws SQLException;
}
