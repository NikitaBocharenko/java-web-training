package by.training.kas.command;

import by.training.kas.dto.TestDTO;
import by.training.kas.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

public class GetTestListCommand implements Command {
    private TestService testService;

    public GetTestListCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Set<String> roleNames = (Set<String>) session.getAttribute("userRoles");
        List<TestDTO> testList;
        if (roleNames.contains("admin")) {
            testList = testService.getAll();
        } else {
            long creatorId = (long) session.getAttribute("userId");
            testList = testService.getAllByCreator(creatorId);
        }
        request.setAttribute("testList", testList);
        return "testList";
    }
}
