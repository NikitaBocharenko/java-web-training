package by.training.kas.dao;

import by.training.kas.entity.Question;
import by.training.kas.entity.QuestionType;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT;

public class QuestionDAOImpl implements QuestionDAO {
    private ConnectionManager connectionManager;

    private static final String SELECT_ALL = "select q.id, q.question_text, t.type_name, q.test_id from question q " +
            "join question_type t on q.question_type_id = t.id ";
    private static final String SELECT_BY_ID = SELECT_ALL + "where q.id = ?";
    private static final String SELECT_BY_TEST_ID = SELECT_ALL + "where q.test_id = ? order by q.id ";
    private static final String SELECT_BY_TEST_ID_IN_LIMIT = SELECT_BY_TEST_ID + " limit ?, 1";
    private static final String INSERT = "insert into question(question_text, question_type_id, test_id) values (?, ?, ?)";
    private static final String UPDATE = "update question set question_text = ?, question_type_id = ? where id = ?";
    private static final String DELETE = "delete from question where id = ?";

    private static final Logger log = Logger.getLogger(QuestionDAOImpl.class);

    public QuestionDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    private Question parse(ResultSet resultSet) throws SQLException, DAOException {
        long id = resultSet.getLong("id");
        String questionText = resultSet.getString("question_text");
        String questionTypeName = resultSet.getString("type_name");
        QuestionType questionType = QuestionType.fromString(questionTypeName)
                .orElseThrow(() -> new DAOException("Unknown question type: " + questionTypeName));
        long testId = resultSet.getLong("test_id");
        return new Question(id, questionText, questionType, testId);
    }

    @Override
    public long insert(Question entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT)) {
            int i = 0;
            statement.setString(++i, entity.getQuestionText());
            statement.setInt(++i, entity.getQuestionType().ordinal() + 1);
            statement.setLong(++i, entity.getTestId());
            log.debug(statement);
            return statement.executeUpdate();
        }
    }

    @Override
    public Question select(long id) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            statement.setLong(++i, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return parse(resultSet);
                } else {
                    throw new DAOException(EXCEPTION_ENTITY_NOT_FOUND_BY_ID_TEXT +
                            " in class " + QuestionDAOImpl.class.getSimpleName() + ", " + id);
                }
            }
        }
    }

    @Override
    public List<Question> selectAll() throws SQLException, DAOException {
        List<Question> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Question question = parse(resultSet);
                result.add(question);
            }
        }
        return result;
    }

    @Override
    public void update(Question entity) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            int i = 0;
            statement.setString(++i, entity.getQuestionText());
            statement.setInt(++i, entity.getQuestionType().ordinal() + 1);
            statement.setLong(++i, entity.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public void delete(long id) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE)) {
            int i = 0;
            statement.setLong(++i, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<Question> selectByTestId(long testId) throws SQLException, DAOException {
        List<Question> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_TEST_ID)) {
            int i = 0;
            statement.setLong(++i, testId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Question question = parse(resultSet);
                    result.add(question);
                }
            }
        }
        return result;
    }

    @Override
    public Question selectByTestIdAndNumber(long testId, int number) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_TEST_ID_IN_LIMIT)) {
            int i = 0;
            statement.setLong(++i, testId);
            statement.setInt(++i, number);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return parse(resultSet);
                } else {
                    throw new DAOException("Question wasn't found by test id: " + testId + " and number: " + number);
                }
            }
        }
    }
}
