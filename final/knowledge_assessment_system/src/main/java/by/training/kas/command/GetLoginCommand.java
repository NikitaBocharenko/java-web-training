package by.training.kas.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetLoginCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        return "login";
    }
}
