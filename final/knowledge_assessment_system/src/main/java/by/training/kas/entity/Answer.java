package by.training.kas.entity;

import java.util.Objects;

public class Answer extends Entity {
    private long questionId;
    private long resultId;
    private boolean isCorrect;

    public Answer(long id, long questionId, long resultId, boolean isCorrect) {
        this.id = id;
        this.questionId = questionId;
        this.resultId = resultId;
        this.isCorrect = isCorrect;
    }

    public long getQuestionId() {
        return questionId;
    }

    public long getResultId() {
        return resultId;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return questionId == answer.questionId &&
                resultId == answer.resultId &&
                isCorrect == answer.isCorrect;
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId, resultId, isCorrect);
    }
}
