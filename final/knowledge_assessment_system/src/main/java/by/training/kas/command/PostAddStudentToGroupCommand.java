package by.training.kas.command;

import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PostAddStudentToGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public PostAddStudentToGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        Set<String> paramNames = request.getParameterMap().keySet();
        List<Long> studentIdsToAddInGroup = paramNames.stream()
                .filter(paramName -> paramName.startsWith("studentN"))
                .map(paramName -> paramName.replace("studentN", ""))
                .map(Long::parseLong)
                .collect(Collectors.toList());
        studentGroupService.addStudentsToGroup(studentIdsToAddInGroup, studentGroupId);
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("studentGroupId", studentGroupIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_STUDENTS_IN_GROUP, redirectParams);
    }
}
