package by.training.kas.validator;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.kas.application.ApplicationConstants.VALIDATION_TEST_TAKING_ERRORS_TYPE;

public class TestTakingValidator implements RequestDataValidator {
    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();

        Set<String> paramNames = request.getParameterMap().keySet();
        Set<String> answers = paramNames.stream()
                .filter(paramName -> paramName.startsWith("option"))
                .collect(Collectors.toSet());
        if (answers.isEmpty()) {
            validationResult.addError(VALIDATION_TEST_TAKING_ERRORS_TYPE, "answers.no-selected");
        }

        return validationResult;
    }
}
