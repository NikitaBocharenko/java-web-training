package by.training.kas.command;

import by.training.kas.application.ApplicationConstants;
import by.training.kas.entity.Test;
import by.training.kas.service.TestService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PostEditTestCommand implements Command {
    private TestService testService;
    private RequestDataValidator requestDataValidator;

    public PostEditTestCommand(TestService testService, RequestDataValidator requestDataValidator) {
        this.testService = testService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            Test test = buildTest(request);
            testService.update(test);
            return RedirectParamStringBuilder.build(CommandType.VIEW_TESTS, new HashMap<>());
        } else {
            request.setAttribute(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE,
                    validationResult.getErrorsByType(ApplicationConstants.VALIDATION_TEST_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_EDIT_TEST);
            return command.process(request, response);
        }

    }

    private Test buildTest(HttpServletRequest request) {
        String idStr = request.getParameter("testId");
        long id = Long.parseLong(idStr);
        String testName = request.getParameter("testName");
        String description = request.getParameter("description");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        String deadlineTimeStr = request.getParameter("deadlineTime");
        Date deadlineTime;
        try {
            deadlineTime = dateFormat.parse(deadlineTimeStr);
        } catch (ParseException e) {
            throw new CommandException("Exception while date parsing: " + e.getMessage(), e);
        }
        return new Test(id, testName, description, null, deadlineTime, -1);
    }
}
