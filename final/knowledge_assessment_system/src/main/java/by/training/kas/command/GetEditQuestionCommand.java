package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.service.QuestionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetEditQuestionCommand implements Command {
    private QuestionService questionService;

    public GetEditQuestionCommand(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("questionId");
        long id = Long.parseLong(idStr);
        QuestionDTO questionDTO = questionService.getById(id);
        request.setAttribute("question", questionDTO);
        return "editQuestion";
    }
}
