package by.training.kas.filter;

import by.training.kas.command.CommandType;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebFilter(urlPatterns = {"/"}, servletNames = {"app"})
public class SecurityFilter implements Filter {
    private final Map<CommandType, Set<String>> commandsRolesMap = new EnumMap<>(CommandType.class);
    private static final String GUEST_ROLE = "guest";
    private static final Logger log = Logger.getLogger(SecurityFilter.class);
    private static final String FORWARD_PLACE = "/jsp/mainLayout.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("security.properties");
        String path = resource.getPath().substring(1);
        Properties property = new Properties();
        try {
            FileReader reader = new FileReader(path);
            property.load(reader);
        } catch (IOException e) {
            throw new ServletException(e.getMessage(), e);
        }
        Stream.of(CommandType.values()).forEach(commandType -> {
            String roles = property.getProperty("command." + commandType.name());
            String[] rolesArray = roles.split(",");
            Set<String> rolesSet = Stream.of(rolesArray).collect(Collectors.toSet());
            commandsRolesMap.put(commandType, rolesSet);
        });
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession(true);
        String commandName = servletRequest.getParameter("command");
        Optional<CommandType> commandTypeOptional = CommandType.fromString(commandName);
        if (commandTypeOptional.isPresent()) {
            Set<String> userRoles = (Set<String>) session.getAttribute("userRoles");
            if (userRoles == null) {
                userRoles = new HashSet<>();
                userRoles.add(GUEST_ROLE);
            }
            Set<String> possibleRoles = new HashSet<>(commandsRolesMap.get(commandTypeOptional.get()));
            possibleRoles.retainAll(userRoles);
            if (!possibleRoles.isEmpty()) {
                log.debug("appropriate rights: " + commandName);
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                log.debug("not enough rights: " + commandName);
                servletRequest.setAttribute("viewName", "exception");
                servletRequest.setAttribute("exceptionMessage", "Not enough rights for command " + commandName);
                servletRequest.getRequestDispatcher(FORWARD_PLACE).forward(servletRequest, servletResponse);
            }
        } else {
            log.debug("unknown command: " + commandName);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        commandsRolesMap.clear();
    }


}
