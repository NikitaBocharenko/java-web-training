package by.training.kas.command;

import by.training.kas.service.QuestionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DeleteQuestionCommand implements Command {
    private QuestionService questionService;

    public DeleteQuestionCommand(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("questionId");
        long id = Long.parseLong(idStr);
        questionService.delete(id);
        String testIdStr = request.getParameter("testId");
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("testId", testIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_QUESTIONS, redirectParams);
    }
}
