package by.training.kas.command;

import by.training.kas.service.AnswerOptionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DeleteAnswerOptionCommand implements Command {
    private AnswerOptionService answerOptionService;

    public DeleteAnswerOptionCommand(AnswerOptionService answerOptionService) {
        this.answerOptionService = answerOptionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("answerOptionId");
        long id = Long.parseLong(idStr);
        answerOptionService.delete(id);
        String questionIdStr = request.getParameter("questionId");
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("questionId", questionIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_OPTIONS, redirectParams);
    }
}
