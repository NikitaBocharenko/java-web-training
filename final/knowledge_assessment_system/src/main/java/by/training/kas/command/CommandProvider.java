package by.training.kas.command;

import java.util.EnumMap;

public class CommandProvider {
    private CommandProvider(){}

    private static EnumMap<CommandType, Command> commands = new EnumMap<>(CommandType.class);

    public static void addCommand(CommandType type, Command command){
        commands.put(type, command);
    }

    public static Command getByType(CommandType type) {
        return commands.get(type);
    }
}
