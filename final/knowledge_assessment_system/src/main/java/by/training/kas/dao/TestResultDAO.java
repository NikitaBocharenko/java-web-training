package by.training.kas.dao;

import by.training.kas.entity.TestResult;

import java.sql.SQLException;
import java.util.List;

public interface TestResultDAO extends EntityDAO<TestResult> {
    List<TestResult> selectByTestId(long testId) throws SQLException;
}
