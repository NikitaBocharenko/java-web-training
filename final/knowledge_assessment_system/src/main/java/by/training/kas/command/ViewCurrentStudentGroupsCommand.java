package by.training.kas.command;

import by.training.kas.dto.StudentGroupDTO;
import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ViewCurrentStudentGroupsCommand implements Command {
    private StudentGroupService studentGroupService;

    public ViewCurrentStudentGroupsCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        long memberId = (long) session.getAttribute("userId");
        List<StudentGroupDTO> groups = studentGroupService.getByMember(memberId);
        request.setAttribute("groups", groups);
        return "studentGroupList";
    }
}
