package by.training.kas.service;

import by.training.kas.entity.UserRole;

import java.util.List;
import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getPossibleRoles();
    List<UserRole> getAll();
    UserRole getByName(String roleName);
}
