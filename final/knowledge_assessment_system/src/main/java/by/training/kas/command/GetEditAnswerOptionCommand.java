package by.training.kas.command;

import by.training.kas.entity.AnswerOption;
import by.training.kas.service.AnswerOptionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetEditAnswerOptionCommand implements Command {
    private AnswerOptionService answerOptionService;

    public GetEditAnswerOptionCommand(AnswerOptionService answerOptionService) {
        this.answerOptionService = answerOptionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("answerOptionId");
        long id = Long.parseLong(idStr);
        AnswerOption answerOption = answerOptionService.getById(id);
        request.setAttribute("option", answerOption);
        return "editAnswerOption";
    }
}
