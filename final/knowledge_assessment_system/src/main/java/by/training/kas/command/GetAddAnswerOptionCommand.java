package by.training.kas.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetAddAnswerOptionCommand implements Command {

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("questionId", request.getParameter("questionId"));
        return "addAnswerOption";
    }
}
