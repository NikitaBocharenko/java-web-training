package by.training.kas.command;

import by.training.kas.dto.TestDTO;
import by.training.kas.service.TestService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetEditTestCommand implements Command {
    private TestService testService;

    private static final Logger log = Logger.getLogger(GetEditTestCommand.class);

    public GetEditTestCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        TestDTO test = testService.getById(testId);
        log.debug(test);
        request.setAttribute("test", test);
        return "editTest";
    }
}
