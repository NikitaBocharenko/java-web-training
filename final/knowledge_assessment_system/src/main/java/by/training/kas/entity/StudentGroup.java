package by.training.kas.entity;

import java.util.Objects;

public class StudentGroup extends Entity {
    private String groupName;
    private long creatorId;

    public StudentGroup(long id, String groupName, long creatorId) {
        this.id = id;
        this.groupName = groupName;
        this.creatorId = creatorId;
    }

    public String getGroupName() {
        return groupName;
    }

    public long getCreatorId() {
        return creatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentGroup that = (StudentGroup) o;
        return creatorId == that.creatorId &&
                Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName, creatorId);
    }
}
