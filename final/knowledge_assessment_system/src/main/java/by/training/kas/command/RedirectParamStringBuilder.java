package by.training.kas.command;

import java.util.Map;
import java.util.Set;

public class RedirectParamStringBuilder {
    private RedirectParamStringBuilder(){}

    public static String build(CommandType commandType, Map<String, String> parameters) {
        StringBuilder result = new StringBuilder("redirect:command=" + commandType);
        Set<Map.Entry<String, String>> entrySet = parameters.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            result.append("&");
            result.append(entry.getKey());
            result.append("=");
            result.append(entry.getValue());
        }
        return result.toString();
    }
}
