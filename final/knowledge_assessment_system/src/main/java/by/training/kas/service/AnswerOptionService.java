package by.training.kas.service;

import by.training.kas.entity.AnswerOption;

import java.util.List;

public interface AnswerOptionService {
    AnswerOption getById(long answerOptionId);
    void add(AnswerOption answerOption);
    void update(AnswerOption answerOption);
    void delete(long answerOptionId);
    List<AnswerOption> getByQuestionId(long questionId);
}
