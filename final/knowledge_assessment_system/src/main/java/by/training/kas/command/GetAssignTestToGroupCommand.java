package by.training.kas.command;

import by.training.kas.dto.TestDTO;
import by.training.kas.service.TestService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

public class GetAssignTestToGroupCommand implements Command {
    private TestService testService;

    public GetAssignTestToGroupCommand(TestService testService) {
        this.testService = testService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        HttpSession session = request.getSession();
        Set<String> roleNames = (Set<String>) session.getAttribute("userRoles");
        List<TestDTO> tests;
        if (roleNames.contains("admin")) {
            tests = testService.getAllNotInGroup(studentGroupId);
        } else {
            long creatorId = (long) session.getAttribute("userId");
            tests = testService.getAllNotInGroupByCreator(studentGroupId, creatorId);
        }
        request.setAttribute("tests", tests);
        request.setAttribute("studentGroupId", studentGroupId);
        return "assignTestToGroup";
    }
}
