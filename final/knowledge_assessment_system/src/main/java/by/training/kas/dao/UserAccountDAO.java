package by.training.kas.dao;

import by.training.kas.entity.UserAccount;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserAccountDAO extends EntityDAO<UserAccount> {
    boolean isLoginAlreadyExists(String login) throws SQLException, DAOException;
    Optional<UserAccount> selectByLoginPassword(String login, String password) throws SQLException;
    List<UserAccount> selectStudentsInGroup(long studentGroupId) throws SQLException;
    List<UserAccount> selectStudentsNotInGroup(long studentGroupId) throws SQLException;
}
