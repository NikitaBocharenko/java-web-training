package by.training.kas.service;

import by.training.kas.dto.TestResultDTO;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TestResultService {
    long submitTest(long testId, long userAccountId, Map<Long, Set<Long>> answers);
    TestResultDTO getById(long resultId);
    List<TestResultDTO> getByTestId(long testId);
}
