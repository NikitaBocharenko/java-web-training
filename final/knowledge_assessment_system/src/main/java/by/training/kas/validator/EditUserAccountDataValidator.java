package by.training.kas.validator;

import by.training.kas.application.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class EditUserAccountDataValidator implements RequestDataValidator {
    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();
        String idStr = request.getParameter(ApplicationConstants.USER_ACCOUNT_ID_ATTRIBUTE_NAME);
        if (idStr == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "id.null");
        } else {
            try {
                Long.parseLong(idStr);
            } catch (NumberFormatException e) {
                validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "id.not-a-number");
            }
        }
        String firstName = request.getParameter(ApplicationConstants.USER_ACCOUNT_FIRST_NAME_ATTRIBUTE_NAME);
        if (firstName == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "first-name.null");
        } else if (firstName.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "first-name.empty");
        }
        String lastName = request.getParameter(ApplicationConstants.USER_ACCOUNT_LAST_NAME_ATTRIBUTE_NAME);
        if (lastName == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "last-name.null");
        } else if (lastName.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "last-name.empty");
        }
        String email = request.getParameter(ApplicationConstants.USER_ACCOUNT_EMAIL_ATTRIBUTE_NAME);
        if (email == null) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "email.null");
        } else if (email.isEmpty()) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "email.empty");
        }
        Set<String> paramNames = request.getParameterMap().keySet();
        boolean isSomeRoleSelected = paramNames.stream().anyMatch(paramName -> paramName.endsWith("Role"));
        if (!isSomeRoleSelected) {
            validationResult.addError(ApplicationConstants.VALIDATION_USER_ACCOUNT_EDIT_ERRORS_TYPE, "roles.no-selected");
        }
        return validationResult;
    }
}
