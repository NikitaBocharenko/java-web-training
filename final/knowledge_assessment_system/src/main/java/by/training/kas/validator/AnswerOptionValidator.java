package by.training.kas.validator;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

import static by.training.kas.application.ApplicationConstants.VALIDATION_ANSWER_OPTION_ERRORS_TYPE;

public class AnswerOptionValidator implements RequestDataValidator {
    private static final String ANSWER_OPTION_TEXT_REGEX = "[\\p{L}\\p{Punct}[0-9] ]{1,100}";

    @Override
    public ValidationResult validate(HttpServletRequest request) {
        ValidationResult validationResult = new ValidationResult();
        String optionText = request.getParameter("optionText");
        if (optionText == null) {
            validationResult.addError(VALIDATION_ANSWER_OPTION_ERRORS_TYPE, "option-text.null");
        } else if (optionText.isEmpty()) {
            validationResult.addError(VALIDATION_ANSWER_OPTION_ERRORS_TYPE, "option-text.empty");
        } else if (!Pattern.matches(ANSWER_OPTION_TEXT_REGEX, optionText)) {
            validationResult.addError(VALIDATION_ANSWER_OPTION_ERRORS_TYPE, "option-text.not-matches");
        }
        return validationResult;
    }
}
