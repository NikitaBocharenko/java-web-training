package by.training.kas.command;

import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DeleteTestFromGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public DeleteTestFromGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        studentGroupService.deleteTestFromGroup(testId, studentGroupId);
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("studentGroupId", studentGroupIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_TESTS_ASSIGNED_TO_GROUP, redirectParams);
    }
}
