package by.training.kas.service;

import by.training.kas.entity.StudentGroup;
import by.training.kas.dto.StudentGroupDTO;

import java.util.List;

public interface StudentGroupService {
    List<StudentGroupDTO> getAll();
    void add(StudentGroup studentGroup);
    StudentGroupDTO get(long studentGroupId);
    void edit(StudentGroup studentGroup);
    void delete(long studentGroupId);
    void addStudentsToGroup(List<Long> studentsId, long studentGroupId);
    void deleteStudentFromGroup(long studentId, long studentGroupId);
    void assignTestToGroup(List<Long> testIds, long studentGroupId);
    void deleteTestFromGroup(long testId, long studentGroupId);
    List<StudentGroupDTO> getByCreator(long creatorId);
    List<StudentGroupDTO> getByMember(long memberId);
}
