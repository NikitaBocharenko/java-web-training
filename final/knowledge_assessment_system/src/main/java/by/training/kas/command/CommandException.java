package by.training.kas.command;

public class CommandException extends RuntimeException {
    public CommandException(String message, Throwable clause) {super(message, clause);}
    public CommandException(String message) {super(message);}
}
