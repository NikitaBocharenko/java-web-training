package by.training.kas.service;

import by.training.kas.dao.AnswerOptionDAO;
import by.training.kas.dao.DAOException;
import by.training.kas.entity.AnswerOption;

import java.sql.SQLException;
import java.util.List;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_DATABASE_KEY;

public class AnswerOptionServiceImpl implements AnswerOptionService {
    private AnswerOptionDAO answerOptionDAO;

    public AnswerOptionServiceImpl(AnswerOptionDAO answerOptionDAO) {
        this.answerOptionDAO = answerOptionDAO;
    }

    @Override
    public AnswerOption getById(long answerOptionId) {
        try {
            return answerOptionDAO.select(answerOptionId);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void add(AnswerOption answerOption) {
        try {
            answerOptionDAO.insert(answerOption);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void update(AnswerOption answerOption) {
        try {
            answerOptionDAO.update(answerOption);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public void delete(long answerOptionId) {
        try {
            answerOptionDAO.delete(answerOptionId);
        } catch (SQLException | DAOException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }

    @Override
    public List<AnswerOption> getByQuestionId(long questionId) {
        try {
            return answerOptionDAO.selectByQuestionId(questionId);
        } catch (SQLException e) {
            throw new ServiceException(EXCEPTION_DATABASE_KEY, e);
        }
    }
}
