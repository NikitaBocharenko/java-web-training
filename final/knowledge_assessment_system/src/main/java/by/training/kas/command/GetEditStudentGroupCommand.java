package by.training.kas.command;

import by.training.kas.dto.StudentGroupDTO;
import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetEditStudentGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public GetEditStudentGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String idStr = request.getParameter("studentGroupId");
        long id = Long.parseLong(idStr);
        StudentGroupDTO studentGroupDTO = studentGroupService.get(id);
        request.setAttribute("group", studentGroupDTO);
        return "editStudentGroup";
    }
}
