package by.training.kas.dao;

import by.training.kas.entity.UserRole;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_METHOD_UNSUPPORTED_TEXT;

public class UserRoleDAOImpl implements UserRoleDAO {
    private static final String SELECT_BY_ROLE_NAME = "select id from user_role " +
            "where role_name = ?";
    private static final String SELECT_POSSIBLE_ROLES = "select id, role_name from user_role " +
            "where role_name <> 'admin' order by id";
    private static final String SELECT_ALL = "select id, role_name from user_role order by id";
    private static final String SELECT_BY_USER_ID = "select k.id, k.role_name " +
            "from user_role k " +
            "join user_account_has_user_role t on k.id = t.user_role_id " +
            "where t.user_account_id = ?";
    private static final String ASSIGN_ROLE_TO_USER = "insert into user_account_has_user_role (user_account_id, user_role_id) " +
            "values (?, ?)";
    private static final String DELETE_ROLES_FOR_ACCOUNT = "delete from user_account_has_user_role where user_account_id = ?";

    private ConnectionManager connectionManager;

    public UserRoleDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    @Override
    public long insert(UserRole entity) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                UserRoleDAOImpl.class.getSimpleName() + ", method: long insert(UserRole entity)");
    }

    @Override
    public UserRole select(long id) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                UserRoleDAOImpl.class.getSimpleName() + ", method: UserRole select(long id)");
    }

    private UserRole parse(ResultSet resultSet) throws SQLException {
        long roleId = resultSet.getLong("id");
        String roleName = resultSet.getString("role_name");
        return new UserRole(roleId, roleName);
    }

    @Override
    public List<UserRole> selectAll() throws SQLException {
        List<UserRole> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                UserRole role = parse(resultSet);
                result.add(role);
            }
        }
        return result;
    }

    @Override
    public void update(UserRole entity) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                UserRoleDAOImpl.class.getSimpleName() + ", method: void update(UserRole entity)");
    }

    @Override
    public void delete(long id) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                UserRoleDAOImpl.class.getSimpleName() + ", method: void delete(long id)");
    }

    @Override
    public UserRole selectByName(String roleName) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_ROLE_NAME)) {
            statement.setString(1, roleName);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    long id = resultSet.getLong("id");
                    return new UserRole(id, roleName);
                }
            }
        }
        return null;
    }

    @Override
    public Set<UserRole> selectPossibleRoles() throws SQLException {
        Set<UserRole> result = new HashSet<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_POSSIBLE_ROLES);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                UserRole role = parse(resultSet);
                result.add(role);
            }
        }
        return result;
    }

    @Override
    public Set<UserRole> selectRolesForAccount(long userAccountId) throws SQLException {
        Set<UserRole> result = new HashSet<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_USER_ID)) {
            statement.setLong(1, userAccountId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    UserRole role = parse(resultSet);
                    result.add(role);
                }
            }
        }
        return result;
    }

    @Override
    public void assignRole(long userAccountId, long userRoleId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(ASSIGN_ROLE_TO_USER)) {
            int i = 0;
            statement.setLong(++i, userAccountId);
            statement.setLong(++i, userRoleId);
            statement.executeUpdate();
        }
    }

    @Override
    public void deleteRolesForAccount(long userAccountId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ROLES_FOR_ACCOUNT)) {
            int i = 0;
            statement.setLong(++i, userAccountId);
            statement.executeUpdate();
        }
    }
}
