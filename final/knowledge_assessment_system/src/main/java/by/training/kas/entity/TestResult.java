package by.training.kas.entity;

import java.util.Date;
import java.util.Objects;

public class TestResult extends Entity {
    private int result;
    private Date completionTime;
    private long userAccountId;
    private long testId;

    public TestResult(long id, int result, Date completionTime, long userAccountId, long testId) {
        this.id = id;
        this.result = result;
        this.completionTime = completionTime;
        this.userAccountId = userAccountId;
        this.testId = testId;
    }

    public int getResult() {
        return result;
    }

    public Date getCompletionTime() {
        return completionTime;
    }

    public long getUserAccountId() {
        return userAccountId;
    }

    public long getTestId() {
        return testId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestResult that = (TestResult) o;
        return result == that.result &&
                userAccountId == that.userAccountId &&
                testId == that.testId &&
                Objects.equals(completionTime, that.completionTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, completionTime, userAccountId, testId);
    }
}
