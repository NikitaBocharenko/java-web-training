package by.training.kas.command;

import by.training.kas.service.TestResultService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FinishTestPassCommand implements Command {
    private TestResultService testResultService;

    public FinishTestPassCommand(TestResultService testResultService) {
        this.testResultService = testResultService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        Long testId = (Long) session.getAttribute("passingTestId");
        Long userId = (Long) session.getAttribute("userId");
        Map<Long, Set<Long>> answers = (Map<Long, Set<Long>>) session.getAttribute("answers");
        session.removeAttribute("passingTestId");
        session.removeAttribute("answers");
        session.removeAttribute("currentQuestionNumber");
        session.removeAttribute("questionNumberList");
        long resultId = testResultService.submitTest(testId, userId, answers);
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("resultId", String.valueOf(resultId));
        redirectParams.put("testId", String.valueOf(testId));
        return RedirectParamStringBuilder.build(CommandType.VIEW_TEST_RESULT, redirectParams);
    }
}
