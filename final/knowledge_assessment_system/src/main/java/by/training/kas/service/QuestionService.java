package by.training.kas.service;

import by.training.kas.entity.Question;
import by.training.kas.dto.QuestionDTO;

import java.util.List;

public interface QuestionService {
    void add(Question question);
    QuestionDTO getById(long id);
    void update(Question question);
    void delete(long id);
    QuestionDTO getByTestIdAndNumber(long testId, int number);
    int getQuestionQuantityInTest(long testId);
    List<QuestionDTO> getByTestId(long testId);
}
