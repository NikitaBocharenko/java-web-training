package by.training.kas.dao;

import by.training.kas.entity.Entity;
import java.sql.SQLException;
import java.util.List;

public interface EntityDAO<T extends Entity> {

    long insert(T entity) throws SQLException, DAOException;

    T select(long id) throws SQLException, DAOException;

    List<T> selectAll() throws SQLException, DAOException;

    void update(T entity) throws SQLException, DAOException;

    void delete(long id) throws SQLException, DAOException;
}
