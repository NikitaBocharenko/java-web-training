package by.training.kas.dao;

import by.training.kas.entity.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static by.training.kas.application.ApplicationConstants.EXCEPTION_KEY_NOT_GENERATED_TEXT;
import static by.training.kas.application.ApplicationConstants.EXCEPTION_METHOD_UNSUPPORTED_TEXT;

public class AnswerDAOImpl implements AnswerDAO {
    private ConnectionManager connectionManager;

    private static final String INSERT = "insert into answer (question_id, result_id, is_correct) values (?, ?, ?)";
    private static final String INSERT_USER_OPTION = "insert into answer_option_has_answer (answer_option_id, answer_id) values (?, ?)";
    private static final String SELECT_BY_RESULT_ID = "select id, question_id, result_id, is_correct from answer where result_id = ?";
    private static final String SELECT_USER_OPTIONS = "select answer_option_id from answer_option_has_answer where answer_id = ?";

    public AnswerDAOImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public long insert(Answer entity) throws SQLException, DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            statement.setLong(++i, entity.getQuestionId());
            statement.setLong(++i, entity.getResultId());
            int isCorrect = entity.getIsCorrect() ? 1 : 0;
            statement.setInt(++i, isCorrect);
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getLong(1);
                } else {
                    throw new DAOException(EXCEPTION_KEY_NOT_GENERATED_TEXT + " in class " + AnswerDAOImpl.class.getSimpleName());
                }
            }
        }
    }

    @Override
    public Answer select(long id) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                AnswerDAOImpl.class.getSimpleName() + ", method: Answer select(long id)");
    }

    @Override
    public List<Answer> selectAll() throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                AnswerDAOImpl.class.getSimpleName() + ", method: List<Answer> selectAll()");
    }

    @Override
    public void update(Answer entity) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                AnswerDAOImpl.class.getSimpleName() + ", method: void update(Answer entity)");
    }

    @Override
    public void delete(long id) throws DAOException {
        throw new DAOException(EXCEPTION_METHOD_UNSUPPORTED_TEXT + ". Class: " +
                AnswerDAOImpl.class.getSimpleName() + ", method: void delete(long id)");
    }

    @Override
    public void insertSelectedOptions(long answerId, long optionId) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_USER_OPTION)) {
            int i = 0;
            statement.setLong(++i, optionId);
            statement.setLong(++i, answerId);
            statement.executeUpdate();
        }
    }

    @Override
    public List<Answer> selectByResultId(long resultId) throws SQLException {
        List<Answer> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_BY_RESULT_ID)) {
            int i = 0;
            statement.setLong(++i, resultId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Answer answer = parseResultSet(resultSet);
                    result.add(answer);
                }
            }
        }
        return result;
    }

    @Override
    public Set<Long> selectUserOptions(long answerId) throws SQLException {
        Set<Long> result = new HashSet<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_USER_OPTIONS)) {
            int i = 0;
            statement.setLong(++i, answerId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Long optionId = resultSet.getLong("answer_option_id");
                    result.add(optionId);
                }
            }
        }
        return result;
    }

    private Answer parseResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        long questionId = resultSet.getLong("question_id");
        long resultId = resultSet.getLong("result_id");
        boolean isCorrect = resultSet.getInt("is_correct") == 1;
        return new Answer(id, questionId, resultId, isCorrect);
    }
}
