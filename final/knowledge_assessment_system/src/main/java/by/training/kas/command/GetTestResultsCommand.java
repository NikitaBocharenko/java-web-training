package by.training.kas.command;

import by.training.kas.dto.TestResultDTO;
import by.training.kas.service.TestResultService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetTestResultsCommand implements Command {
    private TestResultService testResultService;

    public GetTestResultsCommand(TestResultService testResultService) {
        this.testResultService = testResultService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        List<TestResultDTO> testResults = testResultService.getByTestId(testId);
        request.setAttribute("results", testResults);
        return "testResultList";
    }
}
