package by.training.kas.command;

import by.training.kas.dto.QuestionDTO;
import by.training.kas.service.QuestionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GetQuestionsCommand implements Command {
    private QuestionService questionService;

    public GetQuestionsCommand(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String testIdStr = request.getParameter("testId");
        long testId = Long.parseLong(testIdStr);
        List<QuestionDTO> questions = questionService.getByTestId(testId);
        request.setAttribute("questions", questions);
        request.setAttribute("testId", testIdStr);
        return "questionList";
    }
}
