package by.training.kas.command;

import by.training.kas.service.StudentGroupService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DeleteStudentFromGroupCommand implements Command {
    private StudentGroupService studentGroupService;

    public DeleteStudentFromGroupCommand(StudentGroupService studentGroupService) {
        this.studentGroupService = studentGroupService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        String studentGroupIdStr = request.getParameter("studentGroupId");
        long studentGroupId = Long.parseLong(studentGroupIdStr);
        String userAccountIdStr = request.getParameter("userAccountId");
        long userAccountId = Long.parseLong(userAccountIdStr);
        studentGroupService.deleteStudentFromGroup(userAccountId, studentGroupId);
        Map<String, String> redirectParams = new HashMap<>();
        redirectParams.put("studentGroupId", studentGroupIdStr);
        return RedirectParamStringBuilder.build(CommandType.VIEW_STUDENTS_IN_GROUP, redirectParams);
    }
}
