package by.training.kas.command;

import by.training.kas.entity.AnswerOption;
import by.training.kas.service.AnswerOptionService;
import by.training.kas.validator.RequestDataValidator;
import by.training.kas.validator.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static by.training.kas.application.ApplicationConstants.VALIDATION_ANSWER_OPTION_ERRORS_TYPE;

public class PostAddAnswerOptionCommand implements Command {
    private AnswerOptionService answerOptionService;
    private RequestDataValidator requestDataValidator;

    public PostAddAnswerOptionCommand(AnswerOptionService answerOptionService, RequestDataValidator requestDataValidator) {
        this.answerOptionService = answerOptionService;
        this.requestDataValidator = requestDataValidator;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) {
        ValidationResult validationResult = requestDataValidator.validate(request);
        if (validationResult.isValid()) {
            AnswerOption answerOption = buildAnswerOption(request);
            answerOptionService.add(answerOption);
            String questionIdStr = request.getParameter("questionId");
            Map<String, String> redirectParams = new HashMap<>();
            redirectParams.put("questionId", questionIdStr);
            return RedirectParamStringBuilder.build(CommandType.VIEW_OPTIONS, redirectParams);
        } else {
            request.setAttribute("optionText", request.getParameter("optionText"));
            request.setAttribute("isCorrect", request.getParameter("isCorrect"));
            request.setAttribute(VALIDATION_ANSWER_OPTION_ERRORS_TYPE, validationResult.getErrorsByType(VALIDATION_ANSWER_OPTION_ERRORS_TYPE));
            Command command = CommandProvider.getByType(CommandType.GET_ADD_ANSWER_OPTION);
            return command.process(request, response);
        }
    }

    private AnswerOption buildAnswerOption(HttpServletRequest request) {
        String optionText = request.getParameter("optionText");
        String isCorrectStr = request.getParameter("isCorrect");
        boolean isCorrect = isCorrectStr != null;
        String questionIdStr = request.getParameter("questionId");
        long questionId = Long.parseLong(questionIdStr);
        return new AnswerOption(-1, optionText, isCorrect, questionId);
    }
}
