package by.training.kas.dao;

import java.sql.Connection;

public interface ConnectionManager {
    Connection getConnection();
}
